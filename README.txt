shiksha project code

Maven based project structure. Hibernate + Spring + jQuery.

Note to developers:

1. ENVIRONMENT SETUP: 

Follow the steps listed in the below document.



2. BITBUCKET CODE:

- Checkout remote branch 'develop' on your local system and pull the content.
- Create your feature branches based on JIRA-IDs
- Use Bitbucket pull requests to request for code review
- Merging code into develop branch will be taken care by project manager.


3. CODING PRACTICES:

Refer to the below coding guidelines.

