var pageContextElements =new Object();
	pageContextElements.table ='#tblrolepermission';
	pageContextElements.noOfCols=parseInt(noOfRoles)+2;	
	pageContextElements.checkboxClass='.chkbox_rolepermission';

var rolePermission={
		
		role:'',
		permission:'',
		isMapped :'false',
		
		
		ajaxURL:'',
		ajaxMethodType:'',
		ajaxContentType:'',
		ajaxRequestData :'',
		ajaxResponseData:'',
		
		
		init:function(){
			
			self =rolePermission;
			
			//$(pageContextElements.checkboxClass).on('click',self.fnSave);
			
			self.ajaxURL='',
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';			
		},
		
		
		/**
		 * Save new
		 * */	
		fnSave : function(obj){
			
			self.isMapped =$(obj).prop('checked');
			self.role =$(obj).data('roleid');
			self.permission =$(obj).data('permissionid');
			
			
			ajaxRequestData ={
				"permissionId":self.permission,
				"roleId":self.role,
				"isMapped":self.isMapped,
			};
			
			if((self.isMapped) ==true){
				self.ajaxURL ="role/permission";
				self.ajaxMethodType ='POST';	
			}else{
				self.ajaxURL ="role/permission";
				self.ajaxMethodType ='PUT';
			}
			self.ajaxContentType ='application/json';

			self.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);

			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
					fnSuccessAlert();
				}else{
					showErrorAlert(ajaxResponseData.responseMessage);
				}	
			}					
		},
		
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				},
			});
		}
};
	
/**
 * Show success mesage
 * */
function fnSuccessAlert(){	
	gtl.hideSpinner();
	$("#success-msg").snackbar("show");
    
}
	
/**
 *show alert error divs on ajax response 
 * */
function showErrorAlert(pErrorMsg){
	/*var options =  {
			content: "Failure."+pErrorMsg, 
			style: "toast",
			htmlAllowed: true,		    
	}
	$.snackbar(options);*/
	gtl.hideSpinner();
	
}
	
	
	
$(function() {
	
	gtl.fnSetDataTable(pageContextElements.table,pageContextElements.noOfCols);
	
	rolePermission.init();
});