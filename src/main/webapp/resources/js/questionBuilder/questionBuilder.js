var pageContextDivs =new Object();	
	pageContextDivs.table ='#tableDiv';
	


var pageContextElements =new Object();
	pageContextElements.addButton ='#btnAddState';
	pageContextElements.table ='#tblState';	
	pageContextElements.thLabels =['#','State ','Code  ','Action'];
	pageContextElements.noOfCols=4;
	pageContextElements.excludeColFilter =[0,3];
	
	
/*add modal elements*/
var addModal =new Object();
	addModal.modal ='#mdlAddState';
	addModal.addOrgForm ='#frmAddState';
	addModal.orgName ='#txtAddState';
	addModal.orgCode ='#txtAddStateCode';	
	addModal.orgSaveButton ='#btnSaveState';
	addModal.orgCancelButton ='#btnAddStateCancel';

var addFormValidateFields =['txtAddState','txtAddStateCode'];
	
/*edit modal elements*/
var editModal =new Object();
	editModal.modal ='#mdlEditState';
	editModal.editOrgForm ='#frmEditState';
	editModal.orgName ='#txtEditState';
	editModal.orgCode ='#txtEditStateCode';	
	editModal.orgSaveButton ='#btnEditState';
	editModal.orgCancelButton ='#btnEditStateCancel';

var editFormValidateFields =['txtEditState','txtEditStateCode'];

	/*delete modal elements*/
var deleteModal =new Object();
	deleteModal.modal ='#mdlDeleteState';
	deleteModal.message ='#msgDelete';
	deleteModal.deleteButton ='#btnDeleteState';
	editModal.orgCancelButton ='#btnDeleteCancel';

		
	
	
var state ={
		organisationId:0,
		organisationName:'',
		organisationCode:'',
		organisationConfigName:'',		
		ajaxURL:'',
		ajaxMethodType:'',
		ajaxContentType:'',
		ajaxRequestData :'',
		ajaxResponseData:'',
		
		
	init:function(){
		
		self =state;
		
		self.organisationId=0;
		self.organisationName='';
		self.organisationCode ='';
		self.organisationConfigName='';
				
		
		
		$(pageContextElements.addButton).on('click',self.initAddModal);
		$(deleteModal.deleteButton).on('click',self.fnDelete);
		
		self.ajaxURL='',
		self.ajaxMethodType='';
		self.ajaxContentType='';
		self.ajaxRequestData ='';
		self.ajaxResponseData='';
		
		
		
		
	    $('.frmAddOrg input[type="text"],.frmEditOrg input[type="text"]').on('focus', function() {
	    	$(addModal.modal).find('.alertdiv').hide();
	    	$(editModal.modal).find('.alertdiv').hide();
	    });
		
	},
	
	/**
	 * Save new
	 * */	
	fnSave : function(){		
		self.organisationName =$(addModal.orgName).val().toString().trim().replace(/\u00a0/g," ");
		self.organisationCode =$(addModal.orgCode).val().toString().trim().replace(/\u00a0/g," ");
		self.organisationConfigName =$(addModal.orgCustomName).val()!=''?$(addModal.orgCustomName).val().toString().trim().replace(/\u00a0/g," "):'';


		ajaxRequestData ={
				"organisationName":self.organisationName,
				"organisationCode":self.organisationCode,
				"organisationConfigLabel":self.organisationConfigName,
		};
		self.ajaxURL ="state";
		self.ajaxMethodType ='POST';
		self.ajaxContentType ='application/json';

		state.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);

		if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
			if(ajaxResponseData.response=="gtl-200"){
				var newRow =fnCreateNewRow(ajaxResponseData);					
				gtl.fnAppendNewRow(pageContextElements.table,newRow);
				gtl.fnUpdateDTColFilter(pageContextElements.table,[],pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
				gtl.fnShowOrHideModal(addModal.modal,'h');
				fnSuccessAlert();
			}else{
				showErrorAlert(addModal.modal,ajaxResponseData.responseMessage);
			}	
		}					
	},
	
	initEdit: function(orgId,orgName,orgCode,orgConfigName){
		self.organisationId	=orgId;
		self.organisationName	=orgName;
		self.organisationCode	=orgCode;
		self.organisationConfigName	=orgConfigName;
		
		$(editModal.orgName).val(orgName);
		$(editModal.orgCode).val(orgCode);	
		$(editModal.orgCustomName).val(orgConfigName);
				
		gtl.fnResetFormValidation(editModal.editOrgForm,editFormValidateFields);
		
		gtl.fnShowOrHideModal(editModal.modal,'s');
		
	},
	
	fnEdit :function(){
		
		self.organisationName =$(editModal.orgName).val().toString().trim().replace(/\u00a0/g," ");
		self.organisationCode =$(editModal.orgCode).val().toString().trim().replace(/\u00a0/g," ");
		self.organisationConfigName =$(editModal.orgCustomName).val()!=''?
								$(editModal.orgCustomName).val().toString().trim().replace(/\u00a0/g," "):'';
					
		ajaxRequestData ={
			"organisationId" :self.organisationId,
			"organisationName":self.organisationName,
			"organisationCode":self.organisationCode,
			"organisationConfigLabel":self.organisationConfigName,
		};
		self.ajaxURL ="state";
		self.ajaxMethodType ='PUT';
		self.ajaxContentType ='application/json';
		
		state.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
		
		if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
			if(ajaxResponseData.response=="gtl-200"){						
				var newRow =fnCreateNewRow(ajaxResponseData);
				gtl.fnDeleteRow(pageContextElements.table,self.organisationId);
				gtl.fnAppendNewRow(pageContextElements.table,newRow);
				gtl.fnUpdateDTColFilter(pageContextElements.table,[],pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
				gtl.fnShowOrHideModal(editModal.modal,'h');
				fnSuccessAlert();
			}else{
				showErrorAlert(editModal.modal,ajaxResponseData.responseMessage);
			}
		}
	},
	
	
	initDelete:function(pOrgId,pOrgName){		
		self.organisationId	=pOrgId;
	
		$(deleteModal.message).text("Do you really want to delete "+pOrgName+ " ?");
		gtl.fnShowOrHideModal(deleteModal.modal,'s');
		$(deleteModal.modal).find('.alertdiv').hide();
	},
	
	fnDelete :function(){
		console.log("comming...delete"+self.organisationId);
		
		self.ajaxURL ="state/"+self.organisationId;
		self.ajaxMethodType ='DELETE';
		self.ajaxContentType ='application/json';
		
		state.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
		
		if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
			if(ajaxResponseData.response=="gtl-200"){
				gtl.fnDeleteRow(pageContextElements.table,self.organisationId);
				gtl.fnUpdateDTColFilter(pageContextElements.table,[],pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
				gtl.fnShowOrHideModal(deleteModal.modal,'h');
				fnSuccessAlert();
			}else{
				showErrorAlert(deleteModal.modal,ajaxResponseData.responseMessage);
			}
		}
	},
	
	
	initAddModal :function(){
		$(addModal.orgName).val('');
		$(addModal.orgCode).val('');
		$(addModal.orgCustomName).val('');
		gtl.fnResetForm(addModal.addOrgForm);
	},
	
	/**
	 * ajax call
	 * */
	invokeAjax :function(URL,aRequestData,methodType,pContentType){
		gtl.showSpinner();
		ajaxResponseData ='';
		$.ajax({
			url : URL,
			type : methodType,
			contentType:pContentType,
			async : false,
	        data :JSON.stringify(aRequestData ),
			success : function(responseData) {
				ajaxResponseData =responseData;
			},
			error : function(e) {
				gtl.hideSpinner();
				return false;
			},
		});
	}
};
	
/////////////////////////////////////////////////////////

/**
 * create a new row and return
 * */

function fnCreateNewRow(aData){
	var q="&quot;,&quot;";
	var row = 
		"<tr id='"+aData.organisationId+"' data-id='"+aData.organisationId+"'>"+
		"<td></td>"+
		"<td title='"+aData.organisationName+"'>"+aData.organisationName+"</td>"+
		"<td title='"+aData.organisationCode+"'>"+aData.organisationCode+"</td>"+
		"<td title='"+aData.organisationConfigLabel+"'>"+aData.organisationConfigLabel+"</td>"+
		"<td><div class='div-tooltip'>" +
				 "<a class='tooltip tooltip-top' id='btnEditOrg'" +
				 "onclick=state.initEdit(&quot;"+aData.organisationId+q+gtl.escapeHtmlCharacters(aData.organisationName)+q+gtl.escapeHtmlCharacters(aData.organisationCode)+q+gtl.escapeHtmlCharacters(aData.organisationConfigLabel)+"&quot;);>" +
				 "<span class='tooltiptext'>Edit</span>" +
				 "<span class='glyphicon glyphicon-edit font-size12'></span></a>" +
			     "<a style='margin-left:10px;' class='tooltip tooltip-top' " +
			     "onclick=state.initDelete(&quot;"+aData.organisationId+q+gtl.escapeHtmlCharacters(aData.organisationName)+"&quot;);>" +
			     "<span class='tooltiptext'>Delete</span>" +
			     "<span class='glyphicon glyphicon-trash font-size12' ></span></a>" +
			     "</div>" +
		  "</td>"+
		"</tr>";
	return row;
}

/**
 *show alert error divs on ajax response 
 * */
function showErrorAlert(pMdlId,pErrorMsg){
	$(pMdlId+' #alertdiv').show();
	$(pMdlId).find('#successMessage').hide();	
	$(pMdlId).find('#errorMessage').show();
	$(pMdlId).find('#exception').show();
	pErrorMsg = pErrorMsg.replace(/#VALUE/g,categoryLabel);
	$(pMdlId).find('#exception').text(pErrorMsg);
	gtl.hideSpinner();
}
/**
 * Show success mesage
 * */
function fnSuccessAlert(){
	gtl.hideSpinner();
	//$('#success-msg').show();
    $("#success-msg").snackbar("show");
	//$("#success-msg").fadeOut(3000);	
}


	
$(function() {
	gtl.fnSetDTColFilterPagination(pageContextElements.table,pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
	gtl.fnMultipleSelAndToogleDTCol('.search_init',function(){
		gtl.fnHideColumns(pageContextElements.table,[]);
	});
	$(pageContextDivs.table).prop('hidden',false);

	state.init();

	$(addModal.addOrgForm).formValidation({

		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},

		fields: {
			txtAddOrgName: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 2,
						max: 30,
						message: 'The '+categoryLabel+'  name must be more than 2 and less than 30 characters long'
					},
					regexp: {
						/*regexp: /^[a-zA-Z0-9_\.]+$/,*/
						regexp: /^[^'?\"/\\]*$/,
						message: 'The '+categoryLabel+' name can only consist of alphabetical, number, dot and underscore'
					}
				}
			},
			txtAddOrgCode: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 2,
						max: 30,
						message: 'The '+categoryLabel+'  code must be more than 2 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[^'?\"/\\]*$/,
						message: 'The '+categoryLabel+' code can only consist of alphabetical, number, dot and underscore'
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		state.fnSave();
	});

	//////////////////////////////////////////////////////////

	$(editModal.editOrgForm).formValidation({
		fields: {
			txtEditOrgName: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 2,
						max: 30,
						message: 'The '+categoryLabel+'  name must be more than 2 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[^'?\"/\\]*$/,
						message: 'The '+categoryLabel+' name can only consist of alphabetical, number, dot and underscore'
					}
				}
			},
			txtEditOrgCode: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 2,
						max: 30,
						message: 'The '+categoryLabel+'  code must be more than 2 and less than 30 characters long'
					},
					regexp: {
						regexp: /^[^'?\"/\\]*$/,
						message: 'The '+categoryLabel+' code can only consist of alphabetical, number, dot and underscore'
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		state.fnEdit();
	});
});
