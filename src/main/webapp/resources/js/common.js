
//Role codes
var roleCodes=['SU','OADM','EADM','DVADM','DPADM','TECH','LRNR'];






/**
 * 
 *@version : v1.0 
***/
var gtl={
		
		/**
		 * sets bootstrap-multiselect 
		 * @params
		 * @arguments 	:	String	: Selector Id 
		 * */
		fnSetMultiselect :function(){
			$(arguments).each(function(index,ele){
				$(ele).multiselect('destroy');
				$(ele).multiselect({
					maxHeight: 150,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...'					
				});		
			});
		},
		
		/**
		 * Automatic collapse multi select on mouse leave
		 * */
		
		fnCollapseMultiselect :function(){
			$( ".multiselect-container" ).unbind( "mouseleave");
			$( ".multiselect-container" ).on( "mouseleave", function() {
				$(this).click();
			});
		},
		
		/**
		 * Empty the items in select box
		 * */
		fnEmptyAllSelectBox:function(){
			$.each(arguments,function(i,obj){
				$(obj).empty();
			});
		},
		
		/**
		 * sets data table options along with column filters and header names
		 * @params
		 * @tId 	:	String	: Table id
		 * @nCols	:	Array	: Number of columns
		 * @exCols :	Array	: Excluded columns
		 * @hNames : 	Array	: Header column names	
		 * */
		fnSetDTColFilterPagination :function(tId,nCols,exCols,hNames){
			$(tId).on('order.dt',gtl.fnEnableTooltip).on('search.dt',gtl.fnEnableTooltip ).on( 'page.dt',gtl.fnEnableTooltip).DataTable({
				"pagingType": "simple_numbers",
				"sDom": 't<"row"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
				"lengthMenu": [100],
				"bLengthChange": false,
				"bDestroy": true,
				"bFilter": true,
				"autoWidth": false,
				"iDisplayLength": 10,
				"stateSave": false,
				"fnDrawCallback": function ( oSettings ) {
					if ( oSettings.bSorted || oSettings.bFiltered ){
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					}
				},
				"aoColumnDefs": [{
					'bSortable': false,
					'aTargets': ['nosort']
				}],
				"aaSorting": [],

			});
			gtl.fnApplyColumnFilter(tId,nCols,exCols,hNames);
		},

		fnApplyColumnFilter :function(tId,noCols,xlCol,hdrNames){
			
			var obj=$(tId).dataTable();
			var aoCol =new Array();
			for(var ai=0;ai<noCols;ai++){
				if (jQuery.inArray(ai, xlCol)!='-1') {
					aoCol.push(null);
				} else {
					aoCol.push({type:"select",multiple: true});
				}	
			}
			obj.columnFilter({
			    sPlaceHolder : 'head:after',
			    aoColumns: aoCol,
			    bUnique : true,
			    bSort:true,	   
			});
			for(var bj=0;bj<noCols;bj++){
				var l =hdrNames[bj];
				var k=bj;
				if (jQuery.inArray(bj, xlCol) ==-1) {
					$('#h'+(k+1)).prepend(l);
				}
			}
			gtl.fnColSpanIfDTEmpty(tId,noCols);
			gtl.fnRowColors(tId);
		},
		
		//customize the multiple select box and call back for hiding columns in data table
		//for toggle column functionality
		fnMultipleSelAndToogleDTCol :function(arg,callback){
			$(arg).multiselect({
				maxHeight: 180,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				nonSelectedText: '',
				buttonTitle: function(options, select) {			 
					  if(options.length > 0){
							$(select).parents("th").find("button").addClass("filter-enabled").empty().html("<b class='glyphicon glyphicon-filter'></b>");
						}else{
							$(select).parents("th").find("button").removeClass("filter-enabled").empty().html("<b class='caret'></b>");
						}
				  },
				onDropdownHide: function(event) {
				}
			});
			callback();
		},
		
		/**
		 * hide columns in data table
		 * */
		fnHideColumns : function(tableId,hideColList){
			var tableObj = $(tableId).dataTable();
			$.each(hideColList,function(i,val){
				tableObj.fnSetColumnVis(val,false );
				gtl.fnApplyStyleToogleCol($(tableId).dataTable(),val);
			});
		},
		
		
		fnApplyStyleToogleCol :function(aoTable,inCol){			
		    var bcVis = aoTable.fnSettings().aoColumns[inCol].bVisible;
		    if(bcVis==true){
		    	$("#divToogle a#t"+inCol).removeClass("label-default");
		        $("#divToogle a#t"+inCol).addClass("label-success"); 
		        var i;
		        for (i = 1; i <= $("#divToogle").find('a').length-1; i++) {
		        	var flag = aoTable.fnSettings().aoColumns[i].bVisible;
		        	if (flag== false){		            	
		            	return;
		            }
		        }
		        $("#divToogle a#t0").removeClass("label-default");
		        $("#divToogle a#t0").addClass("label-success"); 
		    }else{
		        $("#divToogle a#t"+inCol).removeClass("label-success");
		        $("#divToogle a#t"+inCol).addClass("label-default");
		        $("#divToogle a#t0").removeClass("label-success");
		        $("#divToogle a#t0").addClass("label-default");
		    }
		    $("#divToogle a#t"+inCol).blur();
		    $("#divToogle a#t0").blur();
		},
		
		/**update columnfilter for data table columns after CRUD operations
		** Parameters:"
		* @tblId            	  String		id of table      
		* @hddnCols		      Array      	Default hidden column lists
		* @numCols               Integer      Total number of columns
		* @xcdCols               Array       	Excluded column filter lists
		* @hdrNameList			  Array			Header column list
		*/
		fnUpdateDTColFilter :function(tblId,hddnCols,numCols,xcdCols,hdrNameList){	
			var colList =[];
			for(var index=0;index<numCols;index++){
				colList.push(index);	
			}	
			gtl.fnShowColumns(tblId,colList);		
			gtl.fnSetDTColFilterPagination(tblId,numCols,xcdCols,hdrNameList);
			gtl.fnMultipleSelAndToogleDTCol('.search_init',function(){
				gtl.fnHideColumns(tblId,hddnCols);
			});
			gtl.fnSwitchToCurrentPage(tblId);
			gtl.fnRowColors(tblId);
		},
		//switch to cuurent page in data table after append row in add/edit 
		fnSwitchToCurrentPage :function(pTableName){
			var obTable =$(pTableName).dataTable();
			obTable.fnPageChange(tbPgNum);
		},
	
		
		/**
		 * @params
		 * iCol:	Integer	:Index of col
		 * @tableId: String : Table id
		 * */
		fnShowHide :function( iCol, tableId ){			
			var ooTable =$('#'+tableId).DataTable(); //Keep on current page after changing toogles
			var tPgNum =ooTable.page();			
		    var oTable = $('#'+tableId).dataTable();
		    var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
		    oTable.fnSetColumnVis( iCol, bVis ? false : true );
		    gtl.fnApplyStyleToogleCol(oTable,iCol);
		    oTable.fnPageChange(tPgNum);
		},
		
		/**
		 * @params
		 * col:	Integer	:Index of col
		 * @tableId: String : Table id
		 * */
		fnShowAll :function(cols,tableId){			
			var obTable =$('#'+tableId).DataTable();
			var taPgNum =obTable.page();			
			 var oTable = $('#'+tableId).dataTable(); 
			 $.each(cols,function(i,iCol){
				 var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				 oTable.fnSetColumnVis( iCol, bVis = true );
				 $("#divToogle a#t"+iCol).removeClass("label-default");
			     $("#divToogle a#t"+iCol).addClass("label-success");
			     $("#divToogle a#t0").removeClass("label-primary");
			     $("#divToogle a#t0").addClass("label-success");
			 });
			 oTable.fnPageChange(taPgNum);
		},

		fnShowColumns : function(tableId,colList){
			var tableObj = $(tableId).dataTable();
			$.each(colList,function(i,val){
				tableObj.fnSetColumnVis(val,true);
				 gtl.fnApplyStyleToogleCol($(tableId).dataTable(),i);
			});
		},

		
		
		/**
		 * sets data table options along with column filters and header names
		 * @params
		 * @tId 	:	String	: Table id
		 * */
		fnSetDataTable :function(tId,pNoOfCols){
			$(tId).on('order.dt',gtl.fnEnableTooltip).on( 'search.dt',gtl.fnEnableTooltip ).on( 'page.dt',gtl.fnEnableTooltip).DataTable({
				"pagingType": "simple_numbers",		
				"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
				"lengthMenu": [100 ],
				 "bLengthChange": false,
				 "bDestroy": true,
				 "bFilter": true,
				 "autoWidth": false,
				 "iDisplayLength": 10,
				 "stateSave": false,
				 "fnDrawCallback": function ( oSettings ) {
			            /* Need to redo the counters if filtered or sorted */
			            if ( oSettings.bSorted || oSettings.bFiltered ){
			                for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
			                    $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
			                }
			            }
			        },
			        "aoColumnDefs": [{
				        'bSortable': false,
				        'aTargets': ['nosort']
				    }],
				    "aaSorting": []
			});
		
			gtl.fnColSpanIfDTEmpty(tId,pNoOfCols);
			gtl.fnRowColors(tId);
		},
		
		/*colspan for empty data tables*/
		fnColSpanIfDTEmpty :function(tableId,aNoOfCols){
			/*var tId =tableId+".dataTables_empty";*/
			$(tableId).find('.dataTables_empty').prop('colspan',aNoOfCols);
			/*$(tId).prop('colspan',aNoOfCols);*/
		},
		
		

		/**
		 * append new row to respective data table
		 * @param table
		 * @param aRow
		 */
		fnAppendNewRow :function(table,aRow){
			var oaTable =$(table).DataTable();	
			tbPgNum=oaTable.page();
			oaTable.row.add($(aRow)).draw( false );
			var currentRows = oaTable.data().toArray();  // current table data
			var newRow=currentRows.pop();
			var info = $(table).DataTable().page.info();
			var startRow=info.start;
			currentRows.splice(startRow, 0,newRow );
			oaTable.clear();
			oaTable.rows.add(currentRows);
			
			oaTable.draw(false);
			
			gtl.fnRowColors(table);
		},
		
		/*delete row from data table*/
		fnDeleteRow :function(tableId,deleteRowId){
			var oDtable =$(tableId).DataTable();			
			tbPgNum=oDtable.page();			
			oDtable.row('#'+deleteRowId).remove().draw( false );
			
			gtl.fnRowColors(tableId);
		},
		
		/**
		 * 
		 * */
		
		fnShowAllCols : function(cols,tableId){
			
			var obTable =$('#'+tableId).DataTable();
			var taPgNum =obTable.page();
			
			 var oTable = $('#'+tableId).dataTable(); 
			 $.each(cols,function(i,iCol){
				 var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
				 oTable.fnSetColumnVis( iCol, bVis = true );
				 $("#divToogle a#t"+iCol).removeClass("label-default");
			     $("#divToogle a#t"+iCol).addClass("label-success");
			     $("#divToogle a#t0").removeClass("label-primary");
			     $("#divToogle a#t0").addClass("label-success");
			 });
			 oTable.fnPageChange(taPgNum);
		},
		
		
		
		
		
		
		/**
		 * Hide any alert divs and reset form
		 * @params
		 * @formId 	:	String	: Form Identity
		 * */
		fnResetForm :function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).data('formValidation').resetForm(true);
			
			$('.modal').on('shown.bs.modal', function (e) {
				$(formId).data('formValidation').resetForm(true);
			});
		},
		
		
		/**
		 * Hide any alert divs and reset form and form validation
		 * @params
		 * @formId 	:	String	: Form Identity
		 * @validateFields : Array : Array of Validate field names
		 * */
		fnResetFormValidation :function(formId,validateFields){
			$(formId).find('#alertdiv').hide();
			$('.modal').on('shown.bs.modal', function (e) {
				
				$.each(validateFields,function(index,field){
					$(formId).formValidation('updateStatus', field, 'NOT_VALIDATED').formValidation('validateField', field);
				});
			});
		},
		
		/**
		 * Hide any alert divs and make form validation  as VALID
		 * @params
		 * @formId 	:	String	: Form Identity
		 * @validateFields : Array : Array of Validate field names
		 * */
		fnUpdateFormValidation :function(formId,validateFields){
			$(formId).find('#alertdiv').hide();			
			$.each(validateFields,function(index,field){
				$(formId).formValidation('updateStatus', field, 'VALID').formValidation('validateField', field);
			});
		},
		
		
		
		
		
		
		/**
		 * show or hide modal
		 * @params
		 *  type : s => Show /h=> hide
		 * */
		fnShowOrHideModal :function (mdlId,type){
			if(type=='s'){
				$(mdlId).modal("show");
			}
			if(type=='h'){
				$(mdlId).modal("hide");
			}
		},

		escapeHtmlCharacters :function(text) {
			var map = {
					'&': '&amp;',
					'<': '&lt;',
					'>': '&gt;',			
					"'": '&#039;',
					' ': '&nbsp;',
					'©': '&#169;',
					'®': '&#174;',
					';': '&#59;',
					':': '&#58;',
					'=': '&#61;',
					'@': '&#64;',
					'`': '&#96;',
					'_': '&#95;',
					'[': '&#91;',
					']': '&#93;'
			};
			if(typeof text =='string'){
				text= text.replace(/[&<>"' ]/g, function(m) {
					return map[m];
				});
			}
			return text;
		},
		
		
		
		hideSpinner:function(){
			setTimeout(function(){$('#spinner').hide()},1000);
			
		},
		showSpinner:function(){
			$('#spinner').show();
		},
		
		
		
		fnHideDivs :function(aDivs){
			if(aDivs!=null && aDivs!='undefined'){
				if(aDivs.length>0){
					$.each(aDivs,function(index,ele){
						$(ele).prop('hidden',true);
					});	
				}	
			}
			
			
		},

		fnShowDivs :function(bDivs){
			if(bDivs!=null && bDivs!='undefined'){
				if(bDivs.length>0){
					$.each(bDivs,function(index,ele){
						$(ele).prop('hidden',false);
					});	
				}	
			}
			
			
		},
		
		
		
		
		
		
		
		fnRowColors :function(tId){
			var colors =['success','info','danger','warning'];
			var j=0;
			var nDatarows = $(tId).dataTable().fnGetNodes();
			$.each(nDatarows,function(i,row){
				if(j==colors.length){
					j=0;
				}
				$(row).addClass(colors[j++]);
			});
		},
		
		
		
		/**
		 * Get selected values from multiselect
		 * @params
		 * @elementId 	:	String	: Selector
		 * @returns : array
		 * */
		fnGetSelectedValues :function(elementId){
			var val =[];
			$(elementId).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()!="multiselect-all"){				
						val.push($(option).val());						
					}
				});
			});
			return val;
		},
		
		/**
		 * Get selected names from multiselect
		 * @params
		 * @elementId 	:	String	: Selector
		 * @returns : array
		 * */
		fnGetSelectedNames :function(elementId){
			var val =[];
			$(elementId).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()!="multiselect-all"){				
						val.push($(option).data('name').toString().trim());						
					}
				});
			});
			return val;
		},

		/** 
		 *	this function used to enable tooltip 
		 */

		fnEnableTooltip : function(){
			 $('[data-toggle="tooltip"]').tooltip();
		},
		
		fnDoReload :function(){
			$.ajax({
				type : "GET",
				url : "session/reload",
				success : function() {
				},
				error : function(data) {
				}
			});
		}
};
//$('#btnReload').on('click',gtl.fnDoReload);












/**do session invalidate once session timed out
especially for F5 keys........*/
var fnDoInvalidateSession =function(URL,type){
	$.ajax({
		type : type,
		url : URL,		
		async : false,		
		success : function(data) {
		},
		error : function(data) {
		}
	});
};