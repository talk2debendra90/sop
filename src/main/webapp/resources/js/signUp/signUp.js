
	
//signUp page elements
var signUpPage =new Object();
	signUpPage.div ='#divSignUp';
	signUpPage.signUpForm ='#divSignUp #frmSignUp';
	
	signUpPage.signUpFirstName ='#txtSignUpFirstName';
	signUpPage.signUpMiddlename ='#txtSignUpMiddleName';	
	signUpPage.signUpLastName ='#txtSignUpLastName';
	signUpPage.signUpEmail ='#txtSignUpEmail';
	signUpPage.signUpPassword ='#txtSignUpPassword';	
	
	signUpPage.signUpCountry ='#divSignUp #countrySelectBox';
	signUpPage.signUpMobile ='#phoneNumber';
		
	
	signUpPage.signUpSaveButton ='#btnSaveSignUp';
	signUpPage.signUpCancelButtom ='#btnSignUpCancel';
	

var signUpFormValidateFields =['txtSignUpFirstName','txtSignUpLastName','txtSignUpEmail','txtSignUpPassword'];
	
	
	
var signUp ={
			userId:0,
			
			firstName:'',
			middleName:'',
			lastName:'',
			
			email:'',
			password:'',
			country:'',
			mobile:'',
						
			ajaxURL:'',
			ajaxMethodType:'',
			ajaxContentType:'',
			ajaxRequestData :'',
			ajaxResponseData:'',
			
			
		init:function(){
			
			self =signUp;
			
			self.userId=0;
			
			
			self.firstName ='';
			self.middleName ='';
			self.lastName ='';
			
			self.email	='';
			self.password='';
			self.country='';
			self.mobile	='';
			
					
			self.ajaxURL='';
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
			self.initSignUpPage();
												
		},
				
			
		fnSave : function(){
			
			
			self.firstName =$(signUpPage.signUpFirstName).val().toString().trim().replace(/\u00a0/g," ");			
			self.middleName =$(signUpPage.signUpMiddlename).val()!=''?
										$(signUpPage.signUpMiddlename).val().toString().trim().replace(/\u00a0/g," "):'';
            self.lastName =$(signUpPage.signUpLastName).val().toString().trim().replace(/\u00a0/g," ");
			self.email	=$(signUpPage.signUpEmail).val().toString().trim().replace(/\u00a0/g," ");	
            self.password=$(signUpPage.signUpPassword).val().toString().trim().replace(/\u00a0/g," ");	
            self.mobile =$(signUpPage.signUpMobile).val()!=''?
							$(signUpPage.signUpMobile).val().toString().trim().replace(/\u00a0/g," "):'';
			self.country = $(signUpPage.signUpCountry).find('option:selected').val();
			self.mobile =self.country+'_'+self.mobile;
			
			
			
			ajaxRequestData ={
					"firstName":self.firstName,
					"middleName":self.middleName,
					"lastName": self.lastName,
					"email":self.email,
					"password": self.password,
					"mobileNumber":self.mobile ,
	
			};
			self.ajaxURL ="signup";
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			
			signUp.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
				
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
						fnSuccessAlert();
						window.location.href="";
					}else{
						showErrorAlert(signUpPage.div,ajaxResponseData.responseMessage);
				}	
			}					
		},
		
		
		
		initSignUpPage :function(){
			
			$(signUpPage.signUpFirstName).val('');
			$(signUpPage.signUpMiddlename).val('');
			$(signUpPage.signUpLastName).val('');
			gtl.fnSetMultiselect('#countrySelectBox');
		},
		
		
				
		
		 //ajax call
		
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			console.log("self.invokeAjax");
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
					gtl.hideSpinner();
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				}
			});
		},
		

		
	};
		

	
	
	 //show alert error divs on ajax response 
	
	function showErrorAlert(pMdlId,pErrorMsg){
		gtl.hideSpinner();
		
	}
	
	 // Show success mesage
	
	function fnSuccessAlert(){
		gtl.hideSpinner();
		$("#success-msg").snackbar("show");
	}

	
	
	

$(function() {
		signUp.init();		
		gtl.hideSpinner();

		$('#frmSignUp').formValidation({

			feedbackIcons : {
				validating : 'glyphicon glyphicon-refresh'
			},

			fields: {
				txtSignUpFirstName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'First name must be more than 2 and less than 30 characters long'
						},
						regexp: {							
							regexp: /^[^'?\"/\\]*$/,
							message: 'First name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtSignUpLastName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'Last name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Last name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtSignUpEmail: {
					validators: {
                        emailAddress: {
                            message: ' '
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: ' '
                        }
                    }
				},
				
				txtSignUpPassword: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 6,
							max: 10,
							message: 'Password must be more than 6 characters and less than 10 character long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Password can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				phoneNumber: {
					validators: {
                        phone: {
                            country: 'countrySelectBox',
                           /* message: 'The value is not valid %s phone number'*/
                            message: ' '
                        }
                  }
				},
				
				
				
				txtAddOrgName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'The organization  name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							/*regexp: /^[a-zA-Z0-9_\.]+$/,*/
							regexp: /^[^'?\"/\\]*$/,
							message: 'The organization name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtAddOrgCode: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'The organization code must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'The organization code can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
			}
		}).on('change', '[name="countrySelectBox"]', function(e) {
            $(signUpPage.signUpForm).formValidation('revalidateField', 'phoneNumber');
        }).on('success.form.fv', function(e) {
			e.preventDefault();
			signUp.fnSave();
		});
	});
