    var setNewPasswordContextElements =new Object();	
    setNewPasswordContextElements.div ='#divSetNewPassword';
    setNewPasswordContextElements.setNewPwdForm='#formSetNewPassword';
    setNewPasswordContextElements.newPassword= '#txtNewpassword';
    setNewPasswordContextElements.confirmPwd= '#txtconfirmPassword';
    
    var formSetNewValidateFields =['txtNewpassword','txtNewpassword'];
    


var pageContextElements =new Object();
  
    pageContextElements.formForgot ='#frmForgotPassword';
    pageContextElements.txtEmail='#txtEmail';
    pageContextElements.div='#forgotPasswordDiv';
       
	pageContextElements.sendBtn ='#sendBtn';
  
	var formValidateFields =['txtEmail'];


	var forgotPassword ={
			email:'',
			
			newPassword:'',
			confirmPassword:'',
			hashCode:'',
			
			ajaxURL:'',
			ajaxMethodType:'',
			ajaxContentType:'',
			ajaxRequestData :'',
			ajaxResponseData:'',
						
		init:function(){
			self=forgotPassword;
			
			self.email='';
			self.newPassword='';
			self.confirmPassword='';
			self.hashCode='';
			
			$(pageContextElements.addButton).on('click',self.initForgotModal);		
			self.ajaxURL='',
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
			
		},
		
	 
		fnSend : function(){		
			self.email =$(pageContextElements.txtEmail).val().toString().trim().replace(/\u00a0/g," ");
				
			ajaxRequestData ={
					"email":self.email
			};
			self.ajaxURL ="user/password/forgotPassword";
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			
			forgotPassword.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
				
				if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
					if(ajaxResponseData.response=="gtl-200"){					
						gtl.hideSpinner();
						fnSuccessAlert();
						//var tes= {"isForgotPassword":true};	
						//setTimeout(2000);
						window.location.href="home/fromForgotPassword";
					}else{
						showErrorAlert( pageContextElements.div,ajaxResponseData.responseMessage);
						gtl.hideSpinner();
					}	
				}					
		},
		
		
		fnSetNewForgotPassword :function(){
			self.newPassword =$(setNewPasswordContextElements.newPassword).val().toString().trim().replace(/\u00a0/g," ");
			self.confirmPassword =$(setNewPasswordContextElements.confirmPwd).val().toString().trim().replace(/\u00a0/g," ");
			self.hashCode=hashCode;	
						
			ajaxRequestData ={
					"hashCode": self.hashCode,	
				"newPassword" :self.newPassword,
				"confirmPassword":self.confirmPassword,
			};
			self.ajaxURL ="user/password/setNewPassword";
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			
			forgotPassword.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
			
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){						
					gtl.hideSpinner();
					//fnSuccessAlert();
					$('#success-msg').show();
					window.location.href="";
				}else{
					showErrorAlert(setNewPasswordContextElements.div,ajaxResponseData.responseMessage);
				}
			}
		},

		
		
		initForgotModal :function(){
			$(pageContextElements.txtEmail).val('');
			gtl.fnResetForm(pageContextElements.formForgot);
		},
	
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				},
			});
		}
	};
	

	/**
	 *show alert error divs on ajax response 
	 * */
	function showErrorAlert(pMdlId,pErrorMsg){
		$(pMdlId).find('#errorMessage').show();
		$(pMdlId).find('#exception').show();	
		$(pMdlId).find('#exception').text(pErrorMsg);
		gtl.hideSpinner();
	}
	/**
	 * Show success mesage
	 * */
	function fnSuccessAlert(){
		$('#success-msg').show();
		$("#success-msg").fadeOut(4000);
		gtl.fnResetForm(pageContextElements.formForgot);
	}


	$(function() {	
		
		forgotPassword.init();
		
		 $(pageContextElements.formForgot).formValidation({
			 
			 feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
			 },
			 
		     fields: {		    	
		            
		    	 txtEmail: {
		                validators: {
		                    notEmpty: {
		                        message: ' '
		                    },
		                    stringLength: {
		                        min: 2,
		                        max: 30,
		                        message: 'The Email is required'
		                    },
		                    regexp: {
		                    	regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
								message: 'The value is not a valid email address'
		                    }
		                }
		            },
		            
	            
		        }
		 }).on('success.form.fv', function(e) {
				e.preventDefault();
				forgotPassword.fnSend();
		});
	
	 //////////////////////////////////////////////////////////
		 
		 $(setNewPasswordContextElements.setNewPwdForm).formValidation({
		        fields: {
		        	txtNewpassword: {
						validators : {
							callback : {
								message : 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
								callback : function(value, validator,
										$field) {
									// var regex = new
									// RegExp(/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&*`~_:;<>.,?"'}{|!)(+=-]).*$/);
									var regex = new RegExp(
											/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
									return regex.test(value);
								}
							}
						}
					},
		            txtconfirmPassword: {
						validators : {
							notEmpty : {
								message : ' '
							},
							identical : {
								field : 'newpassword',
								message : 'The Password and its Confirm Password must be the same'
							}
						}
					}
				}
		    }).on('success.form.fv', function(e) {
				e.preventDefault();
				forgotPassword.fnSetNewForgotPassword();
			});
});

	
	
	
	
	
	
	
		
	











