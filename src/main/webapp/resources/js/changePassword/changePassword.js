var pageContextElements =new Object();
    pageContextElements.form='#changePasswordForm';
    pageContextElements.div='#changePasswordDiv';
    
    pageContextElements.currentPassword='#txtCurrentpassword';
    pageContextElements.newPassword='#txtNewpassword';
    pageContextElements.confirmPassword='#txtConfirmPassword'; 
    
	pageContextElements.addButton ='#btnAddchangePassword';
  
	var addFormValidateFields =['txtCurrentpassword','txtNewpassword','txtConfirmPassword'];


	var changePassword ={
			oldPassword:'',
			newPassword:'',	
			confirmPassword:'',
			ajaxURL:'',
			ajaxMethodType:'',
			ajaxContentType:'',
			ajaxRequestData :'',
			ajaxResponseData:'',
						
		init:function(){
			self=changePassword;
			
			self.currentPassword='';			
			self.newPassword='';
			self.confirmPassword='';
			
		
			$(pageContextElements.addButton).on('click',self.initAddModal);
			
			
			self.ajaxURL='',
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
						
		    $('.formChangePassword input[type="text"]').on('focus', function() {
		    	$(pageContextElements.div).find('.alertdiv').hide();
		    	$(pageContextElements.div).find('.alertdiv').hide();
		    });
			
		},
		
		/**
		 * Save new
		 * */	
		fnSave : function(){		
			self.currentPassword =$(pageContextElements.currentPassword).val().toString().trim().replace(/\u00a0/g," ");
			self.newPassword =$(pageContextElements.newPassword).val().toString().trim().replace(/\u00a0/g," ");
			self.confirmPassword =$(pageContextElements.confirmPassword).val().toString().trim().replace(/\u00a0/g," ");
										
			
					
			ajaxRequestData ={
					"oldPassword":self.currentPassword,
					"newPassword":self.newPassword,
					"confirmPassword":self.confirmPassword
			};
			self.ajaxURL ="user/password";
			self.ajaxMethodType ='PUT';
			self.ajaxContentType ='application/json';
			
			changePassword.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
				
				if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
					if(ajaxResponseData.response=="gtl-200"){					
						gtl.hideSpinner();
						gtl.fnShowOrHideModal(pageContextElements.div,'h');
						fnSuccessAlert();
						
					}else{
						showErrorAlert(pageContextElements.div,ajaxResponseData.responseMessage);
						gtl.hideSpinner();
					}	
				}					
		},
		
		
	
		initAddModal :function(){
			$(pageContextElements.currentPassword).val('');
			$(pageContextElements.newPassword).val('');
			$(pageContextElements.confirmPassword).val('');
			gtl.fnResetForm(pageContextElements.form);
		},
		
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				},
			});
		}
	};
		
	/////////////////////////////////////////////////////////

	

	/**
	 *show alert error divs on ajax response 
	 * */
	function showErrorAlert(pMdlId,pErrorMsg){
		$(pMdlId+' #alertdiv').show();
		$(pMdlId).find('#successMessage').hide();	
		$(pMdlId).find('#errorMessage').show();
		$(pMdlId).find('#exception').show();	
		$(pMdlId).find('#exception').text(pErrorMsg);
	}
	/**
	 * Show success mesage
	 * */
	function fnSuccessAlert(){
		$('#success-msg').show();
		$("#success-msg").fadeOut(3000);
		gtl.fnResetForm(pageContextElements.form);
	}


		
	$(function() {	
			/*nCols =5;
			gtl.fnSetDataTable(pageContextElements.table,nCols);
			$(pageContextDivs.table).show();*/
			
			changePassword.init();
			
			 $(pageContextElements.form).formValidation({

				 feedbackIcons : {
						validating : 'glyphicon glyphicon-refresh'
				 },
				 
			     fields: {
			    	 txtCurrentpassword: {
			                validators: {
			                    notEmpty: {
			                        message: ' '
			                    },
			                    /*stringLength: {
			                        min: 6,
			                        max: 30,
			                        message: 'The changePassword  name must be more than 2 and less than 30 characters long'
			                    },
			                    regexp: {
			                        regexp: /^[a-zA-Z0-9_\.]+$/,
			                    	regexp: /^[^'?\"/\\]*$/,
			                        message: 'The changePassword name can only consist of alphabetical, number, dot and underscore'
			                    }*/
			                }
			            },
			            txtNewpassword: {
			                validators: {
			                    notEmpty: {
			                        message: ' '
			                    },
			                    stringLength: {
			                        min: 6,
			                        max: 30,
			                        message: 'The changePassword  length must more than 6 and less than 15 characters long'
			                    },
			                    regexp: {
			                    	regexp: /^[^'?\"/\\]*$/,
			                        message: 'The changePassword code can only consist of alphabetical, number, dot and underscore'
			                    }
			                }
			            },
			            txtConfirmPassword: {
			                validators: {
			                    notEmpty: {
			                        message: ' '
			                    },
			                    stringLength: {
			                        min: 6,
			                        max: 30,
			                        message: 'The changePassword length must more than 6 and less than 15 characters long'
			                    },
			                    regexp: {
			                    	regexp: /^[^'?\"/\\]*$/,
			                        message: 'The changePassword code can only consist of alphabetical, number, dot and underscore'
			                    }
			                }
			            },
			            
		            
			        }
			 }).on('success.form.fv', function(e) {
					e.preventDefault();
					changePassword.fnSave();
			});
			 
			
	});














