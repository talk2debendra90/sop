
var pageContextDivs =new Object();	
	pageContextDivs.table ='#tableDiv';



var pageContextElements =new Object();
	pageContextElements.addButton ='#btnAddUser';
	pageContextElements.table ='#tblUser';
	pageContextElements.thLabels =['#','Name','Email  ','Mobile ','Category ','Role ','Action'];
	pageContextElements.noOfCols=7;
	pageContextElements.excludeColFilter =[0,6];
	
	
/*add modal elements*/
var addModal =new Object();
	addModal.modal ='#mdlAddUser';
	addModal.addUserForm ='#mdlAddUser #frmAddUser';
	
	addModal.userFirstName ='#txtUserFirstName';
	addModal.userMiddlename ='#txtUserMiddleName';	
	addModal.userLastName ='#txtUserLastName';
	addModal.userEmail ='#txtAddUserEmail';
	addModal.userPassword ='#txtAddUserPassword';	
	
	addModal.userCountry ='#mdlAddUser #countrySelectBox';
	addModal.userCountryDiv ='#mdlAddUser #divCountry';
	addModal.userMobile ='#phoneNumber';
	
	
	
	addModal.entity ='#mdlAddUser #entity';	
	addModal.divEntity ='#mdlAddUser #divEntity';
	
	addModal.division ='#mdlAddUser #division';	
	addModal.divDivision ='#mdlAddUser #divDivision';
	
	
	addModal.department ='#mdlAddUser #department';	
	addModal.divDepartment ='#mdlAddUser #divDepartment';
	
	
	
	addModal.role ='#mdlAddUser #role';	
	addModal.divRole ='#mdlAddUser #divRole';
	
	addModal.userSaveButton ='#btnSaveUser"';
	addModal.userCancelButton ='#btnAddUserCancel';
	


var addFormValidateFields =['txtUserFirstName','txtUserLastName','txtAddUserEmail','txtAddUserPassword','department'];
	
/*edit modal elements*/
var editModal =new Object();
	editModal.modal ='#mdlEditUser';
	editModal.editUserForm ='#mdlEditUser #frmEditUser';
		
	editModal.userFirstName ='#txtEditUserFirstName';
	editModal.userMiddlename ='#txtEditUserMiddleName';	
	editModal.userLastName ='#txtEditUserLastName';
	//editModal.userEmail ='#txtEditUserEmail';		
		
	editModal.userCountry ='#mdlEditUser #editCountrySelectBox';
	editModal.userMobile ='#mdlEditUser #editPhoneNumber';
	
	
		
	editModal.department ='#mdlEditUser #department';	
	editModal.divDepartment ='#mdlEditUser #divDepartment';
	
	editModal.role ='#mdlEditUser #role';	
	editModal.divRole ='#mdlEditUser #divRole';
		
		
	editModal.userSaveButton ='#btnEditSaveUser"';
	editModal.userCancelButton ='#btnEditUserCancel';


	var editFormValidateFields =['txtEditUserFirstName','txtEditUserMiddleName','txtEditUserLastName','editPhoneNumber','department'];


/*delete modal elements*/
var deleteModal =new Object();
	deleteModal.modal ='#mdlDeleteUser';
	deleteModal.message ='#msgDelete';
	deleteModal.deleteButton ='#btnDeleteUser';
	deleteModal.deleteCancelButton ='#btnDeleteCancel';

/**
 * Error alert modal 
 * */	
var alertModal = new Object();
	alertModal.modal ='#mdlError';
	alertModal.message ='#alertMsg';
		
	
var user ={
			userId:0,
			roleId:0,
			roleCode:'',
			roleName:'',
			categoryId:0,
			
			orgId :0,
			entityId:0,
			entityName:'',
			
			divisionId:0,
			divisionName:'',
			
			departmentId:0,
			departmentName:'',
						
			firstName:'',
			middleName:'',
			lastName:'',
			
			email:'',
			password:'',
			country:'',
			mobile:'',
			isAdmin:false,
						
			ajaxURL:'',
			ajaxMethodType:'',
			ajaxContentType:'',
			ajaxRequestData :'',
			ajaxResponseData:'',
			
			sequenceId:'',
			maxRoleLevel:'',
			
		init:function(){
			console.log("self.init");
			
			self =user;
			self.categoryId =0;
			
			self.orgId=userOrgId;
			
			
			self.sequenceId=parseInt(sequenceId);
			self.userId=0;
			self.departmentId =0;			
			self.departmentName ='';
			self.roleId =0;
			self.roleCode ='',
			
			self.firstName ='';
			self.middleName ='';
			self.lastName ='';
			
			self.email	='';
			self.password='';
			self.country='';
			self.mobile	='';
			self.isAdmin=false;
								
			
			$(pageContextElements.addButton).on('click',self.initAddModal);
			$(deleteModal.deleteButton).on('click',self.fnDelete);		   
			
			
			self.ajaxURL='';
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
			
			maxRoleLevel ='',
			
			
			
			
		    $('.frmAddUser input[type="text"],.frmEditUser input[type="text"]').on('focus', function() {
		    	$(addModal.modal).find('.alertdiv').hide();
		    	$(editModal.modal).find('.alertdiv').hide();
		    });
		    $('.frmAddUser select,.frmEditUser select').on('change',function(){
		    	$(addModal.modal).find('.alertdiv').hide();
		    	$(editModal.modal).find('.alertdiv').hide();
		    	gtl.fnUpdateFormValidation(addModal.modal,['department']);
		    	gtl.fnUpdateFormValidation(editModal.modal,['department']);
		    });
		    		    		  
		    self.fnGetRoles();
		},
		
		/**
		 * Save new
		 * */	
		fnSave : function(){
			console.log("self.fnSave");
			
			//get category level identity
			if(((self.maxRoleLevel).toLowerCase()=='oadm')){
				self.categoryId =self.orgId;
			}else if(((self.maxRoleLevel).toLowerCase()=='eadm')){
				self.categoryId =$(addModal.entity).find('option:selected').val();
			}else if(((self.maxRoleLevel).toLowerCase()=='dvadm')){
				self.categoryId =$(addModal.division).find('option:selected').val();
			}else if(((self.maxRoleLevel).toLowerCase()=='dpadm') || ((self.maxRoleLevel).toLowerCase()=='tech') || ((self.maxRoleLevel).toLowerCase()=='lrnr')){
				self.categoryId =$(addModal.department).find('option:selected').val();
			}
			
			console.log("self-categoryId:"+self.categoryId);
			
			self.firstName =$(addModal.userFirstName).val().toString().trim().replace(/\u00a0/g," ");			
			self.middleName =$(addModal.userMiddlename).val()!=''?
										$(addModal.userMiddlename).val().toString().trim().replace(/\u00a0/g," "):'';
            self.lastName =$(addModal.userLastName).val().toString().trim().replace(/\u00a0/g," ");
			self.email	=$(addModal.userEmail).val().toString().trim().replace(/\u00a0/g," ");	
            self.password=$(addModal.userPassword).val().toString().trim().replace(/\u00a0/g," ");	
            self.mobile =$(addModal.userMobile).val()!=''?
							$(addModal.userMobile).val().toString().trim().replace(/\u00a0/g," "):'';
			self.country = $(addModal.userCountry).find('option:selected').val();
			self.mobile =self.country+'_'+self.mobile;
			
			self.roleId = $(addModal.role).find('option:selected').val();
			self.roleCode =self.maxRoleLevel;
			
			ajaxRequestData ={
					"firstName":self.firstName,
					"middleName":self.middleName,
					"lastName": self.lastName,
					"email":self.email,
					"password": self.password,
					"mobileNumber":self.mobile ,
					"roleId":self.roleId,
					"roleCode":self.roleCode,
					"categoryId":self.categoryId
			};
			self.ajaxURL ="user";
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
				
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
						var newRow =fnCreateNewRow(ajaxResponseData);					
						gtl.fnAppendNewRow(pageContextElements.table,newRow);
						gtl.fnShowOrHideModal(addModal.modal,'h');
						fnSuccessAlert();
					}else{
						showErrorAlert(addModal.modal,ajaxResponseData.responseMessage);
				}	
			}					
		},
		
		
		initEdit: function(userId,firstName,middleName,lastName,country,mobile,departmentId,roleId,isAdmin){
			
			console.log("self.initEdit");
			
			self.userId = userId;
			self.departmentId =departmentId;
			self.roleId=roleId;
			
			self.firstName =firstName;			
			self.middleName =middleName;									
            self.lastName =lastName;

            self.country=country;
            self.mobile =mobile;
            self.isAdmin =isAdmin
                        
            $(editModal.userFirstName).val(firstName);
            $(editModal.userMiddlename).val(middleName);
            $(editModal.userLastName).val(lastName);
            
            $(editModal.userCountry).val(country);
            $(editModal.userMobile).val(mobile);
            if(isAdmin =="true")
            	$(editModal.userIsAdmin).prop('checked',true);
            else
            	$(editModal.userIsAdmin).prop('checked',false);
            gtl.fnResetFormValidation(editModal.editUserForm,editFormValidateFields);
            
//          /  self.fnBindDepartment(editModal.department,parseInt(departmentId,10));			
            
            self.fnBindRoles(editModal.role,parseInt(roleId));
		
            gtl.fnSetMultiselect(editModal.userCountry);
           
			gtl.fnShowOrHideModal(editModal.modal,'s');
			
		},
		
		fnEdit :function(){
			
			console.log("self.fnEdit");
			
			self.departmentId =$(editModal.department).find('option:selected').val();
			self.roleId =$(editModal.role).find('option:selected').val();
			self.firstName =$(editModal.userFirstName).val().toString().trim().replace(/\u00a0/g," ");
			self.middleName =$(editModal.userMiddlename).val()!=''?
										$(editModal.userMiddlename).val().toString().trim().replace(/\u00a0/g," "):'';
            self.lastName =$(editModal.userLastName).val().toString().trim().replace(/\u00a0/g," ");
            self.mobile =$(editModal.userMobile).val()!=''?
							$(editModal.userMobile).val().toString().trim().replace(/\u00a0/g," "):'';
			self.country = $(editModal.userCountry).find('option:selected').val();
			self.mobile =self.country+'_'+self.mobile;
			self.isAdmin =$(editModal.userIsAdmin).prop('checked');
			
			
			ajaxRequestData ={
					"userId":self.userId,
					"firstName":self.firstName,
					"middleName":self.middleName,
					"lastName": self.lastName,
					"mobileNumber":self.mobile,
					"isAdmin":self.isAdmin,
					"isSuperAdmin":false,
					"departmentId":self.departmentId,
					"roleId":self.roleId
			};
			self.ajaxURL ="user";
			self.ajaxMethodType ='PUT';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
			
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){						
					var newRow =fnCreateNewRow(ajaxResponseData);
					gtl.fnDeleteRow(pageContextElements.table,self.userId);
					gtl.fnAppendNewRow(pageContextElements.table,newRow);
					gtl.fnShowOrHideModal(editModal.modal,'h');
					fnSuccessAlert();
				}else{
					showErrorAlert(editModal.modal,ajaxResponseData.responseMessage);
				}
			}
		},
		
		
		initDelete:function(pUserId,pUName){
			console.log("self.initDelete");
			
			self.userId	=pUserId;
			
			$(deleteModal.message).text("Do you really want to delete "+pUName+ " ?");
			gtl.fnShowOrHideModal(deleteModal.modal,'s');
			$(deleteModal.modal).find('.alertdiv').hide();
		},
		
		fnDelete :function(){
			console.log("self.fnDelete"+self.userId);
			
			self.ajaxURL ="user/"+self.userId;
			self.ajaxMethodType ='DELETE';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
					gtl.fnDeleteRow(pageContextElements.table,self.userId);
					gtl.fnShowOrHideModal(deleteModal.modal,'h');
					fnSuccessAlert();
				}else{
					showErrorAlert(deleteModal.modal,ajaxResponseData.responseMessage);
				}
			}
		},
		
		
		initAddModal :function(){
			
			console.log("self.initAddModal");
			$(addModal.userFirstName).val('');
			$(addModal.userMiddlename).val('');
			$(addModal.userLastName).val('');
			gtl.fnResetForm(addModal.addUserForm);
			
			self.fnBindRoles(addModal.role,0);
			gtl.fnSetMultiselect(addModal.userCountry);
			
			gtl.fnHideDivs([addModal.divEntity,addModal.divDivision,addModal.divDepartment]);
								
		},
		
		
		
		
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			console.log("self.invokeAjax");
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
					gtl.hideSpinner();
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				}
			});
		},
		
		
		/**
		 * Get Roles

		 * */

		fnGetRoles :function(){			
			self.ajaxURL ="role/"+sequenceId;
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			self.roleList =ajaxResponseData;
			console.log("Role List:"+self.roleList);
		},

		
		
		/**
		 * Bind Roles 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */
		fnBindRoles :function(pSelector,selectedValue){
			gtl.fnEmptyAllSelectBox(pSelector);
			$ele = $(pSelector);
			var data =self.roleList;
			$.each(data, function(index, obj) {
				if(obj.id ==selectedValue){
					$ele.append('<option value="'+obj.id+'" data-id="'+obj.id+'"data-code="'+obj.code+'" selected>'+gtl.escapeHtmlCharacters(obj.name)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.id+'" data-id="'+obj.id+'"data-code="'+obj.code+'">'+gtl.escapeHtmlCharacters(obj.name)+'</option>');
				}			
			});
			self.fnMultiselect(pSelector);
			gtl.fnCollapseMultiselect();
		},
		

		/**
		 * 
		 * Multi-select Initialization and invocation
		 * 
		 * */
		fnMultiselect	:function(ele){
			$(ele).multiselect('destroy');
			$(ele).multiselect({
				maxHeight: 150,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,
				filterPlaceholder: 'Search here...',
				onChange : function(event) {
					var isEmpty=true;
					if (ele == (addModal.role)) {
						self.maxRoleLevel ='';						
						self.maxRoleLevel =$(addModal.role).find('option:selected').data('code');
						console.log("Role:"+self.maxRoleLevel);
						
						
						if ($(addModal.role).find('option:selected').length != 0 && (self.maxRoleLevel).toLowerCase()!='oadm' ) {							
							isEmpty =utility.fnBindEntities(addModal.entity,0,'','',self.orgId,[addModal.divEntity],[addModal.divDivision,addModal.divDepartment]);
							self.fnMultiselect(addModal.entity);
							if(isEmpty==true){
								gtl.fnHideDivs([addModal.divEntity,addModal.divDivision,addModal.divDepartment]);
								$(alertModal.message).text('There are no entity of the selected user. Please add entity first.');				
								gtl.fnShowOrHideModal(alertModal.modal,'s');
							}
						}
					}else if(ele == (addModal.entity)){
						if(((self.maxRoleLevel).toLowerCase()=='dvadm')||
								((self.maxRoleLevel).toLowerCase()=='dpadm') || ((self.maxRoleLevel).toLowerCase()=='tech') || ((self.maxRoleLevel).toLowerCase()=='lrnr')){
							isEmpty =utility.fnBindDivisions(addModal.division,0,'','',$(addModal.entity).find('option:selected').val(),[addModal.divDivision],[addModal.divDepartment]);
							self.fnMultiselect(addModal.division);
							if(isEmpty==true){
								gtl.fnHideDivs([addModal.divDivision,addModal.divDepartment]);
								$(alertModal.message).text('There are no division of the selected entity. Please add division first.');				
								gtl.fnShowOrHideModal(alertModal.modal,'s');
							}
						}
					}else if(ele == (addModal.division)){
						if(((self.maxRoleLevel).toLowerCase()=='dpadm') || ((self.maxRoleLevel).toLowerCase()=='tech') || ((self.maxRoleLevel).toLowerCase()=='lrnr')){
							isEmpty =utility.fnBindDepartment(addModal.department,0,'','',$(addModal.division).find('option:selected').val(),[addModal.divDepartment],[]);
							self.fnMultiselect(addModal.department);
							if(isEmpty==true){
								gtl.fnHideDivs([addModal.divDepartment]);
								$(alertModal.message).text('There are no department of the selected division. Please add department first.');				
								gtl.fnShowOrHideModal(alertModal.modal,'s');
							}
						}
					}
				}
			});
			gtl.fnCollapseMultiselect();
		},
		
		
		
		
		
		
		
	};
		
	
	






	

	/**
	 * create a new row and return
	 * */

	function fnCreateNewRow(aData){
		var q="&quot;,&quot;";
		var row = 
			"<tr id='"+aData.userId+"' data-id='"+aData.userId+"'>"+
			"<td></td>"+
			"<td title='"+aData.userName+"'>"+aData.userName+"</td>"+
			"<td title='"+aData.email+"'>"+aData.email+"</td>"+
			"<td title='"+aData.mobileNumber+"'>"+aData.mobileNumber+"</td>"+
			"<td title='"+aData.categoryName+"'>"+aData.categoryName+"</td>"+
			"<td title='"+aData.roleName+"'>"+aData.roleName+"</td>"+
			"<td><div class='div-tooltip'>" +
					 "<a class='tooltip tooltip-top' id='btnEditDivision'" +
					 "onclick=user.initEdit(&quot;"+aData.userId+q+aData.firstName+q+aData.middleName+q+aData.lastName+q+aData.country+q+aData.mobileNumber+q+aData.departmentId+q+aData.roleId+q+aData.isAdmin+"&quot;);>" +
					 "<span class='tooltiptext'>Edit</span>" +
					 "<span class='glyphicon glyphicon-edit font-size12'></span></a>" +
				     "<a style='margin-left:10px;' class='tooltip tooltip-top' " +
				     "onclick=user.initDelete(&quot;"+aData.userId+q+gtl.escapeHtmlCharacters(aData.userName)+"&quot;);>" +
				     "<span class='tooltiptext'>Delete</span>" +
				     "<span class='glyphicon glyphicon-trash font-size12' ></span></a>" +
				     "</div>" +
			  "</td>"+
			"</tr>";
		return row;
	}
	
	
	
	
	
	/**
	 *show alert error divs on ajax response 
	 * */
	function showErrorAlert(pMdlId,pErrorMsg){
		$(pMdlId+' #alertdiv').show();
		$(pMdlId).find('#successMessage').hide();	
		$(pMdlId).find('#errorMessage').show();
		$(pMdlId).find('#exception').show();
		$(pMdlId).find('#exception').text(pErrorMsg);
		
		$(pMdlId).find('.submit').removeClass('disabled');
		$(pMdlId).find('.submit').prop('disabled',false);
		
		gtl.hideSpinner();
		
	}
	/**
	 * Show success mesage
	 * */
	function fnSuccessAlert(){
		gtl.hideSpinner();
		$('#success-msg').show();
		$("#success-msg").fadeOut(3000);
	}

	
	
	

	$( document ).ready(function() {
		gtl.fnSetDTColFilterPagination(pageContextElements.table,pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
		gtl.fnMultipleSelAndToogleDTCol('.search_init',function(){
			gtl.fnHideColumns(pageContextElements.table,[]);
		});
		$(pageContextDivs.table).prop('hidden',false);

		user.init();

		gtl.fnColSpanIfDTEmpty(pageContextElements.table,pageContextElements.noOfCols);
		gtl.hideSpinner();


		$(addModal.addUserForm).formValidation({

			feedbackIcons : {
				validating : 'glyphicon glyphicon-refresh'
			},

			fields: {
				txtUserFirstName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'First name must be more than 2 and less than 30 characters long'
						},
						regexp: {							
							regexp: /^[^'?\"/\\]*$/,
							message: 'First name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtUserLastName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'Last name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Last name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtAddUserEmail: {
					validators: {
						emailAddress: {
							message: ' '
						},
						regexp: {
							regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
							message: ' '
						}
					}
				},

				txtAddUserPassword: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 6,
							max: 10,
							message: 'Password must be more than 6 characters and less than 10 character long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Password can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				phoneNumber: {
					validators: {
						phone: {
							country: 'countrySelectBox',
							/* message: 'The value is not valid %s phone number'*/
							message: ' '
						}
					}
				},
			}
		}).on('change', '[name="countrySelectBox"]', function(e) {
			$(addModal.addUserForm).formValidation('revalidateField', 'phoneNumber');
		}).on('success.form.fv', function(e) {
			e.preventDefault();
			user.fnSave();
		});



		$(editModal.editUserForm).formValidation({
			fields: {
				txtEditUserFirstName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'First name must be more than 2 and less than 30 characters long'
						},
						regexp: {							
							regexp: /^[^'?\"/\\]*$/,
							message: 'First name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtEditUserLastName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'Last name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Last name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},

				editPhoneNumber: {
					validators: {
						phone: {
							country: 'countrySelectBox',
							message: 'The value is not valid %s phone number'

						}
					}
				},

			}
		}).on('change', '[name="countrySelectBox"]', function(e) {
			$(editModal.editUserForm).formValidation('revalidateField', 'editPhoneNumber');
		}).on('success.form.fv', function(e) {
			e.preventDefault();
			user.fnEdit();
		});
	});
