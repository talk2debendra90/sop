var pageContextDivs =new Object();	
	pageContextDivs.table ='#tableDiv';



var pageContextElements =new Object();
	pageContextElements.addButton ='#btnAddUser';
	pageContextElements.table ='#tblUser';
	pageContextElements.thLabels =['#','Name','Email  ','Mobile ','Department ','Action'];
	pageContextElements.noOfCols=6;
	pageContextElements.excludeColFilter =[0,5];
	
	
/*add modal elements*/
var addModal =new Object();
	addModal.modal ='#mdlAddUser';
	addModal.addUserForm ='#mdlAddUser #frmAddUser';
	
	addModal.userFirstName ='#txtUserFirstName';
	addModal.userMiddlename ='#txtUserMiddleName';	
	addModal.userLastName ='#txtUserLastName';
	addModal.userEmail ='#txtAddUserEmail';
	addModal.userPassword ='#txtAddUserPassword';	
	
	addModal.userCountry ='#mdlAddUser #countrySelectBox';
	addModal.userCountryDiv ='#mdlAddUser #divCountry';
	addModal.userMobile ='#phoneNumber';
	
	addModal.userIsAdminDiv ='#mdlAddUser #divIsAdmin';
	addModal.userIsAdmin ='#mdlAddUser #chkboxIsAdmin';
	
	
	
	addModal.department ='#mdlAddUser #department';	
	addModal.divDepartment ='#mdlAddUser #divDepartment';
	
	
	addModal.role ='#mdlAddUser #role';	
	addModal.divRole ='#mdlAddUser #divRole';
	
	addModal.userSaveButton ='#btnSaveUser"';
	addModal.userCancelButton ='#btnAddUserCancel';
	


var addFormValidateFields =['txtUserFirstName','txtUserLastName','txtAddUserEmail','txtAddUserPassword','department'];
	
/*edit modal elements*/
var editModal =new Object();
	editModal.modal ='#mdlEditUser';
	editModal.editUserForm ='#mdlEditUser #frmEditUser';
		
	editModal.userFirstName ='#txtEditUserFirstName';
	editModal.userMiddlename ='#txtEditUserMiddleName';	
	editModal.userLastName ='#txtEditUserLastName';
	//editModal.userEmail ='#txtEditUserEmail';		
		
	editModal.userCountry ='#mdlEditUser #editCountrySelectBox';
	editModal.userMobile ='#mdlEditUser #editPhoneNumber';
		
	editModal.userIsAdminDiv ='#mdlEditUser #divIsAdmin';
	editModal.userIsAdmin ='#mdlEditUser #chkboxIsAdmin';
		
		
		
	editModal.department ='#mdlEditUser #department';	
	editModal.divDepartment ='#mdlEditUser #divDepartment';
	
	editModal.role ='#mdlEditUser #role';	
	editModal.divRole ='#mdlEditUser #divRole';
		
		
	editModal.userSaveButton ='#btnEditSaveUser"';
	editModal.userCancelButton ='#btnEditUserCancel';


	var editFormValidateFields =['txtEditUserFirstName','txtEditUserMiddleName','txtEditUserLastName','editPhoneNumber','department'];


/*delete modal elements*/
var deleteModal =new Object();
	deleteModal.modal ='#mdlDeleteUser';
	deleteModal.message ='#msgDelete';
	deleteModal.deleteButton ='#btnDeleteUser';
	deleteModal.deleteCancelButton ='#btnDeleteCancel';

	
	
var user ={
			userId:0,
			departmentId:0,
			departmentName:'',
			roleId:0,
			firstName:'',
			middleName:'',
			lastName:'',
			
			email:'',
			password:'',
			country:'',
			mobile:'',
			isAdmin:false,
						
			ajaxURL:'',
			ajaxMethodType:'',
			ajaxContentType:'',
			ajaxRequestData :'',
			ajaxResponseData:'',
			
			sequenceId:'',
			
		init:function(){
			console.log("self.init");
			
			self =user;
			
			self.sequenceId=parseInt(sequenceId);
			self.userId=0;
			self.departmentId =0;			
			self.departmentName ='';
			self.roleId =0;
			
			self.firstName ='';
			self.middleName ='';
			self.lastName ='';
			
			self.email	='';
			self.password='';
			self.country='';
			self.mobile	='';
			self.isAdmin=false;
								
			
			$(pageContextElements.addButton).on('click',self.initAddModal);
			$(deleteModal.deleteButton).on('click',self.fnDelete);		   
			
			
			self.ajaxURL='';
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
						
			
		    $('.frmAddUser input[type="text"],.frmEditUser input[type="text"]').on('focus', function() {
		    	$(addModal.modal).find('.alertdiv').hide();
		    	$(editModal.modal).find('.alertdiv').hide();
		    });
		    $('.frmAddUser select,.frmEditUser select').on('change',function(){
		    	$(addModal.modal).find('.alertdiv').hide();
		    	$(editModal.modal).find('.alertdiv').hide();
		    	gtl.fnUpdateFormValidation(addModal.modal,['department']);
		    	gtl.fnUpdateFormValidation(editModal.modal,['department']);
		    });
		    
		    
		    self.fnGetDepartment();
		    self.fnGetRoles();
		},
		
		/**
		 * Save new
		 * */	
		fnSave : function(){
			console.log("self.fnSave");
			
			self.departmentId	=$(addModal.department).find('option:selected').val();
			self.firstName =$(addModal.userFirstName).val().toString().trim().replace(/\u00a0/g," ");			
			self.middleName =$(addModal.userMiddlename).val()!=''?
										$(addModal.userMiddlename).val().toString().trim().replace(/\u00a0/g," "):'';
            self.lastName =$(addModal.userLastName).val().toString().trim().replace(/\u00a0/g," ");
			self.email	=$(addModal.userEmail).val().toString().trim().replace(/\u00a0/g," ");	
            self.password=$(addModal.userPassword).val().toString().trim().replace(/\u00a0/g," ");	
            self.mobile =$(addModal.userMobile).val()!=''?
							$(addModal.userMobile).val().toString().trim().replace(/\u00a0/g," "):'';
			self.country = $(addModal.userCountry).find('option:selected').val();
			self.mobile =self.country+'_'+self.mobile;
			self.roleId= $(addModal.role).find('option:selected').val();
			ajaxRequestData ={
					"firstName":self.firstName,
					"middleName":self.middleName,
					"lastName": self.lastName,
					"email":self.email,
					"password": self.password,
					"mobileNumber":self.mobile ,
					"roleId":self.roleId,
					"isSuperAdmin":false,
					"departmentId":self.departmentId
			};
			self.ajaxURL ="user";
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
				
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
						var newRow =fnCreateNewRow(ajaxResponseData);					
						gtl.fnAppendNewRow(pageContextElements.table,newRow);
						gtl.fnShowOrHideModal(addModal.modal,'h');
						fnSuccessAlert();
					}else{
						showErrorAlert(addModal.modal,ajaxResponseData.responseMessage);
				}	
			}					
		},
		
		
		initEdit: function(userId,firstName,middleName,lastName,country,mobile,departmentId,roleId,isAdmin){
			
			console.log("self.initEdit");
			
			self.userId = userId;
			self.departmentId =departmentId;
			self.roleId=roleId;
			
			self.firstName =firstName;			
			self.middleName =middleName;									
            self.lastName =lastName;

            self.country=country;
            self.mobile =mobile;
            self.isAdmin =isAdmin
                        
            $(editModal.userFirstName).val(firstName);
            $(editModal.userMiddlename).val(middleName);
            $(editModal.userLastName).val(lastName);
            
            $(editModal.userCountry).val(country);
            $(editModal.userMobile).val(mobile);
            if(isAdmin =="true")
            	$(editModal.userIsAdmin).prop('checked',true);
            else
            	$(editModal.userIsAdmin).prop('checked',false);
            gtl.fnResetFormValidation(editModal.editUserForm,editFormValidateFields);
            
            self.fnBindDepartment(editModal.department,parseInt(departmentId,10));			
            
            self.fnBindRoles(editModal.role,parseInt(roleId));
		
            gtl.fnSetMultiselect(editModal.userCountry);
           
			gtl.fnShowOrHideModal(editModal.modal,'s');
			
		},
		
		fnEdit :function(){
			
			console.log("self.fnEdit");
			
			self.departmentId =$(editModal.department).find('option:selected').val();
			self.roleId =$(editModal.role).find('option:selected').val();
			self.firstName =$(editModal.userFirstName).val().toString().trim().replace(/\u00a0/g," ");
			self.middleName =$(editModal.userMiddlename).val()!=''?
										$(editModal.userMiddlename).val().toString().trim().replace(/\u00a0/g," "):'';
            self.lastName =$(editModal.userLastName).val().toString().trim().replace(/\u00a0/g," ");
            self.mobile =$(editModal.userMobile).val()!=''?
							$(editModal.userMobile).val().toString().trim().replace(/\u00a0/g," "):'';
			self.country = $(editModal.userCountry).find('option:selected').val();
			self.mobile =self.country+'_'+self.mobile;
			self.isAdmin =$(editModal.userIsAdmin).prop('checked');
			
			
			ajaxRequestData ={
					"userId":self.userId,
					"firstName":self.firstName,
					"middleName":self.middleName,
					"lastName": self.lastName,
					"mobileNumber":self.mobile,
					"isAdmin":self.isAdmin,
					"isSuperAdmin":false,
					"departmentId":self.departmentId,
					"roleId":self.roleId
			};
			self.ajaxURL ="user";
			self.ajaxMethodType ='PUT';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
			
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){						
					var newRow =fnCreateNewRow(ajaxResponseData);
					gtl.fnDeleteRow(pageContextElements.table,self.userId);
					gtl.fnAppendNewRow(pageContextElements.table,newRow);
					gtl.fnShowOrHideModal(editModal.modal,'h');
					fnSuccessAlert();
				}else{
					showErrorAlert(editModal.modal,ajaxResponseData.responseMessage);
				}
			}
		},
		
		
		initDelete:function(pUserId,pUName){
			console.log("self.initDelete");
			
			self.userId	=pUserId;
			
			$(deleteModal.message).text("Do you really want to delete "+pUName+ " ?");
			gtl.fnShowOrHideModal(deleteModal.modal,'s');
			$(deleteModal.modal).find('.alertdiv').hide();
		},
		
		fnDelete :function(){
			console.log("self.fnDelete"+self.userId);
			
			self.ajaxURL ="user/"+self.userId;
			self.ajaxMethodType ='DELETE';
			self.ajaxContentType ='application/json';
			
			user.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			if(ajaxResponseData!=null && ajaxResponseData!='undefined'){
				if(ajaxResponseData.response=="gtl-200"){
					gtl.fnDeleteRow(pageContextElements.table,self.userId);
					gtl.fnShowOrHideModal(deleteModal.modal,'h');
					fnSuccessAlert();
				}else{
					showErrorAlert(deleteModal.modal,ajaxResponseData.responseMessage);
				}
			}
		},
		
		
		initAddModal :function(){
			
			console.log("self.initAddModal");
			$(addModal.userFirstName).val('');
			$(addModal.userMiddlename).val('');
			$(addModal.userLastName).val('');
			gtl.fnResetForm(addModal.addUserForm);
			
			self.fnBindDepartment(addModal.department,0);
			self.fnBindRoles(addModal.role,0);
			
			gtl.fnSetMultiselect(addModal.userCountry);
								
		},
		
		
		
		
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			console.log("self.invokeAjax");
			gtl.showSpinner();
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
					gtl.hideSpinner();
				},
				error : function(e) {
					gtl.hideSpinner();
					return false;
				}
			});
		},
		

		/**
		 * Bind Department 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */
		fnBindDepartment :function(pSelector,selectedValue){
			gtl.fnEmptyAllSelectBox(pSelector);
			$ele = $(pSelector);
			var data =self.departmentList;
			$.each(data, function(index, obj) {
				if(obj.departmentId ==selectedValue){
					$ele.append('<option value="'+obj.departmentId+'" data-id="'+obj.departmentId+'"data-name="'+obj.departmentName+'" selected>'+gtl.escapeHtmlCharacters(obj.departmentName)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.departmentId+'" data-id="'+obj.departmentId+'"data-name="'+obj.departmentName+'">'+gtl.escapeHtmlCharacters(obj.departmentName)+'</option>');
				}			
			});
			gtl.fnSetMultiselect(pSelector);
			gtl.fnCollapseMultiselect();
		},
		
		
		
		/**
		 * Bind Roles 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */
		fnBindRoles :function(pSelector,selectedValue){
			gtl.fnEmptyAllSelectBox(pSelector);
			$ele = $(pSelector);
			var data =self.roleList;
			$.each(data, function(index, obj) {
				if(obj.id ==selectedValue){
					$ele.append('<option value="'+obj.id+'" data-id="'+obj.id+'"data-name="'+obj.name+'" selected>'+gtl.escapeHtmlCharacters(obj.name)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.id+'" data-id="'+obj.id+'"data-name="'+obj.name+'">'+gtl.escapeHtmlCharacters(obj.name)+'</option>');
				}			
			});
			gtl.fnSetMultiselect(pSelector);
			gtl.fnCollapseMultiselect();
		},
		
		/**
		 * Get Department 
		 * 
		 * */
		fnGetDepartment :function(){			
			self.ajaxURL ="department/user/list";
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			self.departmentList =ajaxResponseData;
			console.log("Department List:"+self.departmentList);
			
		},
		
		
		/**
		 * Get Roles
		 * */
		fnGetRoles :function(){			
			self.ajaxURL ="role/"+sequenceId;
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			self.roleList =ajaxResponseData;
			console.log("Role List:"+self.roleList);
			
		}
		
		
		
		
		
		
	};
		
	
	






	

	/**
	 * create a new row and return
	 * */

	function fnCreateNewRow(aData){
		var q="&quot;,&quot;";
		var row = 
			"<tr id='"+aData.userId+"' data-id='"+aData.userId+"'>"+
			"<td></td>"+
			"<td title='"+aData.userName+"'>"+aData.userName+"</td>"+
			"<td title='"+aData.email+"'>"+aData.email+"</td>"+
			"<td title='"+aData.mobileNumber+"'>"+aData.mobileNumber+"</td>"+
			"<td title='"+aData.departmentName+"'>"+aData.departmentName+"</td>"+			
			"<td><div class='div-tooltip'>" +
					 "<a class='tooltip tooltip-top' id='btnEditDivision'" +
					 "onclick=user.initEdit(&quot;"+aData.userId+q+aData.firstName+q+aData.middleName+q+aData.lastName+q+aData.country+q+aData.mobileNumber+q+aData.departmentId+q+aData.roleId+q+aData.isAdmin+"&quot;);>" +
					 "<span class='tooltiptext'>Edit</span>" +
					 "<span class='glyphicon glyphicon-edit font-size12'></span></a>" +
				     "<a style='margin-left:10px;' class='tooltip tooltip-top' " +
				     "onclick=user.initDelete(&quot;"+aData.userId+q+gtl.escapeHtmlCharacters(aData.userName)+"&quot;);>" +
				     "<span class='tooltiptext'>Delete</span>" +
				     "<span class='glyphicon glyphicon-trash font-size12' ></span></a>" +
				     "</div>" +
			  "</td>"+
			"</tr>";
		return row;
	}
	
	
	
	
	
	/**
	 *show alert error divs on ajax response 
	 * */
	function showErrorAlert(pMdlId,pErrorMsg){
		$(pMdlId+' #alertdiv').show();
		$(pMdlId).find('#successMessage').hide();	
		$(pMdlId).find('#errorMessage').show();
		$(pMdlId).find('#exception').show();
		$(pMdlId).find('#exception').text(pErrorMsg);
		
		$(pMdlId).find('.submit').removeClass('disabled');
		$(pMdlId).find('.submit').prop('disabled',false);
		
		gtl.hideSpinner();
		
	}
	/**
	 * Show success mesage
	 * */
	function fnSuccessAlert(){
		gtl.hideSpinner();
		$('#success-msg').show();
		$("#success-msg").fadeOut(3000);
	}

	
	
	

$( document ).ready(function() {
	gtl.fnSetDTColFilterPagination(pageContextElements.table,pageContextElements.noOfCols,pageContextElements.excludeColFilter,pageContextElements.thLabels);
	gtl.fnMultipleSelAndToogleDTCol('.search_init',function(){
		gtl.fnHideColumns(pageContextElements.table,[]);
	});
	$(pageContextDivs.table).prop('hidden',false);

		user.init();

		gtl.fnColSpanIfDTEmpty(pageContextElements.table,pageContextElements.noOfCols);
		gtl.hideSpinner();


		$(addModal.addUserForm).formValidation({

			feedbackIcons : {
				validating : 'glyphicon glyphicon-refresh'
			},

			fields: {
				txtUserFirstName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'First name must be more than 2 and less than 30 characters long'
						},
						regexp: {							
							regexp: /^[^'?\"/\\]*$/,
							message: 'First name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtUserLastName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'Last name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Last name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtAddUserEmail: {
					validators: {
                        emailAddress: {
                            message: ' '
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: ' '
                        }
                    }
				},
				
				txtAddUserPassword: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 6,
							max: 10,
							message: 'Password must be more than 6 characters and less than 10 character long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Password can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				phoneNumber: {
					 validators: {
	                        phone: {
	                            country: 'countrySelectBox',
	                           /* message: 'The value is not valid %s phone number'*/
	                            message: ' '
	                        }
	                  }
				},
			}
		}).on('change', '[name="countrySelectBox"]', function(e) {
            $(addModal.addUserForm).formValidation('revalidateField', 'phoneNumber');
        }).on('success.form.fv', function(e) {
			e.preventDefault();
			user.fnSave();
		});



		$(editModal.editUserForm).formValidation({
			fields: {
				txtEditUserFirstName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'First name must be more than 2 and less than 30 characters long'
						},
						regexp: {							
							regexp: /^[^'?\"/\\]*$/,
							message: 'First name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
				txtEditUserLastName: {
					validators: {
						notEmpty: {
							message: ' '
						},
						stringLength: {
							min: 2,
							max: 30,
							message: 'Last name must be more than 2 and less than 30 characters long'
						},
						regexp: {
							regexp: /^[^'?\"/\\]*$/,
							message: 'Last name can only consist of alphabetical, number, dot and underscore'
						}
					}
				},
		
				editPhoneNumber: {
					 validators: {
	                        phone: {
	                            country: 'countrySelectBox',
	                            message: 'The value is not valid %s phone number'
	                            
	                        }
	                  }
				},
			
			}
		}).on('change', '[name="countrySelectBox"]', function(e) {
            $(editModal.editUserForm).formValidation('revalidateField', 'editPhoneNumber');
		}).on('success.form.fv', function(e) {
			e.preventDefault();
			user.fnEdit();
		});
	});
