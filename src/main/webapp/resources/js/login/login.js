var pageContextElements =new Object();
	pageContextElements.userName ='#txtUserName';
	pageContextElements.password ='#txtPassword';
	pageContextElements.form ='#frmLogIn';


var user={
		
		init:function(){
			userName ='';
			password ='';
		},
		
		
		login:function(e){
			userName =$(pageContextElements.userName).val();
			password =$(pageContextElements.password).val();
			var loginRequest={"login":userName,"password":password};
			var isValid=false;
			$('.login-form').find('input[type="text"], input[type="password"], textarea').each(function(){
	    		if( $(this).val() == "" ) {
	    			e.preventDefault();
	    			$(this).addClass('input-error');
	    			isValid=false;
	    			return false;
	    		}else {
	    			$(this).removeClass('input-error');
	    			isValid =true;	    			
	    		}
	    	});
			if(isValid==true){
				ajaxCall(loginRequest);
			}
		},
		
		signup:function(){
			
		},
		
		forgotPassword:function(){
			
		}
};

var ajaxCall =function(pLoginRequest){
	$.ajax({
		url : 'signin',
		type : "POST",				
		contentType:"application/json",
        data :JSON.stringify(pLoginRequest ),
		success : function(response) {
			console.log(response)
		    if(response.response=="sop-200"){				    	
		    	window.location = 'question/build';
		    }else{
		    	$(pageContextElements.form).find('#errorMessage').show();
				$(pageContextElements.form).find('#exception').text("Credentials does not match.Please try again.");
		    }
		    
		},
		error : function(e) {
		}
	});
}



jQuery(document).ready(function() {
	
	user.init();
	
    /*
        Fullscreen background
    */
    $.backstretch("web-resources/images/login/pageContext.jpg");
    
    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    	$(pageContextElements.form).find('#errorMessage').hide();
    });
});
