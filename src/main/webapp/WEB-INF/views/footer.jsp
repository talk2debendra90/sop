   <footer class="footer" style="background-color:#3f51b5;">
      <div class="container">
        <div class="col-xs-6" style="text-align:left;padding-left:0px;">
          <p style="color: #fff;margin-top: 13px;font-size: 12px;">Copyright � 2016 by Deb. All rights reserved</p>
        </div>
        <div class="col-xs-4 col-xs-offset-2" style="text-align:right;padding-right:0px;padding-left:0px;">
          <p style="color: #fff;margin-top: 8px;font-size: 9px;text-align:justify;">
          The information contained or any portion 
          thereof may not be reproduced or used in 
          any manner whatsoever without the express written
           permission of SOP.</p>
        </div>

      </div>     
    </footer>

  </body>
</html>



	 <!-- CSS -->       
     <link rel="stylesheet" href="web-resources/css/datatable/jquery.dataTables.min.css">
     <link rel="stylesheet" href="web-resources/css/datatable/material.min.css">
     <link rel="stylesheet" href="web-resources/css/datatable/dataTables.material.min.css">
     
     <link rel="stylesheet"	href="web-resources/css/multiselect/bootstrap-multiselect.css"type="text/css" />
	

	<!-- JS -->
	
	<script src="web-resources/js/lib/jquery/jquery-2.1.1.min.js"></script>
	<script src="web-resources/js/lib/jquery/jquery-ui.min.1.11.3.js"></script>
	<script src="web-resources/js/lib/jquery/jquery.blockUI.js"></script>
	<script src="web-resources/js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="web-resources/js/lib/bootstrap/material.js"></script>
	<script src="web-resources/js/lib/bootstrap/ripples.js"></script>
	<script src="web-resources/js/lib/multiselect/bootstrap-multiselect.js"></script>
	
	<!-- SnackBar -->
	<script src="web-resources/js/lib/snackbar/snackbar.min.js"></script>
	
	
	<!-- FormValidation -->
	<script src="web-resources/js/lib/formValidation/formValidation.min.js"></script>
	<script src="web-resources/js/lib/bootstrap/bootstrap.min.frm.js"></script>
	
	
	<!-- DataTable -->
	<script src="web-resources/js/lib/datatable/jquery.dataTables.min.js"></script>
	<script src="web-resources/js/lib/datatable/datatable.columnfilter.js"></script>
	<script src="web-resources/js/lib/datatable/dataTables.material.min.js"></script>