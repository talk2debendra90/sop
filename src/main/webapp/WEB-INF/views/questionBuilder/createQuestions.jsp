<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page trimDirectiveWhitespaces="true" %>

<jsp:include page="/WEB-INF/views/header.jsp" />

<style>

h1 { padding: .2em; margin: 0; }
#products { float:left; width:200px; height: 600px; margin-right: 20px; }
#products ul {list-style: disc; padding: 1em 0 1em 3em;}
.shoppingCart{ width: 200px; margin: 20px; float: left; }
/* style the list to maximize the droppable hitarea */
.shoppingCart ol { margin: 0; padding: 1em 0 1em 3em; list-style-type: decimal;  }


</style>


<div id="products">
    <h1 class="ui-widget-header">Blocks</h1>
    <div class="ui-widget-content">
        <ul>
            <li data-id="1" class = "credit"> 10000$ </li>
            <li data-id="2"class = "debit"> -10000$ </li>
            <li data-id="3" class = "credit"> 10000$ </li>
            <li data-id="4"> -10000$ </li>
            <li data-id="5" class = "credit"> Bank </li>
            <li data-id="6"> Loan </li>
        </ul>
    </div>
</div>

<div id="shoppingCart1" class="shoppingCart">
    <h1 class="ui-widget-header">Cresit Side</h1>
    <div class="ui-widget-content">
        <ol>
            <li class="placeholder">Add your items here</li>
        </ol>
    </div>
</div>
<div id="shoppingCart2" class="shoppingCart">
    <h1 class="ui-widget-header">Debit side</h1>
    <div class="ui-widget-content">
        <ol>
            <li class="placeholder">Add your items here</li>
        </ol>
    </div>
</div>



















 
<jsp:include page="/WEB-INF/views/footer.jsp" />


<script>

$("#products li").draggable({
    appendTo: "body",
    helper: "clone"
});
$("#shoppingCart1 ol").droppable({
    activeClass: "ui-state-default",
    hoverClass: "ui-state-hover",
    accept:".credit",
    drop: function(event, ui) {
        var self = $(this);
        self.find(".placeholder").remove();
        var productid = ui.draggable.attr("data-id");
        if (self.find("[data-id=" + productid + "]").length) return;
        $("<li></li>", {
            "text": ui.draggable.text(),
            "data-id": productid
        }).appendTo(this);
        // To remove item from other shopping chart do this
        var cartid = self.closest('.shoppingCart').attr('id');
        $(".shoppingCart:not(#"+cartid+") [data-id="+productid+"]").remove();
    }
}).sortable({
    items: "li:not(.placeholder)",
    sort: function() {
        $(this).removeClass("ui-state-default");
    }
});
$("#shoppingCart2 ol").droppable({
    activeClass: "ui-state-default",
    hoverClass: "ui-state-hover",
    accept:".debit",
    drop: function(event, ui) {
        var self = $(this);
        self.find(".placeholder").remove();
        var productid = ui.draggable.attr("data-id");
        if (self.find("[data-id=" + productid + "]").length) return;
        $("<li></li>", {
            "text": ui.draggable.text(),
            "data-id": productid
        }).appendTo(this);
        // To remove item from other shopping chart do this
        var cartid = self.closest('.shoppingCart').attr('id');
        $(".shoppingCart:not(#"+cartid+") [data-id="+productid+"]").remove();
    }
}).sortable({
    items: "li:not(.placeholder)",
    sort: function() {
        $(this).removeClass("ui-state-default");
    }
});
</script>
