<div class="questionContent panel-body" id="shortTextQuestion">

	<div class="questionLabel text-muted text-muted-less-margin">
		<small>Short text</small>
	</div>

	<div id="shortTextQuestionTab">
				<form class="questionTab" id="shortTextQuestionForm" method="post" role="form">
					
					
					<div class="form-group" hidden="true">
						<input class="form-control" id="surveyId" name="surveyId" rows="3" cols="" type="text" hidden="true">
					</div>
					
					
					<div class="form-group" hidden="true">
						<input class="form-control" id="survey-question-auto-id" value="0" name="surveyQuestionAutoId" rows="3" type="text" hidden="true">
					</div>
					
					
					<div class="row text-muted text-muted-less-margin">
						<div class="col-xs-12">
							
							<div class="row edit-block">
								<div class="col-xs-12">
									
								</div>
							</div>            
		                             
					    <%-- <div class="row margin-left-right"> 
					          <div class="timerContentWrapper">
						         <div class="checkbox text-uppercase text text-muted text-muted-less-margin small-checkbox-label">
										<div class="col-xs-8 text-left" style="padding-left:0px;"><label style="padding:0px;">Timer required</label></div>
										<div class="col-xs-4 text-right" style="padding-right:0px;"><input id="${questionProperties.timerRequiredForQuestion}" class="options-checkbox timerRequired" 
										name="timerRequiredForQuestion" value="timerRequiredForQuestion" type="checkbox"></div>
								 </div>
								 
								 <div class="durationDiv">
									<div class="text text-uppercase text text-muted text-muted-less-margin small-checkbox-label">Timer Duration </div> 
									<div class="text-uppercase text text-muted text-muted-less-margin small-checkbox-label">
									<label class="normalFont"><input id="${questionProperties.timerDuration}" style="width :50%;display:inline-block;" 
										   class="timerDuration form-control" value="timerDuration" type="number" disabled>&nbsp Sec.</label></div>	
								 </div>        
							  </div>    
						</div> --%>
						
					<div class="row edit-block">
					<div class="col-xs-12">
					<label class="block-label">Settings</label>
					<div class="row">
						<div class="checkbox text-uppercase text-muted text-muted-less-margin small-checkbox-label">
							<div class="col-xs-8 text-left" ><label class="left-padding-0">Mandatory</label></div>
							<div class="col-xs-4 text-right"><input id="isMandatory" name="isMandatory" data-size="mini" value="isMandatory" type="checkbox"></div>
						</div> 
					</div>
					
					<div class="row assesmentModeWrapper">		
						<div class="">
							<div class="checkbox text-uppercase text-muted text-muted-less-margin small-checkbox-label">
								<div class="col-xs-8 text-left" ><label class="left-padding-0">Score</label></div>
								<div class="col-xs-4 text-right"><input id="${questionProperties.compareResponse}" class="compareResponse" data-size="mini" name="compareResponse" type="checkbox"></div>
							</div>
							<label class="weightageValueLabel col-xs-12">
									<input id="${questionProperties.weightage}" class="weightage form-control" value="1" type="text">
							</label>	
						</div>		
					</div>
					</div>
					</div>
				</div>
				</div>
			</form>
	

			<div class="saveAndCancelQuestion">
				<button type="submit" class="btn btn-primary save-question save-button-qb" onClick="ShortTextQuestion.saveShortTextQuestion(this)">Save</button>
				<button type="button" class="cancel-button btn btn-link cancel-question">Cancel</button>
				<button type="button" title="Previous" class="btn text-muted  pull-left prev-qsn-prev ajs-tooltip" tipsy-gravity="s"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
				<button type="button" title="Next" class="btn text-muted pull-left next-qsn-prev ajs-tooltip" tipsy-gravity="s"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
			</div>
	</div>
</div>
