<div class="questionContent panel-body" id="welcomeQuestion">
		
		<div class="questionLabel text-muted text-muted-less-margin">
			<small>Welcome</small>
			<!-- <span class="text-danger"><small>Welcome screen should be the first screen in your survey. Please do not place it in the middle.</small></span> -->
		</div>
		
		
	<div id="informationQuestionTab">
		<form id="welcomeQuestionForm" method="post" role="form" class="questionTab">

					<div class="form-group" hidden="true">
						<input class="form-control" id="question-id" name="questionId"
							rows="3" cols="" type="text" hidden="true">
					</div>

					<div class="form-group" hidden="true">
						<input class="form-control" id="survey-question-auto-id" value="0"
							name="surveyQuestionAutoId" rows="3" type="text" hidden="true">
					</div>


					<div class="row text-muted text-muted-less-margin">
						<div class="col-xs-12">
							<div class="form-group qsn-verbiage edit-block row"> 
							  <div class="col-xs-12">
								 <div class="text-muted text-muted-less-margin" > 
	                                <small class="control-label" >TITLE</small><sup class="astricForMandatory">*</sup>
	                             </div>     
	                             <div class=" text-muted text-muted-less-margin" >                             
	                                 <textarea data-bv-field="questionVerbiage" 
										class="form-control" id="question-verbiage"
										name="questionVerbiage" rows="3"
										data-bv-notempty="true"  maxlength="1024" 
										data-bv-notempty-message="Question is required"></textarea>
								</div> 
								<div class=" text-muted text-muted-less-margin" > 	
								 	<div id="textarea_feedback"></div>
								</div>
								</div>
								</div>
							 <div class="form-group qsn-desc edit-block row"> 
							  <div class="col-xs-12">
								<div class="vert-spacer-10"></div>
								<!-- <div class="text-muted text-muted-less-margin" > 
	                                <small class="control-label">DESCRIPTION</small>
	                             </div>   --> 
	                              <div class="row">
								<div class="text-muted text-muted-less-margin"> 
	                               <div class="col-xs-8 text-left"> <small class="control-label">DESCRIPTION</small></div>
	                               <div class="col-xs-4 text-right left-padding-0"><input id="${questionProperties.hideQuestionDesc}" class="questionPreviewDesc" name="" data-size="mini" value="" type="checkbox"></div>
	                            </div>   
	                            </div>
	                             <div class=" text-muted text-muted-less-margin" id="questionDescDiv">                             
	                                 <textarea data-bv-field="questionDescription" type="text"
										class="form-control" id="question-description"
										name="questionDescription" rows="2" maxlength="4096"></textarea>
								</div> 
								<div class=" text-muted text-muted-less-margin" > 	
								 	<div id="textarea_description"></div>
								</div>                           
                            </div>
                            </div>
                             <div class="row edit-block">
								<div class="col-xs-12">					
							</div>
                            </div>
                            
                        <%-- <div class="row">
							<div class="backgroundImageCheckbox"> 
					         	 <div class="checkbox text-uppercase text text-muted text-muted-less-margin small-checkbox-label">
									<div class="col-xs-8 text-left" ><label class="left-padding-0">Background image</label></div>
									<div class="col-xs-4 text-right"><input id="isBgImage" name="isBgImage" type="checkbox" data-size="mini" ></div>	
								</div>
							</div>
							
							<div class="backgroundImageContentWrapper col-xs-12" id="backgroundImageCollapse">
								<div id="question-thumbnail" class="backgroundImageThumbnail">
									<div id="${questionProperties.backgroundImage}" class="background-image">
										<a title="" data-original-title="" href="#"
											class="btn btn-default button-no-padding"
											id="background-popover" data-toggle="popover"
											data-trigger="click" data-placement="left" data-trigger="manual"><img
											id="background-thumbnail-url"
											src="web-resources/img/blank.png"
											data-trigger="manual" data-color-dominant=""
											data-background-thumbnail-id="0" style="max-height: 66px">
										</a>
									</div>
								</div>
							</div> 
						</div>
					
						<div class="row">
							<div class="audiocontentWrapper">
								<div class="checkbox text-uppercase text text-muted text-muted-less-margin small-checkbox-label">
									<div class="col-xs-8 text-left" ><label class="left-padding-0">Audio</label></div>
									<div class="col-xs-4 text-right"><input id="${questionProperties.questionAudio}" data-propertyid='0' data-size="mini" class="isAudioAssist" name="isAudioAssist" value="questionAudio" type="checkbox"></div>
								
									<div class="uploadCollapse col-xs-12">
										<label>
											<a class="audioUploadButton" >UPLOAD</a><span class="glyphicon audio-loader glyphicon-repeat normal-right-spinner"></span>
										</label>
										<div class="audioUrlDiv">
											<p><span class="help-block"></span></p>
										</div>
									</div>
								</div>
							</div> 
						</div> --%>
 				<div class="row edit-block">
					<div class="col-xs-12">
					<label class="block-label">Settings</label>   
                         <div class="row">    
		                    <div class="buttonTextWraper">
								<div class="checkbox text-uppercase text text-muted text-muted-less-margin small-checkbox-label">
									<div class="col-xs-5 text-left" ><label class="left-padding-0" style="padding-top: 5px;">Button Text</label></div>
									<div class="col-xs-7 text-right"><input id="${questionProperties.buttonText}" maxlength="24" class="buttonText form-control"
									 	 value="${editSurveyVM.surveyProperties.continueButtonText}" type="text">
									</div>
								</div>	
							</div>
						</div>
						</div>
						</div>
						
               		 </div>
				</div>
			</form>
	
		<div class="saveAndCancelQuestion">
			<button type="submit" class="btn btn-primary save-question save-button-qb" onClick="WelcomeQuestion.saveWelcomeQuestion(this)">Save</button>
			<button type="button" class="cancel-button btn btn-link cancel-question">Cancel</button>
			<!-- <button type="button" class="btn btn-primary pull-right next-qsn-prev">></button>
			<button type="button" class="btn btn-primary pull-right prev-qsn-prev"><</button> -->
		</div>
	</div>
</div>