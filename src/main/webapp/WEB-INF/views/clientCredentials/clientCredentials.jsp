	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	
	
	<%@ include file="/WEB-INF/views/header.jsp" %>



<title>Client Credentials</title>

<!-- Fixed navbar -->
<div class="container">

	<div class="page-header">
		<div class="row">
			<div class="col-xs-6">
				<h1>Client Credentials</h1>
			</div>
		</div>			
		
	</div>

	<div class="row">
	<div class="row">
		<div class="col-xs-6"></div>
		<div class="col-xs-6"></div>
	</div>
		

	
<div class="col-xs-12">
	<div class="jumbotron col-xs-6 col-xs-offset-2" id="changePasswordDiv" >		
			<div class="form-group">
				<label class="control-label" for="txtCurrentpassword">Client ID:</label>
				<input type="text" class="form-control"
					name="txtCurrentpassword" id="txtCurrentpassword" data-fv-notempty
					 value="${clientId}">
			</div>
			<div class="form-group">
				<label class="control-label" for="txtNewpassword">Client Secret</label>
				<input type="text" class="form-control" name="txtNewpassword"
					id="txtNewpassword"  value="${clientSecret}" >
			</div>
			

			
	</div>
	</div>	
	</div>
	</div>
	



<%@ include file="/WEB-INF/views/footer.jsp"%>

