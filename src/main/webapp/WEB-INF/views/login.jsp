	<?xml version="1.0" encoding="UTF-8" ?>
	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SOP(Survey On Palm)</title>
		<link type="image/x-icon" rel="shortcut icon" href="web-resources/images/favicon.ico">
        <!-- CSS -->
       
        <link rel="stylesheet" href="web-resources/css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="web-resources/font-awesome/css/font-awesome.min.css">
	    <!-- JS -->
        <script src="web-resources/js/lib/jquery/jquery-2.1.1.min.js"></script>
   

        
	
	
	<title>Login</title>
	
	
	<!-- CSS -->
	<link rel="stylesheet" href="web-resources/css/login/form-elements.css">
	<link rel="stylesheet" href="web-resources/css/login/login.css">
	
	<script src="web-resources/js/lib/jquery/jquery.backstretch.min.js"></script>
	<script src="web-resources/js/login/login.js"></script>
</head>
<body>

 	<div class="container">
		<!-- Top content -->
	        <div class="top-content">
	            <div class="inner-bg">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-sm-8 col-sm-offset-2 text">
	                            <h1><strong>SOP(Survey On Palm)</strong></h1>
	                            <div class="description">
	                            	<p>
		                            	
	                            	</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-sm-6 col-sm-offset-3 form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Login to SOP(Survey On Palm)</h3>
	                            		<p>Enter your username and password to log on:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="" method="post" class="login-form" id="frmLogIn">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" name="form-username" placeholder="Email Id..." 
				                        		class="form-username form-control" id="txtUserName">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="form-password" placeholder="Password..."
				                        	 class="form-password form-control" id="txtPassword">
				                        </div>
				                        
				                        <div class="form-group">
											<div class="alert alert-danger" id="errorMessage"
												 style="display: none; width: 100%; margin-left: 0px; border-width: 2px;">
												 <span id="exception"></span>
											</div>
										</div>
				                        
				                        
				                        
				                        <button type="button" class="btn btnLogin" id="btnSignIn" onclick="user.login(event);">Sign in!</button>
				                        <a href="signup" class="">Register</a>
				                        <a href="user/password/forgot" class="pull-right">Forgot password?</a>
				                        <br/>
				                        
				                    </form>
			                    </div>
						</div>
	                    </div>
	                    <div class="row">
	                        <div class="col-sm-6 col-sm-offset-3 social-login">
	                        	<h3>...or login with:</h3>
	                        	<div class="social-login-buttons">
		                        	<a class="btn btn-link-1 btn-link-1-facebook" href="#">
		                        		<i class="fa fa-facebook"></i> Facebook
		                        	</a>
		                        	<a class="btn btn-link-1 btn-link-1-twitter" href="#">
		                        		<i class="fa fa-twitter"></i> Twitter
		                        	</a>
		                        	<a class="btn btn-link-1 btn-link-1-google-plus" href="#">
		                        		<i class="fa fa-google-plus"></i> Google Plus
		                        	</a>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div><!-- top-content -->
	</div> <!-- container -->
</body>
</html>
