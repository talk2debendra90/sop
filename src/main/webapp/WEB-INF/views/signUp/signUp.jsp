
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="/WEB-INF/views/header.jsp" %>



	<!-- container -->
      <div class="container" id="divSignUp">	
      
      
      
      <!-- page header -->
	<div class="page-header">
		<div class="row">
			<div class="col-xs-5">
				<h1>Register</h1>
			</div>


			<div class="col-xs-5 ">
				<div id="success-msg" data-content="Account created successfully."
					data-style="toast" data-timeout="1500" data-html-allowed="true">
				</div>
			</div>
		</div>
	   </div><!--// page header -->	
			
			<div class="row panel">
			<div class="modal-body ">
				<form role="form" id="frmSignUp" class="frmAddUser form" style="min-height:400px;">

					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtSignUpFirstName">First Name *</label>
						<input  type="text" class="form-control" id="txtSignUpFirstName" name="txtSignUPFirstName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtSignUpMiddleName">Middle Name</label>						
						<input  type="text" class="form-control" id="txtSignUpMiddleName" name="txtSignUPMiddleName"/>
					</div>
					
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtSignUpLastName">Last Name *</label>
						<input  type="text" class="form-control" id="txtSignUpLastName" name="txtSignUPLastName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtSignUpEmail">Email *</label>
						<input  type="text"  class="form-control"  id="txtSignUpEmail" name="txtSignUPEmail"
								data-fv-notempty data-fv-notempty-message=" "/>						
					</div>
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtSignUpPassword">Password *</label>
						<input  type="password"  class="form-control"  id="txtSignUpPassword" name="txtSignUPPassword"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					

					<div class="form-group col-xs-4">
						<label class="control-label">Country</label><br>
						<select class="form-control select" name="countrySelectBox" id="countrySelectBox">
								<option value="US">United States</option>
								<option value="BG">Bulgaria</option>
								<option value="BR">Brazil</option>
								<option value="CN">China</option>
								<option value="CZ">Czech Republic</option>
								<option value="DK">Denmark</option>
								<option value="FR">France</option>
								<option value="DE">Germany</option>
								<option value="IN" selected>India</option>
								<option value="MA">Morocco</option>
								<option value="NL">Netherlands</option>
								<option value="PK">Pakistan</option>
								<option value="RO">Romania</option>
								<option value="RU">Russia</option>
								<option value="SK">Slovakia</option>
								<option value="ES">Spain</option>
								<option value="TH">Thailand</option>
								<option value="AE">United Arab Emirates</option>
								<option value="GB">United Kingdom</option>
								<option value="VE">Venezuela</option>
						</select>
						
					</div>
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label">Phone number</label>
						<input type="text" class="form-control" name="phoneNumber"  id="phoneNumber"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					
					
					
				
					
					

					<div id="alertdiv" class="alertdiv col-xs-12">
						
						<div class="alert alert-danger" id="errorMessage" hidden="true">
							<strong>Failure!</strong> Record could not be saved. <br> 
							<span id="exception"></span>
						</div>
					</div>
					
					<div class="btn-group col-xs-12"  id="buttonGroup">
						<button type="submit" class="btn btn-primary btnSaveSignUP submit"
							style="display: block;" id="btnSaveSignUP" >Save</button>
						<button type="button" data-dismiss="modal" class="btn btn-link"
							style="display: block;" id="btnSignUPCancel" onclick="window.location.href='' ">Cancel</button>
					</div>
					
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
				</form>
				</div>
				</div>
			
			
	  </div>
			
		<!-- </div> -->
	<!-- </div> -->
<!-- </div> -->

<%@ include file="/WEB-INF/views/footer.jsp" %>

	
	
	<script src="web-resources/js/signUp/signUp.js"></script>

