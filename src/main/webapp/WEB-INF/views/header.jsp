	<?xml version="1.0" encoding="UTF-8" ?>
	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
	
	<%@page import="com.celesco.sop.util.SessionVariables"%>	
	
<!DOCTYPE html>
<html>
    <head>
	
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SOP(Survey On Palm)</title>
		<link type="image/x-icon" rel="shortcut icon" href="web-resources/images/favicon.ico">
		<base href="${pageContext.request.contextPath}/" />
        
        
        <!-- CSS -->    
		<!-- Material Design fonts -->
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700"/>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons"/>
		<!-- Bootstrap material-->		
		<link rel="stylesheet" href="web-resources/css/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="web-resources/css/bootstrap/bootstrap-material-design.css">
		<link rel="stylesheet" href="web-resources/css/bootstrap/ripples.min.css">
		<!--SnackBar-->
		<link rel="stylesheet" href="web-resources/css/snackbar/snackbar.css"></script>
		<link rel="stylesheet" href="web-resources/css/snackbar/material.css"></script>
		
		
		
		<link href="web-resources/css/nav.css" rel="stylesheet" type="text/css">
		<!--<link href="web-resources/css/navbar.css" rel="stylesheet" type="text/css"> -->
		<!-- <link href="web-resources/css/custom.css" rel="stylesheet" type="text/css"> -->
		<link href="web-resources/css/gtl.css" rel="stylesheet"	type="text/css">
		<link href="web-resources/css/tooltip.css" rel="stylesheet"	type="text/css">
				
		
		
		 <!-- JS -->
		<script src="web-resources/js/lib/jquery/jquery-2.1.1.min.js"></script>
		<script src="web-resources/js/common.js"></script>
		<script src="web-resources/js/utility.js"></script>

		<c:set var="url">${pageContext.request.requestURL}</c:set>
		<script>		
			baseContextPath ='${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}';			
		</script>
		<%-- <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" /> --%>
		<script>
			gtl.showSpinner();
		</script>
		
</head>
<body>			
		<!--Navigation -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">			
			<!-- menu bar container -->
			<div class="container-fluid">
			
				<!-- navigation header start -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="home" id="navHome" class="navHome">
						<img alt="Gamez2Learn" src="" style="margin-top: -78px;" />  
					</a>
				</div><!--// navigation header start -->
				
				<c:if test="${not isForgotPassword && not isSignUp}">
					<!-- // navigation Menu List Start -->
					 <div class="navbar-collapse collapse navbar-responsive-collapse">
						<!-- left side menubar -->
						  <ul class="nav navbar-nav" style="display: block;">
							<li>
								<a href="home" id="home" class="mnuhome">Home</a>
							</li>						
											
							<!-- MasterData menu start-->
							<li class="dropdown drop-menu" id="mnuMasterList">
								<a href="#"  class="dropdown-toggle mnumaster" data-toggle="dropdown" id="mnuMaster" >
									
								</a>
								<ul class="dropdown-menu menu-hover multi-level" role="menu" aria-labelledby="dropdownMenu">
																									
									<li id="mnuCategoryList" class="dropdown-submenu mnucategory">
										<a tabindex="-1" ></a>
								    <li>
									
							</ul>
						</ul> <!--// left side menubar -->
	
						<ul class="fa-ul" id="online"style="float: left;margin-left: 75px;margin-top: 15px;display:none;"> 
						  <li><i class="fa-li fa fa-spinner fa-spin"></i>Connecting......</li>
						</ul>
	
	
	
						<!-- Right Side navigation  Start -->
						<ul class="nav navbar-nav navbar-right" id="divMenuRight">
							<li class="dropdown drop-menu pull-right" style="display: block;margin-top:-6px;">
								<a title="" style="white-space:nowwrap; overflow:hidden; text-overflow:ellipsis;"
									href="#" class="dropdown-toggle" data-toggle="dropdown" id="navMnuRight">
									<img src="web-resources/images/home/avtar.jpg" height="30px"
											width="30px" class="img-circle" alt="Circular Image">
								</a>
								
								<ul class="dropdown-menu  menu-hover pull-right">
									<li><a href="user/password">Change password</a></li>
									<li><a href="clientCredentials">Profile</a></li>								
									<li><a href="logout">Sign out</a></li>
								</ul>
							</li>
						</ul><!--// Right Side navigation  end -->
						
						<c:set value="<%=SessionVariables.getUserName()%>" var="userName"></c:set>
						<h6	style="margin-top: 19px; display: inline-block; float: right; color: #E6E6E6;">Hi ,
							${userName}
						</h6>
					</div>	
			</c:if>
				<!-- // navigation Menu List End -->				
			</div>
			<!-- /. menu bar container -->
		</div>
		<!--Ending Navigation -->
	
	
	
	<!-- spinner element -->
	<div id="spinner" class="spinner" style="display: none;" aria-hidden="true">
		<img id="img-spinner" src="web-resources/images/spinner.gif" alt="Loading" />
	</div>

<!-- Session Timeout Window start -->
	<div class="modal fade pg-show-modal" data-backdrop="static" id="sessionout" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Session timed-out</h4>
				</div>
				<div class="modal-body">
					<div class="col-xs-3">
						<img alt="TimeOut" src="web-resources/images/sessionTimeout.png" style="margin-top: 3px; max-height: 60px" />
					</div>
					<div class="col-xs-9">
						<p>
							You were logged out of SOP(Survey On Palm) because your session timed out.
							Please login again. <br />
						</p>
					</div>
					<br />
				</div>
				<div class="modal-footer">
					<a id="btnReload" class="btn btn-raised btn-primary pull-left" onclick="location.reload();return false;">Reload</a>					
				</div>
			</div>
		</div>
	</div>
	<!-- Session Timeout Window End -->


	

	<script>
	
		$(document).ready(function(){
			  $.material.init();
			/* on mouse over open the drop down menu */
			
			 $('.drop-menu').on( "mouseover", function() {
				$(".drop-menu").removeClass("open");
				$(this).addClass("open");
			});	 
			 $('.drop-menu').on( "mouseout", function() {
				 $(this).removeClass("open");
			 });
			 
			 /*// on mouse over open the drop down menu ends here*/
			 
			 gtl.hideSpinner();
			 
			 $('#spinner').hide();
			 $("#spinner").bind("ajaxSend", function() {
			   	 $(this).show();
			    }).bind("ajaxStop", function() {
			        $(this).hide();
			    }).bind("ajaxError", function() {
			        $(this).hide();
			});
		});  
	</script>

	<script>
		var isSignUpPage ='${isSignUp}';
		if(isSignUpPage!='true'){
			(function() {		
				var timer = setInterval(function() {				
					var timeoutCookie = getCookie("TimeoutCookie");
					console.log(timeoutCookie);
					if (timeoutCookie == "") {					
						fnDoInvalidateSession('session/invalidate','GET');					
						$('#sessionout').modal('show');
						window.clearInterval(timer);
					}
				}, 1000);
			})();
		}
		
		
		function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ')
					c = c.substring(1);
				if (c.indexOf(name) == 0)
					return c.substring(name.length, c.length);
			}
			return "";
		}
	</script>	