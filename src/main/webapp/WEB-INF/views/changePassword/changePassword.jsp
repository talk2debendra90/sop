	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	
	
	<%@ include file="/WEB-INF/views/header.jsp" %>



<title>Change Password</title>

<!-- Fixed navbar -->
<div class="container">

	<div class="page-header">
		<div class="row">
			<div class="col-xs-6">
				<h1>Change Password</h1>
			</div>
		</div>			
		
	</div>

	<div class="row">
	<div class="row">
	<div class="col-xs-6"></div>
	<div class="col-xs-6">
			<div class="alert alert-success" id="success-msg" role="alert"
				style="display:none; float: left;">
				<strong>Success! &nbsp; </strong> <span id="showMessage">Password
					Changed successfully...</span>
			
			</div>
		</div>
		</div>
		

	
<div class="col-xs-12">
	<div class="jumbotron col-xs-4 col-xs-offset-4" id="changePasswordDiv" >
		<form id="changePasswordForm">
			<div class="form-group">
				<label class="control-label" for="txtCurrentpassword">Current
					Password</label> <input type="password" class="form-control"
					name="txtCurrentpassword" id="txtCurrentpassword" data-fv-notempty
					data-fv-notempty-message=" ">
			</div>
			<div class="form-group">
				<label class="control-label" for="txtNewpassword">New Password</label>
				<input type="password" class="form-control" name="txtNewpassword"
					id="txtNewpassword" data-fv-notempty data-fv-notempty-message=" "
                                        data-fv-stringlength="true"
				               		    data-fv-stringlength-min="8"
				                		data-fv-stringlength-message=" "  >
			</div>
			<div class="form-group">
				<label class="control-label " for="txtConfirmPassword">Confirm
					Password</label> <input type="password" class="form-control"
					name="txtConfirmPassword" id="txtConfirmPassword" data-fv-notempty
					data-fv-identical="true" data-fv-identical-field="txtConfirmPassword"
					data-fv-notempty-message=" "
					data-fv-identical-message="The password and its confirm password are not the same">
			</div>
			<div class="row alertdiv" id="alertdiv">
				<!-- <div class="alert alert-success" id="successPwdMsg" hidden="true">
					<strong style="display: inline-block;">Password changed
						successfully!</strong>
				</div>-->
				<div class="alert alert-danger" id="alertdiv" hidden="true">
					<strong>Current Password not match!</strong> <br>
					<span id="exception"></span>
				</div>
			</div> 

			
				<div class="btn-group">
					<button type="submit" class="btn btn-primary"
						id="changePasswordSaveButton" name="changePasswordSaveButton" style="display:block">Change</button>
					<button type="button" class="btn btn-link"
						id="changePasswordCancelButton" onclick="window.location.href = ('home');"
						name="changePasswordCancelButton" style="display:block">Cancel</button>
				
			    </div>
		</form>
	</div>
	</div>	
	</div>
	</div>
	



<%@ include file="/WEB-INF/views/footer.jsp"%>

<script src="web-resources/js/changePassword/changePassword.js"></script>
