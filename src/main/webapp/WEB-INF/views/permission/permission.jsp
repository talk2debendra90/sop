<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
	
	<%@ include file="/WEB-INF/views/header.jsp" %>
<!-- /. fixed nav bar -->
<div class="container">
	<div class="page-header">
		<div>
			<ol class="breadcrumb">
				<li class="active">Masters</li>
				<li class="active">User Management</li>
				<li class="active">Permissions</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<h1>Permissions</h1>
			</div>
			<div class="col-xs-4">
				<div class="alert alert-success" id="success-msg" role="alert"
					style="display: none;">
					<strong>Success! &nbsp; </strong> <span id="showMessage"></span>
				</div>
			</div>
			<div class="col-xs-2">
			
			</div>
		</div>
		
	<div class="toggle-col small " id="divToogle">
			Show/Hide columns :
		<a id="t0" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowAll([1,2],'tblPermission');">Show All</a> 
		<a id="t1" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(1,'tblPermission');">Permission Name</a>
		<a id="t2" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(2,'tblPermission');">Description</a> 
		
	</div>
	<div id="tableDiv" ><!-- hidden="true" -->
		<table class="table table-striped table-hover mdl-data-table tblPermission" id="tblPermission">
			<thead>
				<tr id="head">
					<th id="h1" class="nosort" style="width: 40px">#</th>
					<th id="h2" class="nosort">Permission Name</th>
					<th id="h3" class="nosort">Description</th>
				</tr>
			</thead>
				<tbody>
				<c:forEach items="${permissionList}" var="permission"
					varStatus="loop">
					<tr id="${permission.permissionId}">
						<td style="width: 40px;"></td>
						<td><c:out value="${permission.permissionName}" /></td>						
						<td><c:out value="${permission.permissionDescription}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</div>
<script src="web-resources/js/permission/permission.js"></script>
<jsp:include page="/WEB-INF/views/footer.jsp"></jsp:include>
