	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
	
	<%@ include file="/WEB-INF/views/header.jsp" %>


<script>
 	var noOfRoles ='${noOfRoles}'
</script>




<div class="container">
	<div class="page-header">
		<div>
			<ol class="breadcrumb">
				<li class="active">Masters</li>
				<li class="active">User Management</li>
				<li class="active">Role Permissions</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-xs-5">
				<h1>Role Permissions</h1>
			</div>
			<div class="col-xs-4">
				<div id="success-msg"				
				     data-content="Information updated" data-style="toast" data-timeout="1500" data-html-allowed="true">
				</div>
			</div>
		</div>
			
	<div id="tableDiv" >
			<table
				class="table table-striped table-hover mdl-data-table tblrolepermission" id="tblrolepermission">
				<thead>
					<tr>
						<th class="nosort">#</th>
						<th class="nosort">Permission</th>
						<c:forEach items="${roles}" var="role">
							<th class="nosort" id="${role.id}">${role.name}</th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${rolePermission}" var="permission" varStatus="loop">
						<tr>
							<td></td>
							<c:set var="permissionId" value="${fn:split(permission.key,'=')}" />
							<td>${permissionId[1]}</td>
							<c:forEach items="${roles}" var="role" varStatus="loop">
								<c:choose>
									<c:when test="${  fn:contains(permission.value, role.id)}">
										<td>
											<input class="chkbox_rolepermission text-center" id="${role.id}_${permissionId[0]}"
												type="checkbox" data-roleid="${role.id}"
												data-permissionid="${permissionId[0]}" checked="checked"
												onclick="rolePermission.fnSave(this);" />
										</td>
									</c:when>
									<c:otherwise>
										<td>
											<input class="chkbox_rolepermission text-center" id="${role.id}_${permissionId[0]}"
												type="checkbox" data-roleid="${role.id}"
												data-permissionid="${permissionId[0]}" onclick="rolePermission.fnSave(this);"/>
										</td>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
</div>
</div>
<script src="web-resources/js/permission/rolePermission.js"></script>
<jsp:include page="/WEB-INF/views/footer.jsp"></jsp:include>
