	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	
	<%@ include file="/WEB-INF/views/header.jsp" %>
<script type="text/javascript">
	var sequenceId = "${sequenceId}";
	var userOrgId ='${orgId}';
</script>
<!-- container -->
<div class="container">

	<!-- page header -->
	<div class="page-header">
		<div>
			<ul class="breadcrumb" style="margin-bottom: 5px;">
				<li class="active">Masters</li>
				<li class="active">User Management</li>
				<li class="active">User</li>
			</ul>
		</div>

		<div class="row">
			<div class="col-xs-5">
				<h1>User</h1>
			</div>
			
			<div class="col-xs-5 ">
				<div id="success-msg"				
				     data-content="Information updated" data-style="toast" data-timeout="1500" data-html-allowed="true">
				</div>
			</div> 

			
			<div class="col-xs-2">
				<a id="btnAddUser" data-toggle="modal" data-target="#mdlAddUser"  class="btn btn-raised btn-primary pull-right">
					Create User
				</a>
			</div>
			
		</div>
	</div><!--// page header -->
	<br/>
	
	<div class="toggle-col small " id="divToogle">
			Show/Hide columns :
		<a id="t0" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowAll([1,2,3,4,5],'tblUser');">Show All</a> 
		<a id="t1" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(1,'tblUser');">Name</a> 
		<a id="t2" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(2,'tblUser');">Email</a>
		<a id="t3" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(3,'tblUser');">Mobile</a>
		<a id="t4" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(4,'tblUser');">Category</a>
		<a id="t5" class="label label-success" href="javascript:void(0);" onclick="gtl.fnShowHide(5,'tblUser');">Role</a>
	</div>
	
	<div id="tableDiv" hidden="true">
		<table class="table table-striped table-hover mdl-data-table tblorganisation" 
				id="tblUser">
			<thead>
				<tr id="head">
					<th id="h1" class="nosort" style="width: 40px">#</th>
					<th id="h2" class="nosort">Name</th>
					<th id="h3" class="nosort">Email</th>
					<th id="h4" class="nosort">Mobile</th>
					<th id="h5" class="nosort">Category</th>
					<th id="h6" class="nosort">Role</th>
					<th id="h7" class="nosort" style="width: 60px">Action</th>
				</tr>
					
			</thead>
				<tbody>
					<c:forEach items="${userList}" var="user" varStatus="loop">
						<tr id="${user.userId}" data-id="${user.userId}">
							<td></td>
							<td title="${user.userName}">${user.userName}</td>
							<td title="${user.email}">${user.email}</td>
							<td title="${user.mobileNumber}">${user.mobileNumber}</td>
							<td title="${user.categoryName}">${user.categoryName}</td>
							<td title="${user.roleName}">${user.roleName}</td>
							<td>
								<div class="div-tooltip">
									<a class="tooltip tooltip-top"
										onclick="user.initEdit('${user.userId}','${user.firstName}','${user.middleName}','${user.lastName}','${user.country}','${user.mobileNumber}','${user.departmentId}','${user.roleId}','${user.isAdmin}');">
										<span class="tooltiptext">Edit</span>
										<span class="glyphicon glyphicon-edit font-size12"></span>
									</a>
									<a style="margin-left: 10px;" class="tooltip tooltip-top"											
											onclick="user.initDelete('${user.userId}','${user.userName}');"><span
											class="tooltiptext">Delete</span> 
											<span class="glyphicon glyphicon-trash font-size12"></span> 
									</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div><!-- // container -->

	<%@ include file="addUser.jsp" %>
	<%@ include file="editUser.jsp" %>
	<%@ include file="deleteUser.jsp" %>
	<%@ include file="errorAlert.jsp" %>
	

	<%@ include file="/WEB-INF/views/footer.jsp" %>

	
	
	<script src="web-resources/js/user/user.js"></script>