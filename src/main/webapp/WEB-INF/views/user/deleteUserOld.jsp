<div class="modal fade pg-show-modal" id="mdlDeleteUser" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                        <h4 class="modal-title">Delete user</h4>
                    </div>
                    <div class="modal-body">
                        <p class="msgDelete" id="msgDelete" /></p>

                        <div class="col-xs-12 row alertdiv" id="alertdiv">
                            <div class="alert alert-danger" id="errorMessage" hidden="true">
                                <strong>Failure!</strong> Record could not be deleted.
                                <br> <span id="exception"></span>
                            </div>
                        </div>

                        <div class="btn-group" id="buttonGroup">
                            <button type="button" class="btn btn-danger" style="display: block;" id="btnDeleteUser">Delete</button>
                            <button type="button" data-dismiss="modal" class="btn btn-link" style="display: block;" id="btnDeleteCancel" >Cancel</button>
                        </div>
                                               
                    </div>
                </div>
            </div>
</div>