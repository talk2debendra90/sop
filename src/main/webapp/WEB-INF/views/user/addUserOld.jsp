<div  class="modal fade pg-show-modal" id="mdlAddUser" tabindex="-1" data-backdrop="static" 
		data-keyboard="false" role="dialog" aria-hidden="true">
		
	<div class="modal-dialog  modal-lg">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Create user</h4>
			</div>
			
			<div class="modal-body">
				<form role="form" id="frmAddUser" class="frmAddUser form" style="min-height:400px;">

					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtUserFirstName">First Name *</label>
						<input  type="text" class="form-control" id="txtUserFirstName" name="txtUserFirstName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtUserMiddleName">Middle Name</label>						
						<input  type="text" class="form-control" id="txtUserMiddleName" name="txtUserMiddleName"/>
					</div>
					
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtUserLastName">Last Name *</label>
						<input  type="text" class="form-control" id="txtUserLastName" name="txtUserLastName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtAddUserEmail">Email *</label>
						<input  type="text"  class="form-control"  id="txtAddUserEmail" name="txtAddUserEmail"
								data-fv-notempty data-fv-notempty-message=" "/>						
					</div>
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label" for="txtAddUserPassword">Password *</label>
						<input  type="password"  class="form-control"  id="txtAddUserPassword" name="txtAddUserPassword"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					

					
					<div class="form-group col-xs-4 label-floating">
						<label class="control-label">Phone number</label>
						<input type="text" class="form-control" nam	e="phoneNumber"  id="phoneNumber"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					<div class="form-group col-xs-4 department" id="divDepartment">
						<label class="control-label" for="department">Department</label>
						<span class="asterisk">*</span><br /> 
						<select id="department" name="department" class="form-control select" data-fv-notempty
							data-fv-notempty-message=" " data-category="Department"
							size="2">
						</select>
					</div>
					<div class="form-group col-xs-4">
						<label class="control-label">Country</label>
						<select class="form-control select" name="countrySelectBox" id="countrySelectBox">
								<option value="US">United States</option>
								<option value="BG">Bulgaria</option>
								<option value="BR">Brazil</option>
								<option value="CN">China</option>
								<option value="CZ">Czech Republic</option>
								<option value="DK">Denmark</option>
								<option value="FR">France</option>
								<option value="DE">Germany</option>
								<option value="IN" selected>India</option>
								<option value="MA">Morocco</option>
								<option value="NL">Netherlands</option>
								<option value="PK">Pakistan</option>
								<option value="RO">Romania</option>
								<option value="RU">Russia</option>
								<option value="SK">Slovakia</option>
								<option value="ES">Spain</option>
								<option value="TH">Thailand</option>
								<option value="AE">United Arab Emirates</option>
								<option value="GB">United Kingdom</option>
								<option value="VE">Venezuela</option>
						</select>
						
					</div>
				<!-- 	<div class="col-xs-4"></div>
					<div class="form-group col-xs-2 isAdmin" id="divIsAdmin">						
						<input type="checkbox"   id="chkboxIsAdmin" name="chkboxIsAdmin" />
						<label class="control-label" for="chkboxIsAdmin">IsAdmin</label>
					</div> -->

					<div class="form-group col-xs-4 role" id="divRole">
						<label class="control-label" for="role">Role</label>
						<span class="asterisk">*</span><br /> 
						<select id="role" name="role" class="form-control select" data-fv-notempty
							data-fv-notempty-message=" " data-category="role"
							size="2">
						</select>
					</div>




					<div id="alertdiv" class="alertdiv col-xs-12">
						<div class="alert alert-success" id="successMessage" hidden="true">
							<strong style="display: inline-block;">Success!</strong> 
							<span id="success"></span>
						</div>
						<div class="alert alert-danger" id="errorMessage" hidden="true">
							<strong>Failure!</strong> Record could not be saved. <br> 
							<span id="exception"></span>
						</div>
					</div>
					
					<div class="btn-group col-xs-12"  id="buttonGroup">
						<button type="submit" class="btn btn-primary btnSaveUser submit"
							style="display: block;" id="btnSaveUser" >Save</button>
						<button type="button" data-dismiss="modal" class="btn btn-link"
							style="display: block;" id="btnAddUserCancel">Cancel</button>
					</div>
					
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
				</form>
			</div>
		</div>
	</div>
</div>