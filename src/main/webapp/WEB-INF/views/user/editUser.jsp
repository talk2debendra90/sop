<div  class="modal fade pg-show-modal" id="mdlEditUser" tabindex="-1" data-backdrop="static" 
		data-keyboard="false" role="dialog" aria-hidden="true">
		
	<div class="modal-dialog  modal-lg">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit user</h4>
			</div>
			
			<div class="modal-body">
				<form role="form" id="frmEditUser" class="frmEditUser form" style="min-height:400px;">

					<div class="form-group col-xs-4">
						<label class="control-label" for="txtEditUserFirstName">First Name</label>
						<span class="asterisk">*</span> 
						<input  type="text" class="form-control" id="txtEditUserFirstName" name="txtEditUserFirstName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					
					<div class="form-group col-xs-4">
						<label class="control-label" for="txtEditUserMiddleName">Middle Name</label>						
						<input  type="text" class="form-control" id="txtEditUserMiddleName" name="txtEditUserMiddleName"/>
					</div>
					
					
					<div class="form-group col-xs-4">
						<label class="control-label" for="txtEditUserLastName">Last Name</label>
						<span class="asterisk">*</span> 
						<input  type="text" class="form-control" id="txtEditUserLastName" name="txtEditUserLastName"
						 		data-fv-notempty data-fv-notempty-message=" "/>
					</div>
				<!-- 	
					<div class="form-group col-xs-4">
						<label class="control-label" for="txtEditUserEmail">Email</label>
						<span class="asterisk">*</span> 
						<input  type="text"  class="form-control"  id="txtEditUserEmail" name="txtEditUserEmail"
								data-fv-notempty data-fv-notempty-message=" "/>						
					</div>
					<div class="form-group col-xs-4">
						<label class="control-label" for="txtEditUserPassword">Password</label>
						<span class="asterisk">*</span>
						<input  type="password"  class="form-control"  id="txtEditUserPassword" name="txtEditUserPassword"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div> -->
					

					<div class="form-group col-xs-4">
						<label class="control-label">Country</label>
						<select class="form-control select" name="countrySelectBox" id="editCountrySelectBox">								
								<option value="BG">Bulgaria</option>
								<option value="BR">Brazil</option>
								<option value="CN">China</option>
								<option value="CZ">Czech Republic</option>
								<option value="DK">Denmark</option>
								<option value="FR">France</option>
								<option value="DE">Germany</option>
								<option value="IN">India</option>
								<option value="MA">Morocco</option>
								<option value="NL">Netherlands</option>
								<option value="PK">Pakistan</option>
								<option value="RO">Romania</option>
								<option value="RU">Russia</option>
								<option value="SK">Slovakia</option>
								<option value="ES">Spain</option>
								<option value="TH">Thailand</option>
								<option value="AE">United Arab Emirates</option>
								<option value="GB">United Kingdom</option>
								<option value="US">United States</option>
								<option value="VE">Venezuela</option>
						</select>
						
					</div>
					<div class="form-group col-xs-4">
						<label class="control-label">Phone number</label>
						<input type="text" class="form-control" name="editPhoneNumber"  id="editPhoneNumber"
						data-fv-notempty data-fv-notempty-message=" "/>
					</div>
					
					<div class="form-group col-xs-4 department" id="divDepartment">
						<label class="control-label" for="editDepartment">Department</label>
						<span class="asterisk">*</span><br /> 
						<select id="department" name="editDepartment" class="form-control select" data-fv-notempty
							data-fv-notempty-message=" " data-category="Department"
							size="2">
						</select>
					</div>
					<!-- <div class="form-group col-xs-2 isAdmin" id="divIsAdmin">						
						<input type="checkbox"   id="chkboxIsAdmin" name="chkboxIsAdmin" />
						<label class="control-label" for="chkboxIsAdmin">IsAdmin</label>
					</div> -->
					<div class="form-group col-xs-4 role" id="divRole">
						<label class="control-label" for="editRole">Role</label>
						<span class="asterisk">*</span><br /> 
						<select id="role" name="editDepartment" class="form-control select" data-fv-notempty
							data-fv-notempty-message=" " data-category="Role"
							size="2">
						</select>
					</div>






					<div id="alertdiv" class="alertdiv col-xs-12">
						<div class="alert alert-success" id="successMessage" hidden="true">
							<strong style="display: inline-block;">Success!</strong> 
							<span id="success"></span>
						</div>
						<div class="alert alert-danger" id="errorMessage" hidden="true">
							<strong>Failure!</strong> Record could not be saved. <br> 
							<span id="exception"></span>
						</div>
					</div>
					
					<div class="btn-group col-xs-12"  id="buttonGroup">
						<button type="submit" class="btn btn-primary btnEditSaveUser submit"
							style="display: block;" id="btnEditSaveUser" >Save</button>
						<button type="button" data-dismiss="modal" class="btn btn-link"
							style="display: block;" id="btnEditUserCancel">Cancel</button>
					</div>
					
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
					<div class="col-xs-12"></div>
				</form>
			</div>
		</div>
	</div>
</div>