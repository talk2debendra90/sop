package com.celesco.sop.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
   
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		
		
		http.csrf().disable();
		
		http.headers().frameOptions().sameOrigin();
		
		http
		.authorizeRequests()
		.antMatchers("/web-resources/**","/","/**","/home","/login").permitAll()
		
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/")
		.defaultSuccessUrl("/home", true)
		.failureForwardUrl("/fail2login")	
		.permitAll()
		.and()
		.logout().logoutSuccessUrl("/")
		.permitAll();
	}
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {        
    	auth.authenticationProvider(customAuthenticationProvider);
    	
    }
    @Override
	public void configure(WebSecurity web) throws Exception {
		 web.ignoring().antMatchers("/css/**","/*.js");
	}

}
