package com.celesco.sop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.celesco.sop.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	
	
	/**
  	 *Returns user by login name
  	 * @param login
  	 * @return an user by login 
  	 */
	@Query("FROM User AS user"			
			+ " WHERE user.login=:login"
			+ " AND user.isActive=true")
	public User findUserByLogin(@Param("login") String login);
	
	
	
	}
