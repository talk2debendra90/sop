package com.celesco.sop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.celesco.sop.model.UserRoleMapping;

@Repository
public interface UserRoleMappingRepository extends CrudRepository<UserRoleMapping, Integer> {


	
	/**
  	 *Returns a list UserRoleMapping by userId
  	 * 
  	 * @param userId
  	 * @return a list of UserRoleMapping
  	 */
	
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE user.id=:userId"
			+ " AND user.isActive=true AND role.isActive =true AND userRoleMapping.isActive =true")
	public List<UserRoleMapping> getUserRoleMappingbyUserId(@Param("userId")Integer userId);




	/**
  	 *Returns a list UserRoleMapping by roleId
  	 * 
  	 * @param roleId
  	 * @return a list of UserRoleMapping
  	 */
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE role.id=:roleId"
			+ " AND user.isActive=true "
			+ " AND role.isActive =true "
			+ " AND userRoleMapping.isActive =true")
	public List<UserRoleMapping> getUserRoleMappingsByRoleId(@Param("roleId")Integer roleId);

	
	

	/**
  	 *Returns a list of FS&RC UserRoleMapping by userId
  	 * 
  	 * @param userIds
  	 * @return a list of list of UserRoleMapping 
  	 */
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE user.id IN (:userIds)"
			+ " AND user.isActive=true AND role.isActive =true AND userRoleMapping.isActive =true"
			+ " AND (role.code ='FS'  OR  role.code = 'RC')")
	public List<UserRoleMapping> getUserRoleMappingOfFSAndRCByUserIds(@Param("userIds")List<Integer> userIds);

	
	
	/**
  	 *Returns a list of UserRoleMapping by roleCode
  	 * 
  	 * @param roleCode
  	 * @return a list of list of UserRoleMapping 
  	 */
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE role.code=:roleCode"
			+ " AND user.isActive=true AND role.isActive =true AND userRoleMapping.isActive =true")
	public List<UserRoleMapping> getUserRoleMappingByRoleCode(@Param("roleCode")String roleCode);

	/**
  	 *Returns a list of UserRoleMapping by roleCode
  	 * 
  	 * @param roleCode
  	 * @return a list of list of UserRoleMapping 
  	 */
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE role.code IN (:roleCode)"
			+ " AND user.isActive=true AND role.isActive =true AND userRoleMapping.isActive =true")
	public List<UserRoleMapping> getUserRoleMappingByRoleCode(@Param("roleCode")List<String> roleCode);


	/**
  	 *Returns a list of UserRoleMapping by roleCode & userId
  	 * 
  	 * @param roleCode
  	 * @param userId
  	 * @return a list of list of UserRoleMapping 
  	 */
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ "	LEFT JOIN FETCH userRoleMapping.user as user"
			+ "	LEFT JOIN FETCH userRoleMapping.role as role"
			+ "	WHERE userRoleMapping.isActive =true"
			+ "	AND"
			+ "	role.isActive =true"
			+ "	AND"
			+ "	user.isActive =true"
			+ "	AND"
			+ "	role.code=:roleCode"
			+ "	AND user.id=:userId")
	public List<UserRoleMapping> getUserRoleMappingByRoleCodeAndUserId(@Param("roleCode")String roleCode,
																		@Param("userId")Integer userId);



	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE userRoleMapping.id=:mappingId"
			+ " AND user.isActive=true "
			+ " AND role.isActive =true "
			+ " AND userRoleMapping.isActive =true")
	public UserRoleMapping getUserRoleMappingById(@Param("mappingId") Integer mappingId);



	/*@Query("FROM UserRoleMapping AS userRoleMapping"
			+ "	LEFT JOIN FETCH userRoleMapping.user as user"
			+ "	LEFT JOIN FETCH userRoleMapping.role as role"
			+ "	WHERE userRoleMapping.isActive =true"
			+ "	AND role.isActive =true"
			+ "	AND user.isActive =true"
			+ " AND role.code NOT IN(:roleCodes)"
			+ "	AND user.id=:userId")
	public List<UserRoleMapping> getMappingByUserIdAndRoleCodesNotInThis(
			@Param("userId")Integer userId,
			@Param("roleCodes")List<String> roleCodes);*/




	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ " LEFT JOIN FETCH userRoleMapping.user as user"
			+ " LEFT JOIN FETCH userRoleMapping.role as role"
			+ " WHERE role.id IN(:roleIds)"
			+ " AND userRoleMapping.isActive =true")
	public List<UserRoleMapping> getUserRoleMappingsByRoleIds(@Param("roleIds")List<Integer> roleIds);
	
	
	@Query("FROM UserRoleMapping AS userRoleMapping"
			+ "	LEFT JOIN FETCH userRoleMapping.user as user"
			+ "	LEFT JOIN FETCH userRoleMapping.role as role"
			+ "	WHERE userRoleMapping.isActive =true"
			+ "	AND role.isActive =true"
			+ "	AND user.isActive =true"
			+ " AND role.programCode !=:programCode"
			+ "	AND user.id=:userId")
	public List<UserRoleMapping> getAllMappingByUserIdAndRoleProgramCodeNotInThis(
			@Param("userId")Integer userId,
			@Param("programCode") Integer programCode);	
	
}
