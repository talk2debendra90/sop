package com.celesco.sop.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.celesco.sop.util.QueryParams;

@Transactional
@Service
public class AbstractPaginationRepositoryService<T> {

	
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<T> getFilteredPaginationData(String dataQuery,boolean isCriteria,
											 String criteriaValue,int offset,int limit,QueryParams...queryParams){
	
		
		
		Query query = manager.createQuery(dataQuery);
		for (QueryParams param : queryParams) {
			query.setParameter(param.getParameterName(),param.getValue());
		}
		if(isCriteria)
			query.setParameter("criteria",criteriaValue);
		/*if(offset==1){
			query.setFirstResult(offset-1);	
		}else{
			query.setFirstResult((--offset)+limit);
		}*/
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		
		List<T> data =query.getResultList();
		return data;
		
	}
	
	public long getTotalCount(String countQuery,boolean isCriteria, String criteriaValue,QueryParams...queryParams){
		Query queryTotal = manager.createQuery(countQuery);
		if(isCriteria)
			queryTotal.setParameter("criteria",criteriaValue);
		for (QueryParams param : queryParams) {
			queryTotal.setParameter(param.getParameterName(),param.getValue());
		}
		return (long)queryTotal.getResultList().size();
	}
	
	
}
