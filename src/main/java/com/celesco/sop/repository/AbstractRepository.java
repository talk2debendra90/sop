package com.celesco.sop.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AbstractRepository/*<T, ID extends Serializable> */ {
	
	@PersistenceContext
	private EntityManager manager;
	
	
	/*@Transactional
	public <S extends T> S saveOrUpdate(S entity) {
		manager.persist(entity);
		manager.flush();
		manager.clear();
		return entity;
	}*/
	
	@Transactional
	public void flushAndClear(){
		manager.flush();
		manager.clear();
	}
	
	
	
	
	
}
