package com.celesco.sop.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.celesco.sop.dto.RoleVm;
import com.celesco.sop.model.Role;
import com.celesco.sop.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Override
	public List<RoleVm> getAllRole() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleVm addRole(RoleVm roleVm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleVm editRole(RoleVm roleVm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleVm deleteRole(Integer roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllRoleByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllSelectedRoleListByUserId(Integer userId, Integer loggedUserRoleSequenceNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRoleSequenceNumberByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllRolesGreaterThanLoggedUserSequenceNumber(Integer logedUserSequenceNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleVm getLoggedUserSuperRole(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLoggedUserSuperRoleName(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLoggedUserSuperRoleCode(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Role getRoleByCode(String code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllRole(String appType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllRolesGreaterThanLoggedUserSequenceNumber(Integer logedUserSequenceNumber,
			String appType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllSelectedRolesByUserAndRoleSequence(Integer userId, Integer loggedUserRoleSequenceNumber,
			String appType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Role getRoleById(Integer roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Role> getAllRoleByRoles(List<Integer> roleIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleVm> getAllRoleByAppTypeAndUserId(String appType, Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
