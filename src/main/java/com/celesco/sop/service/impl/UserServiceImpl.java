package com.celesco.sop.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.celesco.sop.dto.UserDto;
import com.celesco.sop.model.User;
import com.celesco.sop.repository.UserRepository;
import com.celesco.sop.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
		
	@Autowired
	private UserRepository userRepository;
	

	@Override
	public User login(UserDto userDto) {
		
		String pwd = DigestUtils.md5Hex(userDto.getPassword());
		User user =userRepository.findUserByLogin(userDto.getLogin().trim());

		if (user != null && userDto.getLogin().trim().equals(user.getLogin().trim()) && pwd.equals(user.getPassword())) {
			return user;
		}
		return null;

	}

}



