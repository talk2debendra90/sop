package com.celesco.sop.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.celesco.sop.service.PermissionService;

@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {}
