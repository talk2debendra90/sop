package com.celesco.sop.service;

import java.util.List;

import com.celesco.sop.dto.RolePermissionVm;
import com.celesco.sop.model.RolePermission;

public interface RolePermissionService {
	/**
	 * 
	 * @param permissionVm
	 * @return
	 */
	public int addPermissionToRole(RolePermissionVm permissionVm);
	/**
	 * 
	 * @param permissionVm
	 * @return
	 */
	public Boolean deletePermissionToRole(RolePermissionVm permissionVm);
	/**
	 * 
	 * @return
	 */
	public List<RolePermissionVm> getAllRolePermission();
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public Integer getLoggedUserSequenceNumber(Integer userId);
	
	public List<RolePermission> getAllRolePermissionByRole(Integer roleId);
	
	public void deleteRolePermissionByRole(Integer roleId);
	

}
