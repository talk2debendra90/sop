package com.celesco.sop.service;

import java.util.List;

import com.celesco.sop.dto.RoleVm;
import com.celesco.sop.model.Role;

public interface RoleService {
	/**
	 * 
	 * @return
	 */
	public List<RoleVm> getAllRole();
	/**
	 * 
	 * @param roleVm
	 * @return
	 */
	public RoleVm addRole(RoleVm roleVm);
	/**
	 * 
	 * @param roleVm
	 * @return
	 */
	public RoleVm editRole(RoleVm roleVm);
	/**
	 * 
	 * @param roleId
	 * @return
	 */
	public RoleVm deleteRole(Integer roleId);
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public List<RoleVm> getAllRoleByUserId(Integer userId);
	/**
	 * 
	 * @param userId
	 * @param loggedUserRoleSequenceNumber
	 * @return
	 */
	public List<RoleVm> getAllSelectedRoleListByUserId(Integer userId,
			Integer loggedUserRoleSequenceNumber);
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public Integer getRoleSequenceNumberByUserId(Integer userId);
	/**
	 * 
	 * @param logedUserSequenceNumber
	 * @return
	 */
	public List<RoleVm> getAllRolesGreaterThanLoggedUserSequenceNumber(
			Integer logedUserSequenceNumber);
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public RoleVm getLoggedUserSuperRole(Integer userId);
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String getLoggedUserSuperRoleName(Integer userId);
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public String getLoggedUserSuperRoleCode(Integer userId);
	
	/**
	 * Return active {@Link Role}
	 * @param code
	 * @return  active {@Link Role}
	 */
	public  Role getRoleByCode(String code);
	
	/**
	 * 
	 * @param appType
	 * @return
	 */
	public List<RoleVm> getAllRole(String appType);
	
	public List<RoleVm> getAllRolesGreaterThanLoggedUserSequenceNumber( Integer logedUserSequenceNumber,String appType);
	
	public List<RoleVm> getAllSelectedRolesByUserAndRoleSequence(Integer userId, Integer loggedUserRoleSequenceNumber,String appType);

	public Role getRoleById(Integer roleId);
	
	public List<Role> getAllRoleByRoles(List<Integer> roleIds);
	
	//public List<String> getAllExcludedRoleCodeByAppType(String appType);
	
	public List<RoleVm> getAllRoleByAppTypeAndUserId(String appType, Integer userId);
	
	
}
