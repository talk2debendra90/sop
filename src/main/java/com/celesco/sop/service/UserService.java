package com.celesco.sop.service;

import com.celesco.sop.dto.UserDto;
import com.celesco.sop.model.User;

public interface UserService {
	/**
	 * 
	 * @return
	 */
	public User login(UserDto userVm);
	
}
