package com.celesco.sop.exception;

public class ChildExistsException extends RuntimeException {
	 
	private static final long serialVersionUID = 3250408398796674781L;
	String message;
	
	

	public ChildExistsException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
