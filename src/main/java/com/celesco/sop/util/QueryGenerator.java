package com.celesco.sop.util;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class QueryGenerator {
	
	protected static final Logger LOGGER = Logger.getLogger(QueryGenerator.class);
	public static String createTable(String tableName){
		String createTableQuery="CREATE TABLE :TABLE_NAME (id int(11) NOT NULL auto_increment, PRIMARY KEY (id))";
		return createTableQuery.replace(":TABLE_NAME", tableName);
	}
	
	
	public static String addColumn(String tableName,String columnName,String columnType,String columnWidth){
		String addColumnQuery="ALTER TABLE :TABLE_NAME ADD COLUMN :COLUMN_NAME :COLUMN_TYPE(:COLUMN_WIDTH)";
		try{
		addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName).replace(":COLUMN_NAME", columnName).replace(":COLUMN_TYPE", columnType).replace(":COLUMN_WIDTH", columnWidth);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	public static String addColumn(String tableName,String columnName,String columnType){
		String addColumnQuery="ALTER TABLE :TABLE_NAME ADD COLUMN :COLUMN_NAME :COLUMN_TYPE";
		try{
		addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName).replace(":COLUMN_NAME", columnName).replace(":COLUMN_TYPE", columnType);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	public static String addConstraint(String alterTableName,String columnName,String refferenceTable ,String refferenceColumn){
		String addColumnQuery=	"ALTER TABLE :ALTER_TABLE_NAME ADD CONSTRAINT :ALTER_TABLE_NAME_fk_:COLUMN_NAME FOREIGN KEY (:COLUMN_NAME) REFERENCES :REFERENCE_TABLE(:REFERENCE_ID)";

		try{
			addColumnQuery=addColumnQuery.replace(":ALTER_TABLE_NAME", alterTableName).replace(":COLUMN_NAME", columnName)
					.replace(":REFERENCE_TABLE", refferenceTable).replace(":REFERENCE_ID", refferenceColumn);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	public static String renameColumn(String tableName,String oldColumnName,String newColumnName,String columnType,String columnWidth){
		String renameColumnQuery="ALTER TABLE :TABLE_NAME CHANGE COLUMN :COLUMN_NAME1  :COLUMN_NAME2 :COLUMN_TYPE(:COLUMN_WIDTH)";
		try{
		renameColumnQuery=renameColumnQuery.replace(":TABLE_NAME", tableName)
				.replace(":COLUMN_NAME1", oldColumnName)
				.replace(":COLUMN_NAME2", newColumnName)
				.replace(":COLUMN_TYPE", columnType)
				.replace(":COLUMN_WIDTH", columnWidth);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		
		return renameColumnQuery;
	}
	
	public static String renameColumn(String tableName,String oldColumnName,String newColumnName,String columnType){
		String addColumnQuery="ALTER TABLE :TABLE_NAME CHANGE COLUMN :COLUMN_NAME1 :COLUMN_NAME2 :COLUMN_TYPE";
		try{
		addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName)
				.replace(":COLUMN_NAME1", oldColumnName)
				.replace(":COLUMN_NAME2", newColumnName)
				.replace(":COLUMN_TYPE", columnType);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	
	public static String dropColumn(String tableName,String columnName){
		String addColumnQuery="ALTER TABLE :TABLE_NAME DROP COLUMN :COLUMN_NAME";
		try{
		addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName).replace(":COLUMN_NAME", columnName);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	

	public static String dropTable(String tableName){
		String addColumnQuery="DROP TABLE :TABLE_NAME";
		try{
			addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	
	public static String inserInToTable(String tableName,String columnName,String dataValue){
		String addColumnQuery="INSERT INTO :TABLE_NAME (:COLUMN_NAME) VALUES(':DATAVALUE')";
		try{
		addColumnQuery=addColumnQuery.replace(":TABLE_NAME", tableName).replace(":COLUMN_NAME", columnName).replace(":DATAVALUE", dataValue);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addColumnQuery;
	}
	
	public static String inserInToTable(String tableName,Map<String,String> dataValue){
		String columns="";
		String values="";
		
		for (Map.Entry<String, String> entry : dataValue.entrySet()) {
			 columns=columns+entry.getKey()+",";
			 String setValue=entry.getValue().replace("'", "''");
			 //setValue=setValue.replace("\'", "\\'");
			values=values+"'"+setValue+"',";
			
		}
		String query="insert into "+tableName +"(" +columns+":COL)"+"values("+values+":COL)";
		query=query.replace(",:COL)", ")");
		return query;
	}
	
	/*public static String inserExcelDataInToTable(String tableName,Map<String,ExcelData1> dataValue){
		String columns="";
		String values="";
		
		for (Entry<String, ExcelData1> entry : dataValue.entrySet()) {
			
			 ExcelData excelData=entry.getValue();
			 
			 columns=columns+entry.getKey()+",";
			 String setValue=entry.getValue();
			 setValue=setValue.replace("\'", "\\'");
			values=values+"'"+setValue+"',";
			
		}
		String query="insert into "+tableName +"(" +columns+":COL)"+"values("+values+":COL)";
		query=query.replace(",:COL)", ")");
		return query;
	}*/
	public static String renameTable(String fromTableName ,String toTableName){
		
		String addTableQuery="RENAME TABLE :TABLE_NAME1 TO :TABLE_NAME2";
		try{
			addTableQuery=addTableQuery.replace(":TABLE_NAME1", fromTableName).replace(":TABLE_NAME2", toTableName);
		}catch(Exception exception){
			LOGGER.info(exception);
		}
		return addTableQuery;}
	
	public static String  selectAllTableData(String tableName,String columnName,List<Integer> param,String uploadedByColumn,List<String> uploadedBy)
	{ 
		String selectTableQuery="SELECT * FROM :TABLENAME WHERE :COLUMNNAME1 IN "+param+" AND :COLUMNNAME2 IN "+new Gson().toJson(uploadedBy)+" AND isActive=true";
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":COLUMNNAME1", columnName).replace(":COLUMNNAME2", uploadedByColumn).replace("[", "(").replace("]", ")");
		return selectTableQuery;
		}
	public static String  selectManageTableData(String tableName,String columnName,List<Integer> param,String uploadedByColumn,List<Integer> uploadedBy)
	{ 
		String selectTableQuery="SELECT * FROM :TABLENAME WHERE :COLUMNNAME1 IN "+param+" AND :COLUMNNAME2 IN "+new Gson().toJson(uploadedBy)+" AND isActive=true";
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":COLUMNNAME1", columnName).replace(":COLUMNNAME2", uploadedByColumn).replace("[", "(").replace("]", ")");
		return selectTableQuery;
		}
	public static String  selectAllTableDataByLocationIdAndUploadedBy(String tableName,String columnName,List<Integer> param)
	{
		String selectTableQuery="SELECT * FROM :TABLENAME WHERE isActive=true AND  :COLUMNNAME in "+param;
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":COLUMNNAME", columnName);
		return selectTableQuery;
		}

	public static String findValueInTable(String tableName,String columnName, String code) {
		String selectTableQuery="SELECT * FROM :TABLENAME WHERE :SEARCHVALUE = ':PARAM'";
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":SEARCHVALUE", columnName).replace(":PARAM", code);
		 return selectTableQuery;
	}
	public static String getLocationIdsFromTable(String tableName,String columnName, String code) {
		String selectTableQuery="SELECT locationid FROM :TABLENAME WHERE  isActive=true and :SEARCHVALUE = ':PARAM'";
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":SEARCHVALUE", columnName).replace(":PARAM", code);
		 return selectTableQuery;
	}
	/*public static String getLocationIdCountsFromTable(String tableName,String columnName, String code) {
		String selectTableQuery="SELECT locationid FROM :TABLENAME WHERE isActive=true and :SEARCHVALUE = ':PARAM'";
		selectTableQuery=selectTableQuery.replace(":TABLENAME", tableName).replace(":SEARCHVALUE", columnName).replace(":PARAM", code);
		 return selectTableQuery;
	}*/
	public static String updateQuestionData(String table, String originalQueryPart, int id, String columnName) {
				
			String updateQuery = "UPDATE "+table+" SET "+originalQueryPart+" WHERE "+columnName+"= "+id;
		
		return updateQuery;
	}
	
	
	public static String deleteQuestionData(String columnName, String id,	String table) {
		
		String deleteQuery = "UPDATE "+table+" SET isActive=false WHERE "+columnName+" in("+id+")";	
		return deleteQuery;
	}
	
	//@author:Debendra
	public static String deleteManageData(List<String> ids,	String table) {				
		String deleteQuery = "UPDATE "+table+" SET isActive=false WHERE  id  IN "+ids;
		return deleteQuery;
	}
	
	
	public static String getReportsData(int surveyId, String id) {
		
		String getReportsQuery = "select * FROM kpi where surveyId = "+surveyId+" and id= "+id;
		return getReportsQuery;
	}
	
	public static String getColumnWidth(String tableName,String columnName){
		String columnWidthQuery="SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.columns WHERE table_name =':TABLE_NAME' AND COLUMN_NAME=':COLUMN_NAME'";
		columnWidthQuery=columnWidthQuery.replace(":TABLE_NAME", tableName).replace(":COLUMN_NAME", columnName);
		return columnWidthQuery;
	}


	public static String getSurveyDataByLocationId(List<Integer> locationIds,String tableName) {
		String reportDataQuery = "select * from "+ tableName+" where locationid in "+locationIds+" AND isActive=true order by createdDate asc";
		return reportDataQuery;
	}

	public static String  getLocationIdsQuery(String tableName,List<String> param){
		String locationIdQuery ="SELECT id,name FROM "+tableName+ " WHERE id IN "+param+ "AND isActive ="+true;
		return locationIdQuery .replace("[", "(").replace("]", ")");
	}
	
	public static String  getQuestionInformation(String infoType,String selectedInfoType,String xlsColumnName,Integer surveyId){
		String questionInfoQuery ="SELECT questionText,"+infoType+"  ,id FROM question  WHERE surveyId= "+surveyId+ 
									" AND xlsColumnAddress='"+xlsColumnName+
									"'  AND  "+infoType+"='"+selectedInfoType+"'  AND isActive ="+true;
		return questionInfoQuery .replace("[", "(").replace("]", ")");
	}

	
	//create a temp table and copy an existing record from parent table
	//preform the update operation and drop temp table
	//@author:Debendra
	public static String createTempTable(String parentTable,Integer cloneRecordId) {		
		String createTempTableQuery = "CREATE  TABLE  dommy  SELECT * FROM  " +parentTable + "  WHERE id =  "+cloneRecordId;
		return createTempTableQuery;
	}
	public static String updateTmpTable(String updateValue,Integer cloneRecordId) {
		String updateTmpTableQuery = "UPDATE dommy SET " + updateValue+ " WHERE id = "+cloneRecordId;
		return updateTmpTableQuery;
	}
	
	public static String setModifiedByOrDate(String modifiedDate,Integer modifiedUserId,Integer cloneRecordId) {
		String setmodifiedByAndDtaeQuery = "UPDATE dommy SET  modifiedDate='" + modifiedDate+ "' , modifiedBy="+ modifiedUserId+" WHERE id = "+cloneRecordId;
		return setmodifiedByAndDtaeQuery;
	}

	public static String alterTmpTable() {			
		String alterTempTableIdQuery = "ALTER TABLE dommy  drop id";
		return alterTempTableIdQuery;
	}
	public static String cloneDataFromTmpTable(String toTableName) {		
		String cloneRecordToTableQuery = "INSERT INTO " +toTableName+ " SELECT 0,dommy.* FROM dommy";
		return cloneRecordToTableQuery;
	}
	
	public static String dropTmpTable() {		
		String dropTmpTableQuery = "DROP TABLE if exists dommy";		
		return dropTmpTableQuery;
	}
	
	public static String doSoftDelete(String tableName, Integer recordId,boolean flag) {		
		String softDelQuery = "UPDATE  "+ tableName + " SET isActive =" +flag+ " WHERE id =" +recordId;
		return softDelQuery;
	}
	//end of tmptable operations
	
	
	//@author:Debendra
	//get surveyRecord data as per recordId : managedata
	public static String getSurveyRecordByIdQuery(String tableName, Integer recordId) {		
		String getRecordQuery = "SELECT * FROM  "+ tableName+ " WHERE id =" +recordId+ " AND isActive=true";
		return getRecordQuery;
	}


	public static String updateDataOfSurveyTableByLocationId(String table, String columnName, List<Integer> code) {
		String updateSurveyTableQuery = "UPDATE "+table+" SET isActive=false  WHERE "+columnName+" in "+code;
		return updateSurveyTableQuery.replace("[", "(").replace("]", ")");
	}
	
	
	public static String isSurveyEmptyQuery(String tableName){
		String query = "SELECT COUNT(*) FROM " + tableName+ " WHERE isActive=true";
		return query;
	}
	
	public static String getLocationInfoById(String tableName, List<Integer> locationIds){
		String reportDataQuery = "select id, code from "+ tableName+" where id in "+locationIds;
		return reportDataQuery;
	}
	
	public static String getLocationIdsFromSurveyTable(String tableName){
		String query = "select locationid from "+tableName+" where isActive=true";
		return query;
	}
	
	public static String getLocationsByVillageIds(List<Integer> villageIds){
		String query = "from Village as village LEFT JOIN FETCH village.revenueVillage revenue "
				+ "LEFT JOIN FETCH revenue.panchayat gramPanchayat "
				+ "LEFT JOIN FETCH gramPanchayat.nyayPanchayat nyayPanchayat "
				+ "LEFT JOIN FETCH nyayPanchayat.block block "
				+ "LEFT JOIN FETCH block.tehsil tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where village.id in "+villageIds+" and village.isActive=true order by village.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByRevenueIds(List<Integer> revenueIds){
		String query = "from RevenueVillage as revenueVillage "
				+ "LEFT JOIN FETCH revenueVillage.panchayat gramPanchayat "
				+ "LEFT JOIN FETCH gramPanchayat.nyayPanchayat nyayPanchayat "
				+ "LEFT JOIN FETCH nyayPanchayat.block block "
				+ "LEFT JOIN FETCH block.tehsil tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where revenueVillage.id in "+revenueIds+" and revenueVillage.isActive=true order by revenueVillage.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByGramPanchayatIds(List<Integer> gPanchayatIds){
		String query = "from Panchayat as gramPanchayat "
				+ "LEFT JOIN FETCH gramPanchayat.nyayPanchayat nyayPanchayat "
				+ "LEFT JOIN FETCH nyayPanchayat.block block "
				+ "LEFT JOIN FETCH block.tehsil tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where gramPanchayat.id in "+gPanchayatIds+" and gramPanchayat.isActive=true order by gramPanchayat.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByNyayPanchayatIds(List<Integer> nPanchayatIds){
		String query = "from NyayPanchayat as nyayPanchayat "
				+ "LEFT JOIN FETCH nyayPanchayat.block block "
				+ "LEFT JOIN FETCH block.tehsil tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where nyayPanchayat.id in "+nPanchayatIds+" and nyayPanchayat.isActive=true order by nyayPanchayat.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByBlockIds(List<Integer> blockIds){
		String query = "from Block as block "
				+ "LEFT JOIN FETCH block.tehsil tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where block.id in "+blockIds+" and block.isActive=true order by block.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByTehsilIds(List<Integer> tehsilIds){
		String query = "from Tehsil as tehsil "
				+ "LEFT JOIN FETCH tehsil.district district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where tehsil.id in "+tehsilIds+" and tehsil.isActive=true order by tehsil.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByDistrictIds(List<Integer> districtIds){
		String query = "from District as district "
				+ "LEFT JOIN FETCH district.zone zone LEFT JOIN FETCH zone.state state where district.id in "+districtIds+" and district.isActive=true order by district.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByZoneIds(List<Integer> zoneIds){
		String query = "from Zone as zone LEFT JOIN FETCH zone.state state where zone.id in "+zoneIds+" and zone.isActive=true order by zone.zoneName";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	
	public static String getLocationsByStateIds(List<Integer> stateIds){
		String query = "from State as state where state.id in "+stateIds+" and state.isActive=true order by state.name";
		query = query.replace("[", "(").replace("]", ")");
		return query;
	}
	

	
	public static String getLegendsFromSurvey(String tableName,String colName){
		String query = "SELECT "+colName +" FROM "+ tableName+" where  isActive=true";
		return query;
	}
	public static String getQuestionDataByInfoTypes(Integer surveyId, List<String> infoTypeList,
			List<String> infoSubTypeList, List<String> infoAreaList){
		String infoType = "", infoSubType = "", infoArea = "", query = "", queryEnd = "";
		if(infoTypeList != null){	
			if(isBlankIncluded(infoTypeList)){
				if(infoTypeList.size()>1){
					query = " AND ( informationType in (";
					queryEnd = " OR informationType is null) ";
					infoType = generateCondition(infoTypeList, query, queryEnd);
				}else{
					infoType = " AND informationType is null ";
				}				
			}else{
				query = " AND informationType in (";
				queryEnd = "";
				infoType = generateCondition(infoTypeList, query, queryEnd);
			}
		}
		if(infoSubTypeList != null){			
			if(isBlankIncluded(infoSubTypeList)){
				if(infoSubTypeList.size()>1){
					query = " AND ( informationSubType in (";
					queryEnd = " OR informationSubType is null) ";
					infoSubType = generateCondition(infoSubTypeList, query, queryEnd);
				}else{
					infoSubType = " AND informationSubType is null ";
				}				
			}else{
				query = " AND informationSubType in (";
				queryEnd = "";
				infoSubType = generateCondition(infoSubTypeList, query, queryEnd);
			}
		}
		if(infoAreaList != null){
			if(isBlankIncluded(infoAreaList)){
				if(infoAreaList.size()>1){
					query = " AND ( informationArea in (";
					queryEnd = " OR informationArea is null) ";
					infoArea = generateCondition(infoAreaList, query, queryEnd);
				}else{
					infoArea = " AND informationArea is null ";
				}				
			}else{
				query = " AND informationArea in (";
				queryEnd = "";
				infoArea = generateCondition(infoAreaList, query, queryEnd);
			}
		}
		String questionInfoQuery ="from Question as question where question.survey.id= "+surveyId+" "
				+ ""+infoType+" "+infoSubType+" "+infoArea;
		return questionInfoQuery;
	}
	
	
	public static String generateCondition(List<String> list, String conditionString, String queryEnd){
		StringBuilder sb = new StringBuilder();
		sb.append(conditionString);
		for(int index = 0; index < list.size(); index++){
			String condition = list.get(index).toString();
			if(!condition.equals("Blank")){
				condition = condition.replaceAll("'", "''");
				sb.append("'"+condition+"',");
			}
		}
		String query = sb.toString().substring(0, sb.lastIndexOf(","));
		return query+")"+queryEnd;
	}
	
	public static boolean isBlankIncluded(List<String> list){		
		return list.contains("Blank");
	}
}
