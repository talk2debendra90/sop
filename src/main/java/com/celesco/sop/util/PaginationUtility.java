package com.celesco.sop.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.celesco.sop.dto.PaginationVm;

public class PaginationUtility<T> {

	public static Pageable createPageRequest(PaginationVm<?> paginationVm) {
			
		if(paginationVm==null){
			return null;
		}	
		
		if(paginationVm.getLimit() == 0)
				return null;
			
			Sort.Direction directionType = paginationVm.getSort() == null || 
											!paginationVm.getSort().equalsIgnoreCase("desc")?
													Sort.Direction.ASC: Sort.Direction.DESC;
			boolean isSortable =paginationVm.getFields()!=null?!paginationVm.getFields().isEmpty()?true:false:false;
			if(isSortable){
				return  new PageRequest(paginationVm.getPageNumber()-1,paginationVm.getLimit(),
						new Sort(directionType,paginationVm.getFields()));
			}else{
				return  new PageRequest(paginationVm.getPageNumber()-1,paginationVm.getLimit());
			}
		}

}
