package com.celesco.sop.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class GenericJDBCUtility {
	private GenericJDBCUtility() {
		
	}
public static ResultSet getResultSet (Connection conn,String query) throws SQLException{
		PreparedStatement st = conn.prepareStatement(query);
		return st.executeQuery(query);
	}
}
