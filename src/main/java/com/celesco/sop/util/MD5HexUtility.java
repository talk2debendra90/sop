package com.celesco.sop.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.UUID;

import org.apache.log4j.Logger;

public class MD5HexUtility {
	private static final Logger logger = Logger.getLogger(MD5HexUtility.class);

	public static String getSecurePassword(String passwordToHash){		
		String salt;		
		String generatedPassword = null;
		try {
			salt=MD5HexUtility.getSalt();           
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(salt.getBytes());
			byte[] bytes = md.digest(passwordToHash.getBytes());
			StringBuilder sb = new StringBuilder();
			for(int i=0; i< bytes.length ;i++){
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		}
		catch (NoSuchAlgorithmException nsae){
			nsae.printStackTrace();        	
			logger.error(nsae);
			return nsae.getMessage();
		} catch(NoSuchProviderException e) {
			e.printStackTrace();
			logger.error(e);
			return e.getMessage();
		}
		return generatedPassword;
	}

	//Add salt
	private static String getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
	{
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");     
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return new String(salt);
	}


	public static String getUUID(){
		return  UUID.randomUUID().toString();
	}
	
	
	
	public static String getSecureString(String string){		
		String salt;		
		String generatedSecureString = "";
		if(string!=null){
			try {
				salt=MD5HexUtility.getSalt();           
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(salt.getBytes());
				byte[] bytes = md.digest(string.getBytes());
				StringBuilder sb = new StringBuilder();
				for(int i=0; i< bytes.length ;i++){
					sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
				}
				generatedSecureString = sb.toString();
			}
			catch (NoSuchAlgorithmException nsae){
				nsae.printStackTrace();        	
				logger.error(nsae);
				return nsae.getMessage();
			} catch(NoSuchProviderException e) {
				e.printStackTrace();
				logger.error(e);
				return e.getMessage();
			}	
		}
		return generatedSecureString;
	}
	
}
