package com.celesco.sop.util;

public class SOPConstants {
    private SOPConstants() {

    }
    public static final char EXTENSION_SEPARATOR = '.';
    public static final String INTEGER_REGEX = "\\d+";
    public static final String ALPHANUM_REGEX = "^[0-9A-Za-z](\\w)*$";

    public static final String LOCATION_ALREADY_EXIST="Location already exist. Please specify different name.";
    public static final String DATA_ALREADY_EXIST="This already exists. Please choose different one.";

    public static final String STATE_ZONE_RELATION_MESSAGE="There are some Zones associated with this State. So you cannot delete this data.";
    public static final String ZONE_DISTRICT_RELATION_MESSAGE="There are some Districts associated with this Zone. So you cannot delete this data.";
    public static final String DISTRICT_TEHSIL_RELATION_MESSAGE="There are some Tehsils associated with this District. So you cannot delete this data.";
    public static final String TEHSIL_BLOCK_RELATION_MESSAGE="There are some Blocks or Cities associated with this Tehsil. So you cannot delete this data.";
    public static final String BLOCK_NAYA_PANCHAYAT_RELATION_MESSAGE="There are some Nyaya Panchayats associated this with this Block. So you cannot delete this data.";
    public static final String NAYA_PANCHAYAT_GRAM_PANCHAYAT_RELATION_MESSAGE="There are some Gram Panchayats associated this with this Nyaya Panchayat. So you cannot delete this data.";	
    public static final String GRAM_PANCHAYAT_REVENUE_VILLAGE_RELATION_MESSAGE="There are some Revenue Villages associated this with this Gram Panchayat. So you cannot delete this data.";
    public static final String REVENUE_VILLAGE_VILLAGE_RELATION_MESSAGE="There are some Villages associated this with this Revenue Village. So you cannot delete this data.";
    public static final String INSTITUTIONTYPE_INSTITUTION_RELATION_MESSAGE="There are some Institutions associated with this InstitutionType so you cannot delete this data";


    public static final String SOURCE_ASSESSMENT_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this Source so you cannot delete this data";
    public static final String STAGE_ASSESSMENT_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this Stage so you cannot delete this data";
    public static final String GRADE_ASSESSMENT_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this Grade so you cannot delete this data";
    public static final String SUBJECT_ASSESSMENT_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this Subject so you cannot delete this data";
    public static final String UNIT_ASSESSMENT_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this Chapter so you cannot delete this data";
    public static final String UNIT_ASSESSMENT_SUBJECT__GRADE_RELATIONSHIP_MESSAGE="There are some Question Papers associated with this  chapter. So you can't delete this chapter.";
    public static final String UNIT_QUESTION_BANK_RELATIONSHIP_MESSAGE="There are some Question bank associated with this chapter. So you can't delete this chapter.";




    public static final String INVALID_USERID_PASSWORD_PATTERN = null;
    public static final String USER_EAMIL_ALREADY_EXISTS=" Email already exists!!";
    public static final String USER_ALREADY_EXIST="User already exist with same email or login name.";
    public static final String USER_LOGIN_NAME_ALREADY_EXIST="Login name already exist. Try with another";
    public static final String USER_CAN_NOT_DELETE ="Record Couldn't be deleted. User has been attached with some activity.";



    public static final String DATABASE_CONNECTION_ERROR="Could not connect to database. Please contact to IT support ";	
    public static final String DATABASE_ERROR="A database error occurred while processing this request. Please contact to IT support ";
    public static final String DATABASE_COLUMNLIMIT ="Entered data too long. Can not process request. Please contact IT support.";

    //for smart-filter keys
    public static final String LOCATION_STATE ="State";
    public static final String LOCATION_ZONE ="Zone";
    public static final String LOCATION_DISTRICT="District";
    public static final String LOCATION_TEHSIL="Tehsil";
    public static final String LOCATION_BLOCK="Block";
    public static final String LOCATION_CITY="City";
    public static final String LOCATION_NP="Nyaya Panchayat";
    public static final String LOCATION_GP="Gram Panchayat";
    public static final String LOCATION_RV="Revenue Village";
    public static final String LOCATION_VILLAGE="Village";
    public static final String SMART_FILTER_SCHOOL="School";
    public static final String SMART_FILTER_CENTER="Center";

    // for smart-filter campaign
    public static final String LOCATION_SOURCE ="Source";
    public static final String LOCATION_STAGE = "Stage";

    //for robo selection of campaign in plan assessment view  keys
    public static final String ELEMENT_CAMPAIGN ="Campaign";
    public static final String ELEMENT_SCHOOL ="School";
    public static final String ELEMENT_GRADE ="Grade";
    public static final String ELEMENT_SECTION ="Section";
    public static final String ELEMENT_SUBJECT ="Subject";


    public static final String LOCATION_TYPE_RURAL="Rural";
    public static final String LOCATION_TYPE_RURAL_CODE="R";
    public static final String LOCATION_TYPE_URBAN="Urban";
    public static final String LOCATION_TYPE_URBAN_CODE="U";



    public static final String UPDATE_MESSAGE="Information Updated";
    public static final String UPLOAD_MESSAGE="Upload successfully";

    public static final String SAVE_MESSAGE="added";
    public static final String DELETE_MESSAGE="deleted";
    public static final String DELETE_FAILURE_MESSAGE="Can't Delete";

    public static final String SUCCESS_MESSAGE_CONSTANT ="sop-200";
    public static final String FAILURE_MESSAGE_CONSTANT ="sop-400";
    public static final String PARTIALLY_UPDATED ="sop-800";
    public static final String FAILURE_INPUT_NULL ="sop-405";
    public static final String FAILURE_MESSAGE_NOT_EXISTS_CONSTANT ="shiksha-404";
    public static final String FAILURE_MESSAGE_NOT_EXISTS ="This record is no more exists. Please contact IT-support.";
    public static final String FAILURE_CLOSE_CONSTANT ="shiksha-600";
    public static final String NA ="N/A";
    public static final String ALREADY_EXIST="shiksha-800:The record name already exists.";
    public static final String FALSE="false";


    public static final String DATE_FORMAT="YYYY-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_WITH_TIME_ZONE="YYYY-MM-dd HH:mm:ssZ";
    public static final String CODE_ALREADY_EXIST = "Code already exist. Please specify different code.";


    public static final String SCHOOL_ALREADY_EXIST="School already exist. Please specify different name.";

    public static final Integer STATUS_YET_TO_START=1;
    public static final Integer STATUS_IN_PROGRESS=2;
    public static final Integer STATUS_COMPLETED=3;
    public static final Integer STATUS_SKIPPED=4;
    public static final Integer CLOSED_BY_FS=6;
    public static final Integer CLOSED_BY_RC=5;
    public static final Integer STATUS_FINALIZED=7;
    public static final Integer STATUS_DRAFT=8;
    public static final Integer STATUS_COMPLETED_LATE=9;
    public static final String STATUS_YET_TO_START_STRING="Yet to start";
    public static final String STATUS_IN_PROGRESS_STRING="In progress";
    public static final String STATUS_COMPLETED_STRING="Completed";
    public static final String STATUS_SKIPPED_STRING="Skipped";
    public static final String STATUS_PENDING="Pending";
    //public static final String STATUS_COMPLETED_LATE_STRING="Completed Late";
    public static final String CLOSED_BY_FS_STRING="Closed By FS";
    public static final String CLOSED_BY_RC_STRING="Closed By RC";
    public static final String STATUS_FINALIZED_STRING="Finalized";
    public static final String STATUS_DRAFT_STRING="Draft";




    public static final String SCHOOL_USER_RELATIONSHIP_MESSAGE = "There are some Users associated with this School so you cannot delete this data";
    public static final String SCHOOL_CAMPAIGN_RELATIONSHIP_MESSAGE = "There are some projects associated with this School so you cannot delete this data";
    public static final String SCHOOL_SCHOOLGRADESECTION_RELATIONSHIP_MESSAGE = "There are some Grades associated with this School so you cannot delete this data";
    public static final String SCHOOL_CAMPAIGN_RELATIONSHIP_DEACTIVATE_MESSAGE = "There are some projects associated with this School in current Academic Year so you cannot inactive this data";




    //Report pathVarName Constants
    public static final String REPORT1PATHVAR ="1_STUDENT_PERFORMANCE_BOYS_VS_GIRLS";
    public static final String REPORT2PATHVAR ="2_STUDENT_PERFORMANCE_DISTRICT_BOYS_VS_GIRLS";
    public static final String REPORT3PATHVAR ="3_STUDENT_PERFORMANCE_BY_BLOOM";
    public static final String REPORT4PATHVAR ="4_ATTENDENCE_VS_TEST_SCORE";
    public static final String REPORT5PATHVAR ="5_ATTENDENCE_OVER_MONTH_BOYS_VS_GIRLS";
    public static final String REPORT6PATHVAR ="6_GIRLS_PERFORMANCE_OVER_CHAPTER";
    public static final String REPORT7PATHVAR ="7_BOYS_PERFORMANCE_OVERE_CHAPTER";
    public static final String REPORT8PATHVAR ="8_TEST_SCORE_AND_CONTENT_DELIVERY_TIME";
    public static final String REPORT9PATHVAR ="9_PVT_GOVT_SCHOOL_PERFORMANCE";

    //Report Title Constants
    public static final String REPORT1TITLE ="Student performance (Boys vs Girls)";
    public static final String REPORT2TITLE ="Student Performance DistrictWise(Boys vs Girls)";
    public static final String REPORT3TITLE ="Student Performance (Basis of five scholastic dimension)";
    public static final String REPORT4TITLE ="Attendance vs test score";
    public static final String REPORT5TITLE ="Attendance over the month (Boys vs Girls)";
    public static final String REPORT6TITLE ="Girls Performance Over the Chapter";
    public static final String REPORT7TITLE ="Boys Performance Over the Chapter";
    public static final String REPORT8TITLE ="Test score and content delivery time";
    public static final String REPORT9TITLE ="Private vs Government schools performance";
    //Report View Name Constants

    public static final String REPORT1 ="report1";
    public static final String REPORT2 ="report2";
    public static final String REPORT3 ="report3";
    public static final String REPORT4 ="report4";
    public static final String REPORT5 ="report5";
    public static final String REPORT6 ="report6";
    public static final String REPORT7 ="report7";
    public static final String REPORT8 ="report8";
    public static final String REPORT9 ="report9";


    //For Password Constants
    public static final String FORGOT_PASSWORD_EMAIL_SUBJECT="Shiksha Notification: Forgot Password?";
    public static final String GRADE_SCHOOLGRADESECTION_RELATIONSHIP_MESSAGE = "There are some Schools associated with this Grade so you cannot delete this data";
    public static final String GRADE_GRADESUBJECTUNIT_RELATIONSHIP_MESSAGE = "There are some Subjects associated with this Grade so you cannot delete this data";
    public static final String SECTION_SCHOOLGRADESECTION_RELATIONSHIP_MESSAGE = "There are some Grades associated with this Section so you cannot delete this data";
    public static final String SUBJECT_GRADESUBJECTUNIT_RELATIONSHIP_MESSAGE = "There are some chpaters associated with this Subjects so you cannot delete this data";
    public static final String UNIT_GRADESUBJECTUNIT_RELATIONSHIP_MESSAGE = "There are some Subjects associated with this Chapters so you cannot delete this data";
    public static final String CITY_SCHOOL_RELATIONSHIP_MESSAGE = "There are some Schools associated with this city so you cannot delete this data";
    public static final String VILLAGE_SCHOOL_RELATIONSHIP_MESSAGE = "There are some Schools associated with this village so you cannot delete this data";
    public static final String ASSESSMENT_GRADESUBJECTUNIT_RELATIONSHIP_MESSAGE = "There are some Question Papers associated with this mapping so you cannot delete this data";
    public static final String USER_ROLE_RELATIONSHIP_MESSAGE = "There are some Users associated with this role so you cannot delete this data";
    public static final String ASSESSMENTPLAN_AUTO_START_MSG = " This Question Paper is auto started";
    public static final String FORGOT_PASSWORD_EMAIL_BODY=" Click below link to reset your password.";
    public static final String ASSESSMENT_NOT_STARTED ="Question Paper not started";
    public static final String ASSESSMENT_ALREADY_STARTED ="Question Paper already started";
    public static final String ASSESSMENT_NOT_FOUND ="Question Papper not found";	
    public static final String NO_STUDENT_FOR_ASSESSMENT ="Question Paper have no student";
    public static final String ASSESSMENT_AUTO_CLOSED =" This Question Paper is auto closed";
    public static final String CAMPAIGN_NOT_STARTED = "Project not started";
    public static final String RollNumber_ALREADY_EXIST = "Roll No. already exist. Please specify different Number";
    public static final Integer STAGE_PT_ID = 1;
    public static final Integer STAGE_PAT_ID = 2;
    public static final String STAGE_PT_CODE = "PT";
    public static final String STAGE_PAT_CODE = "PAT";
    public static final String SCHOOL_GRADE_SECTION_MAPPING_ERROR = "SCHOOL_GRADE_SECTION_MAPPING_ERROR";
    public static final String NAME_COLUMN = "name";
    public static final String ISACTIVE_TRUE_STRING = "isActive=true";
    public static final Integer[] GRADE_SEQUENCE_NUMBERS = {1,2,3,4,5,6,7,8,9,10};
    public static final String CT_USER="CT";
    public static final String SHIKSHA_NOTIFICATION="Shiksha Notification";
    public static final String WEBAPPINFOEMAIL ="shikshawebappinfo@shikshainitiative.org";
    public static final String WEBAPPINFOPHONE ="+91 (0120) 6900012";
    public static final String MALE = "MALE";
    public static final String FEMALE = "FEMALE";
    public static final String KNOWLEDGE = "Knowledge";
    public static final String OPERATION = "Operation";
    public static final String ANALYSIS = "Analysis";
    public static final String UNDERSTANDING = "Understanding";
    public static final String APPLICATION = "Application";
    public static final CharSequence SHIKSHA_SUPPORT_EMAIL = "shikshawebappinfo@shikshainitiative.org";
    public static final String TEACHER = "Teacher";
    public static final String TEACHER_ROLE_CODE = "T";
    public static final String ROLE_CT_CODE="CT";
    public static final String ROLE_SU_CODE="SU";
    public static final String ROLE_RC_CODE="RC";
    public static final String ROLE_FS_CODE = "FS";
    public static final String ROLE_ITE_CODE = "IT-E";
    public static final String EDITED = "edited";
    public static final String ADDED = "added";
    public static final String DELETED = "deleted";
    public static final String UPDATED = "updated";
    public static final String ACTIVATED = "activated";
    public static final String INACTIVATED = "inactivated";
    public static final String FSORITE = "FS or IT-E";
    public static final String ASSIGNED = "assigned";
    public static final String STARTED = "started";
    public static final String CLOSED = "closed";
    public static final String SHIKSHA_NOTIFICATION_CHANGE_PASSWORD = "Shiksha Notification: Password changed";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_ADDED="Shiksha Notification: New School Added";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_DELETED="Shiksha Notification: School Deleted";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_ACTIVATED="Shiksha Notification: School Activated";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_INACTIVATED="Shiksha Notification: School Inactivated";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_EDITED="Shiksha Notification: School Edited";
    public static final String SHIKSHA_NOTIFICATION_TEACHER_ADDED="Shiksha Notification: Teacher Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_TEACHER_EDITED="Shiksha Notification: Teacher Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_INSTRUCTOR_ADDED="Shiksha Notification: Instructor Added";
    public static final String SHIKSHA_NOTIFICATION_INSTRUCTOR_EDITED="Shiksha Notification: Instructor Edited";
    public static final String SHIKSHA_NOTIFICATION_INSTRUCTOR_DELETED="Shiksha Notification: Instructor Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_TEACHER_DELETED="Shiksha Notification: Teacher Deleted";
    public static final String SHIKSHA_NOTIFICATION_STUDENT_ADDED="Shiksha Notification: Student Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_STUDENT_EDITED="Shiksha Notification: Student Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_STUDENT_DELETED="Shiksha Notification: Student Deleted";
    public static final String SHIKSHA_NOTIFICATION_FS_OR_ITE_ADDED="Shiksha Notification: FS or IT-E Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_SUPERVISOR_ADDED="Shiksha Notification: Supervisor Added";
    public static final String SHIKSHA_NOTIFICATION_SUPERVISOR_EDITED="Shiksha Notification: Supervisor Edited";
    public static final String SHIKSHA_NOTIFICATION_FS_OR_ITE_EDITED="Shiksha Notification: FS or IT-E Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_FS_OR_ITE_DELETED="Shiksha Notification: FS or IT-E Deleted";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_ASSIGNED_TO_FS="Shiksha Notification: School Assigned to FS";
    public static final String SHIKSHA_NOTIFICATION_CENTER_ASSIGNED_TO_IST="Shiksha Notification: Center Assigned to Instructor";
    public static final String SHIKSHA_NOTIFICATION_CENTER_DELETED_FROM_IST="Shiksha Notification: Center Deleted from Instructor";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_DELETED_FROM_FS="Shiksha Notification: School Deleted from FS";
    public static final String SHIKSHA_NOTIFICATION_RC_ADDED="Shiksha Notification: RC Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_RC_EDITED="Shiksha Notification: RC Added/Edited";
    public static final String SHIKSHA_NOTIFICATION_PROJECT_ADDED="Shiksha Notification: Project Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_PROJECT_EDITED="Shiksha Notification: Project Edited";
    public static final String SHIKSHA_NOTIFICATION_PROJECT_DELETED="Shiksha Notification: Project Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_PROJECT_CLOSED="Shiksha Notification: Project Closed";
    public static final String SHIKSHA_NOTIFICATION_PROJECT_STARTED="Shiksha Notification: Project Started";
    public static final String SHIKSHA_NOTIFICATION_SCHOOL_ASSIGNED_TO_PROJECT="Shiksha Notification: School(s) Assigned to Project";
    public static final String SHIKSHA_NOTIFICATION_CHANGING_SCHOOL_AND_QUESTION_PAPER_FOR_PROJECT="Shiksha Notification: School(s) or Question Paper(s) changed for the Project";
    public static final String SHIKSHA_NOTIFICATION_RC_PASSWORD_CHANGED = "Shiksha Notification: RC Password Changed";
    public static final String SHIKSHA_NOTIFICATION_FS_PASSWORD_CHANGED = "Shiksha Notification: FS Password Changed";
    public static final String SHIKSHA_NOTIFICATION_TEACHER_PASSWORD_CHANGED = "Shiksha Notification: Teacher Password Changed";
    public static final String SHIKSHA_NOTIFICATION_STUDENT_GRADE_PROMOTION = "Shiksha Notification: Student promoted to next Grade";
    public static final String SHIKSHA_NOTIFICATION_CHAPTER_ADDED = "Shiksha Notification: Chapter Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_CHAPTER_DELETED = "Shiksha Notification: Chapter Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_QUESTION_PAPER_VERSION_CHANGED = "Shiksha Notification: Question Paper Version changed";
    public static final String SHIKSHA_NOTIFICATION_QUESTION_PAPER_PLAN_DTAE_CHANGED="Shiksha Notification: Question Paper Plan Start/End date is modified";
    public static final String SHIKSHA_NOTIFICATION_SUBJECT_EDITED="Shiksha Notification: Subject Edited";
    public static final String SHIKSHA_NOTIFICATION_SUBJECT_ADDED="Shiksha Notification: New Subject Added";
    public static final String SHIKSHA_NOTIFICATION_TAXONOMY_DELETED="Shiksha Notification: Taxonomy Deleted";
    public static final String SHIKSHA_NOTIFICATION_TAXONOMY_ADDED="Shiksha Notification: New Taxonomy Added";
    public static final String SHIKSHA_NOTIFICATION_QUESTION_ADDED="Shiksha Notification: Question Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_QUESTION_DELETED="Shiksha Notification: Question Added/Deleted";
    public static final String SHIKSHA_NOTIFICATION_ISSUE_ALERT_IN_PROJECT="Shiksha Notification: Project Alert! ";
    public static final String SHIKSHA_NOTIFICATION_SYNC_ALERT="Shiksha Notification: Sync Alert! ";
    public static final String SHIKSHA_NOTIFICATION_USER_PASSWORD_RESET = "Shiksha Notification: Password reset";
    public static final String STUDENT_QUESTION_PAPER_RELATION="Some Question Paper plan associated with this Student so you cannot delete this Student";

    public static final String SHIKSHA_NOTIFICATION_SUBJECT_DESKTOP_VERSION="Shiksha Notification: Desktop version";
    public static final String SHIKSHA_NOTIFICATION_SUBJECT_TABLET_VERSION="Shiksha Notification: Tablet version";

    public static final String SHIKSHA_NOTIFICATION_DESKTOP_VERSION="A new Desktop APP version- @VERSION  is available. Please download it from  @DOWNLOAD";
    public static final String SHIKSHA_NOTIFICATION_TABLET_VERSION="A new Tablet APP version- @VERSION  is available. Please download it from @DOWNLOAD";
    public static final String OPEN_LAB_GOVT_SCHOOL="Open Lab-Govt";
    public static final String OPEN_LAB_PVT_SCHOOL="Open Lab-Pvt";
    public static final String THETA_GOVT_SCHOOL="Theta-Govt";
    public static final String THETA_PVT_SCHOOL="Theta-Pvt";
    public static final String GOVERNMENT="'Government'";
    public static final String GOVT="Govt";
    public static final String PVT="Pvt";
    public static final String PRIVATE="Private";
    public static final Integer[] MONTH_INDEX = {4,5,6,7,8,9,10,11,12,1,2,3};
    public static final String[] MONTH_NAMES = { "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec","Jan", "Feb", "Mar"};

    public static final String [] SECTOR_AND_SCHOOL_TYPE_LABELS={OPEN_LAB_GOVT_SCHOOL, THETA_GOVT_SCHOOL, OPEN_LAB_PVT_SCHOOL, THETA_PVT_SCHOOL };
    public static final Integer [] SECTOR_AND_SCHOOL_TYPE_INDEX={1,2,3,4};
    public static final String [] TAXONOMY_LABELS={"K", "U", "O", "An","Ap" };
    public static final Integer [] TAXONOMY_INDEX={1,2,3,4,5};
    public static final String [] SCHOOL_TYPE_LABELS={GOVT, PRIVATE};
    public static final Integer [] SCHOOL_TYPE_INDEX={1,2};

    public static final String THETA = "Theta";
    public static final String OPENLAB = "Open Lab";
    public static final String [] OPENLAB_TYPE_LABELS={OPENLAB,THETA};
    public static final Integer [] OPENLAB_TYPE_INDEX={1,2};
    public static final String THETA_NO = "0";
    public static final String OPENLAB_NO = "1";
    public static final String GOVT_NO = "2";
    public static final String PVT_NO = "1";
    public static final String QUERY_OPENLAB = "@#OPENLAB";
    public static final String QUERY_SCHOOLTYPE = "@#SCHOOLTYPE";
    public static final String QUERY_SCHOOLTYPEID = "@#SCHOOLTYPEID";
    public static final String QUERY_ACADEMICYEARS = "@#ACADEMICYEARS";
    public static final String QUERY_STATES = "@#STATES";
    public static final String QUERY_STATEIDS ="@#STATEIDS";
    public static final String QUERY_DISTRICTS = "@#DISTRICTS";
    public static final String QUERY_DISTRICTIDS = "@#DISTRICTIDS";
    public static final String QUERY_BLOCKS = "@#BLOCKS";
    public static final String QUERY_STATEID="@#STATE_ID";
    public static final String QUERY_GRADEID="@#GRADEID";
    public static final String QUERY_PROJECTID="@#PROJECTID";
    public static final String QUERY_SUBJECTID="@#SUBJECTID";
    public static final String QUERY_ASSESSMENTIDS = "@#ASSESSMENTIDS";
    public static final String QUERY_ROLEID = "@#ROLEID";
    public static final String QUERY_ROLECODE = "@#ROLECODE";
    public static final String QUERY_USERID="@#USERID";
    public static final String QUERY_SCHOOLIDS="@#SCHOOLIDS";
    public static final String QUERY_GRADEIDS="@#GRADEIDS";
    public static final String QUERY_SUBJECTIDS="@#SUBJECTIDS";
    public static final String QUERY_STAGEID="@#STAGEID";
    public static final String QUERY_ACADEMICYEAR = "@#ACADEMICYEAR";
    public static final String QUERY_GENDER = "@#GENDER";
    public static final String QUERY_GENDERS = "@#GENDERS";
    public static final String QUERY_OPENLAB_SCHOOLTYPE = "@#OPENLAB_SCHOOLTYPE_FILTER_QUERY";
    public static final String QUERY_TABLENAME = "@#TABLE";
    public static final String GRADE_TABLE_NAME = "grade";
    public static final String PROJECT_TABLE_NAME = "campaign";
    public static final String NAME_STRING = "name";
    public static final String ID_STRING = "id";
    public static final String NAME_COLUMN_STRING="@#NAMECOLUMN";
    public static final String OPENLAB_SCHOOLTYPE_FILTER_QUERY=" and schltype.id=@#SCHOOLTYPE and schl.isOpenLab =@#OPENLAB ";
    public static final String AVERAGE_STRING = "Average";
    public static final String LINE_STRING = "line";
    public static final String BAR_STRING = "bar";
    public static final String TOTAL_STRING = "total";
    public static final String TOTAL_SCHOOL_STRING="totalSchool";
    public static final String OPEN_LAB_STRING="OpenLab";
    public static final String TOTAL_STUDENTS_STRING="totalStudents";
    public static final String QUERY_ACADEMICYEARID = "@#ACADEMICYEARID";
    public static final String QUERY_LOCATIONIDS = "@#LOCATIONIDS";
    public static final String GENDER_FILTER = "@#GENDER_FILTER";
    public static final String GENDER_FILTER_QUERY=" and std.gender= '@#GENDER' ";
    public static final String TOTAL = "Total";
    public static final String MALE_STRING = "Male";
    public static final String FEMALE_STRING = "Female";
    public static final String QUERY_FROMDATE = "@#FROMDATE";
    public static final String QUERY_TODATE = "@#TODATE";
    public static final String ENROLLED="Enrolled";
    public static final String APPEARED="Appeared";
    public static final String ABSENT="Absent";
    public static final String PENDING="Pending";
    public static final String SCORED_GTE90="Scored >= 90%";
    public static final String SCORED_LTE90="Scored < 90%";
    public static final String ATTEMPTED="Attempted";
    public static final String[] RCFSPERFORMANCE_LABELS ={ENROLLED,APPEARED,ABSENT,PENDING,SCORED_GTE90, SCORED_LTE90 };
    public static final Integer [] RCFSPERFORMANCE_INDEX ={1,2,3,4};
    public static final String[] PERFORMANCE_LABELS ={"Total", ">=90%", "<90%"};
    public static final Integer [] PERFORMANCE_INDEX ={1,2};
    public static final String GOVT_PT="Govt-PT";
    public static final String GOVT_PAT="Govt-PAT";
    public static final String PVT_PT="Pvt-PT";
    public static final String PVT_PAT="Pvt-PAT";

    public static final String OPENLAB_PAT="Open Lab-PAT";
    public static final String OPENLAB_PT="Open Lab-PT";
    public static final String THETA_PAT="Theta-PAT";
    public static final String THETA_PT="Theta-PT";
    public static final int MAX_FRACTIONAL_DIGIT = 1;
    public static final String STRING_LABEL_ID="labelId";
    public static final String STRING_TOTAL_STUDENT="totalstudents";
    public static final String DUPLICATE_EMAIL_FOUND = "This Email is associated with more then one User. Please contact to Administrator ";
    public static final String EMAIL_NOT_REGISTERED = "This Email is not associated with any user. Please enter correct Email";
    public static final String TOTAL_PROJECT_STRING = "totalProject";
    public static final String MONTH_NUM_STRING = "monNumber";
    public static final CharSequence QUERY_FLAG_FILTER = "@#FLAGFILTER";
    public static final CharSequence QUERY_CSTATUS_ID_3 = " and cstatus.statusId=3 ";
    public static final String ACADEMIC_YEAR_HAVE_CAMPAIGN = "There are some Project associated with Academic Year have start date or end date not in Academic Year Span";
    public static final String ACADEMIC_YEAR_HAVE_CAMPAIGN_START_DATE = "There are some projects associated with current edited start date of this academic year.";
    public static final String ACADEMIC_YEAR_HAVE_CAMPAIGN_END_DATE = "There are some projects associated with current edited end date of this academic year.";


    public static final String DATE_FORMAT_DD_MMM="dd-MMM";
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";
    public static final String ACADEMIC_YEAR_EXIST="Academic Year already exist";
    public static final String INVALID_ACADEMIC_YEAR="Academic Year start date and end date years can't be same or can not be more than one year span";

    public static final String INVALID_ACADEMIC_YEAR_SPAN="Start date/End date are not following the edited Academic Year span.";
    public static final String INVALID_CONSECUTIVE_ACADEMIC_YEAR_SPAN="Start date/End date are not following the consecutive Academic Year span.";

    public static final String PREVIOUS_YEAR_EXIST="Previous Academic Year already Exist.";
    public static final String CURRENT_YEAR_EXIST="Current Academic Year already Exist.";


    public static final String ACTIVITY_COMPLTED=" completed ";
    public static final String ACTIVITY_STARTED="  started ";
    public static final String ACTIVITY_BADGE_AWARDED=" awarded a badge ";
    public static final String TOPIC_TAG="Topic";
    public static final String SUBJECT_TAG = "Subject";
    public static  final String STRING_ACCESS_TOKEN="ACCESS_TOKEN" ;
    public static  final String STRING_SURVEY_CODE="SURVEY_CODE";
    public static  final String STRING_OFFLINE="offline";
    public static final String SESSION_EXPIRED_WARNNING_MESSAGE = "User session expired. Please SignIn...";

    public static final String ERROR_MESSAGE_STRING="errorMessage";
    public static final String ERROR_PAGE_PATH="error/error";
    public static final String ONLINE="ONLINE";
    public static final String  ISLOGINPAGE="isLoginPage";
    public static final String  ISHOMEPAGE="isHomePage";
    public static final String STRING_GOVT="GOVT";
    public static final String STRING_PRIVATE="PRIVATE";
    public static final Object NOT_TEACHER = "Your are not a teacher";
    public static final String SIKHISM = "SIKHISM";
    public static final String JAINISM = "JAINISM";
    public static final String ISLAM = "ISLAM";
    public static final String HINDUISM = "HINDUISM";
    public static final String CHRISTIANITY = "CHRISTIANITY";
    public static final String SIKH = "SIKH";
    public static final String JAIN = "JAIN";
    public static final String MUSLIM = "Muslim";
    public static final String HINDU = "HINDU";
    public static final String CHRISTIAN = "CHRISTIAN";
    public static final String SC="SC";
    public static final String ST="ST";
    public static final String OBC="OBC";
    public static final String GENERAL="GENERAL";
    public static final String MALE_CODE="M";
    public static final String FEMALE_CODE="F";
    public static final String YES = "yes";
    public static final String NO = "no";
    public static final String APVE="A +VE";
    public static final String ANVE="A -VE";
    public static final String BPVE="B +VE";
    public static final String BNVE="B -VE";
    public static final String OPVE="O +VE";
    public static final String ONVE="O -VE";
    public static final String ABPVE="AB +VE";
    public static final String ABNVE="AB -VE";



    public static final String DASHBOARD_PERMISSION = "viewReportDashBoard";
    public static final String PERMISSION_DENIED = "Permission denied!. You have not permitted to access Dashboard.";
    public static final String PERMISSION_GRANTED="You have successfully authenticated.";
    public static final String USER_NOT_EXIST = "User not exists.";
    public static final String CREDENTIAL_NOT_MATCHED = "Credentials doesn't match. In case you have forgotten your password, please contact the administrator for help.";



    public static final String ACADEMIC_YEAR_UPGRADE_FAILURE = "There are some pending/inprogress projects which needs to be complete before upgrading the academic year. For more information , Please contact to IT support.";
    public static final String STRING_31_DEC = "31-Dec";
    public static final String TAG_CHAPTER = "Chapter";
    public static final String TAG_SUBJECT = "Subject";
    public static final String TAG_SOURCE = "Source";
    public static final String TAG_STAGE = "Stage";
    public static final String TAG_GRADE = "Grade";
    public static final String TAG_TAXONOMY = "Taxonomy";
    public static final String QUESTION_BANK_EXIST=" QuestionBank name already exist. ";
    public static final String ASSESSMENT_ALREADY_EXIST = " Question paper name already exist.";
    public static final String QUESTION_PAPER_HAVE_NO_QUESTION = "This Question paper has no question.First add questions and then do finalize.";
    public static final String QUESTION_MAPPED_MORE_THEN_ONE_CHAPTER="This Question paper has some questions that are mapped to more than one chapter. This is not allowed. Please rectify before finalizing."; 
    public static final String QUESTION_HAVE_NO_CHAPTER_MAPPING="This Question paper has some questions that do not have chapter mapping. This is not allowed. Please rectify before finalizing.";
    public static final String QUESTION_HAVE_NO_TAXONOMY_MAPPING="This Question paper has some questions that do not have taxonomy mapping. This is not allowed. Please rectify before finalizing.";
    public static final String QUESTION_HAVE_NO_CHAPTER_AND_TAXONOMY_MAPPING="This Question paper has some questions that do not have taxonomy and question mapping. This is not allowed. Please rectify before finalizing.";
    public static final String QUESTION_HAVE_NO_SCORE="You have not set answers for the questions of this question paper. Need to set the answer of all questions of this question paper.";
    public static final String QUESTION_HAVE_NO_ANSWER="You have not set answers for the questions of this question paper. Need to set the answer of all questions of this question paper.";
    public static final String QUESTION_PAPER_USER_MISMATCH="This Question Paper does not belongs to the user associated with Surveygamez. Please contact IT support.";
    public static final String QUESTION_PAPER_NOT_EXIST="This Question Paper is no more exists in Surveygamez. Please contact IT support.";
    public static final String INTERNAL_SERVER_ERROR="The server encountered an internal error and was unable to complete your request. Please contact IT support.";
    public static final String TAXONOMY_QUESTION_RELATION_EXIST = "There are some Question associated with this Taxonomy so you cannot delete this data";
	public static final String ASSESSMENT_CAMPAIGN_RELATION_EXIST = "There are some Projects associated with this Question paper so you cannot delete this data";
	public static final String NEED_TO_SET_ANSWER="Need to set the Answers for this survey";
	public static final String  LOGIN="logIn";
	public static final String  PASSWORD="password";
	public static final String  SG_AUTHENTICATE_URL="sgAunthenticateUrl";
	public static final String  REDIRECT_URL="redirectUrl";
	public static final String HOLIDAY_ALREADY_EXIST = "Holiday name already exists in this year.";
	public static final String INVALID = "invalid";
	public static final String HOLIDAY_EXCEL_DATE_FORMAT="dd/MM/yyyy";
	public static final String ERROR_OCCURED_WHILE_SAVING = "Error occured while saving data";
	public static final String ERROR_OCCURED_WHILE_UPDATE = "Error occured while updating data";
	public static final String CENTER_ID_ALREADY_EXIST = "Center id already exists.";
	
	public static final String TEACHING_PLAN_HOLIDAY_MESSAGE = "Start date should not be holiday";
	
}
