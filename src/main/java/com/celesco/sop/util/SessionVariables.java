package com.celesco.sop.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.celesco.sop.dto.RoleVm;
import com.celesco.sop.model.Role;
import com.celesco.sop.model.User;


public class SessionVariables {

	public static String getLogin(){
		Object sessionValue = getSession().getAttribute("login");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setLogin(String login) {
		getSession().setAttribute("login", login);
	}
	
	public static String getPwd(){
		Object sessionValue = getSession().getAttribute("pwd");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setPwd(String pwd) {
		getSession().setAttribute("pwd", pwd);
	}
	
	
	public static User getUser(){
		Object sessionValue = getSession().getAttribute("user");
		return sessionValue != null ? (User)sessionValue : null;
	}
	
	public static void setUser(User user) {
		getSession().setAttribute("user", user);
	}
	
	
	
	public static Integer getLoginFail() {
		Object sessionValue = getSession().getAttribute("loginFail");
		return sessionValue != null ? (Integer)sessionValue : null;
	}

	public static void setLoginFail(int loginFail) {
		getSession().setAttribute("loginFail", loginFail);
	}
	
    
	public static Integer getUserId() {
		Object sessionValue = getSession().getAttribute("userId");
		return sessionValue != null ? (Integer)sessionValue : null;
	}

	public static void setUserId(int userId) {
		getSession().setAttribute("userId", userId);
	}

	
	

	public static Integer getRoleId() {
		Object sessionValue = getSession().getAttribute("roleId");
		return sessionValue != null ? (Integer)sessionValue : null;
	}

	public static void setRoleId(int roleId) {
		getSession().setAttribute("roleId", roleId);
	}
	
	public static Role getRoleList() {
		Object sessionValue = getSession().getAttribute("roleLists");
		return sessionValue != null ? (Role)sessionValue : null;
	}

	public static void setRoleList(Role role) {
		getSession().setAttribute("roleLists", role);
	}
	
	
	
	
	public static String getUserName() {

		Object sessionValue = getSession().getAttribute("userName");
		return sessionValue != null ? sessionValue.toString() : null;
		
		//return  getSession().getAttribute("userName").toString();
	}

	public static void setUserName(String userName) {

		
		 getSession().setAttribute("userName", userName);
	}
	
	public static Boolean getIsAdmin(){
		Object sessionValue = getSession().getAttribute("isAdmin");
		return sessionValue != null ? new Boolean(sessionValue.toString()) : null;
	}
	public static void setIsAdmin(boolean isAdmin) {
		getSession().setAttribute("isAdmin", isAdmin);
	}
	
	public static String getEmail() {

		Object sessionValue = getSession().getAttribute("email");
		return sessionValue != null ? sessionValue.toString() : null;
	}

	public static void setEmail(String userName) {
	
		 getSession().setAttribute("email", userName);
	}

	
	public static Integer getCheckSuccess() {

		Object sessionValue = getSession().getAttribute("checkSuccess");
		return sessionValue != null ? (Integer)sessionValue : null;
	}

	public static void setCheckSuccess(int checkSuccess) {

		 getSession().setAttribute("checkSuccess", checkSuccess);
	}
	

	
	
	@SuppressWarnings("unused")
	public static void setSessionToNull()
	{
		HttpSession httpSession = getSession();
		httpSession = null;
		
	}
	
	
	public static HttpSession getSession() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();

		return session;
	}

	public static Integer getPasswordRetries() {
		Object sessionValue = getSession().getAttribute("passwordRetries");
		 return sessionValue != null ? (Integer)sessionValue : null;
		
	}

	public static void setPasswordRetries(int passwordRetries) {
		getSession().setAttribute("passwordRetries", passwordRetries);
		
	}

	
	
	public static String getUploadExcelFileName() {
		Object sessionValue = getSession().getAttribute("excelFileName");
		return sessionValue != null ? sessionValue.toString() : null;
	}

	public static void setUploadExcelFileName(String excelFileName) {
		getSession().setAttribute("excelFileName", excelFileName);
	}
	
	public static void setSuccessMsg(String message){
		getSession().setAttribute("success", message);
	}

	public static void setRoleName(String roleName) {
		 getSession().setAttribute("roleName", roleName);
		
	}
	

	

	public static String getReportName(){
		Object sessionValue = getSession().getAttribute("reportName");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setReportName(String reportName) {
		getSession().setAttribute("reportName", reportName);
	}
	public static Date getLastLoginAttempt(){
		Object sessionValue = getSession().getAttribute("lastLoginAttempt");
		return sessionValue != null ? (Date)sessionValue : null;
	}
	
	public static void setLastLoginAttempt(Date lastLoginAttempt) {
		getSession().setAttribute("lastLoginAttempt", lastLoginAttempt);
	}
	public static String getSuperRoleCode() {

		Object sessionValue = getSession().getAttribute("superRoleCode");
		return sessionValue != null ? sessionValue.toString() : null;
		
	
	}

	public static void setSuperRoleCode(String superRoleCode) {
           getSession().setAttribute("superRoleCode", superRoleCode);
	}

	
	
	
	public static String getSGEndPoint(){
		Object sessionValue = getSession().getAttribute("sgEndPoint");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setSGEndPoint(String sgEndPoint) {
		getSession().setAttribute("sgEndPoint", sgEndPoint);
	}
	
	
	public static String getBaseURL(){
		Object sessionValue = getSession().getAttribute("baseUrl");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setBaseURL(String baseUrl) {
		getSession().setAttribute("baseUrl", baseUrl);
	}

	
	public static Boolean getIsNotified(){
		Object sessionValue = getSession().getAttribute("isNotified");
		return sessionValue != null ? new Boolean(sessionValue.toString()) : null;
	}
	public static void setIsNotified(boolean isNotified) {
		getSession().setAttribute("isNotified", isNotified);
	}
	
	
	
	public static String getAppType(){
		Object sessionValue = getSession().getAttribute("appType");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setAppType(String appType) {
		getSession().setAttribute("appType",appType);
	}
	
	
	
	public static boolean isShikshaPlus(){
		Object sessionValue = getSession().getAttribute("isShikshaPlus");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsShikshaPlus(boolean isShikshaPlus) {
		getSession().setAttribute("isShikshaPlus",isShikshaPlus);
	}
	
	
	
	public static boolean isShikshaPlusApp(){
		Object sessionValue = getSession().getAttribute("isShikshaPlusApp");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsShikshaPlusApp(boolean isShikshaPlusApp) {
		getSession().setAttribute("isShikshaPlusApp",isShikshaPlusApp);
	}
	
	public static boolean isShikshaApp(){
		Object sessionValue = getSession().getAttribute("isShikshaApp");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsShikshaApp(boolean isShikshaApp) {
		getSession().setAttribute("isShikshaApp",isShikshaApp);
	}
	
	
	
	public static boolean isMultiRoles(){
		Object sessionValue = getSession().getAttribute("isMultiRoles");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsMultiRoles(boolean isMultiRoles) {
		getSession().setAttribute("isMultiRoles",isMultiRoles);
	}

	
	
	public static boolean isShikshaMultiRoles(){
		Object sessionValue = getSession().getAttribute("isShikshaMultiRoles");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsShikshaMultiRoles(boolean isShikshaMultiRoles) {
		getSession().setAttribute("isShikshaMultiRoles",isShikshaMultiRoles);
	}
	
	
	public static boolean isShikshaPlusMultiRoles(){
		Object sessionValue = getSession().getAttribute("isShikshaPlusMultiRoles");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsShikshaPlusMultiRoles(boolean isShikshaPlusMultiRoles) {
		getSession().setAttribute("isShikshaPlusMultiRoles",isShikshaPlusMultiRoles);
	}
	
	
	
	
	public static List<RoleVm> getShikshaMultiRoles(){
		Object sessionValue = getSession().getAttribute("shikshaMultiRoles");
		return sessionValue != null ? (List<RoleVm>)sessionValue : new ArrayList<RoleVm>();
	}
	
	public static void setShikshaMultiRoles(List<RoleVm> shikshaMultiRoles) {
		getSession().setAttribute("shikshaMultiRoles",shikshaMultiRoles);
	}
	
	public static List<RoleVm> getShikshaPlusMultiRoles(){
		Object sessionValue = getSession().getAttribute("shikshaPlusMultiRoles");
		return sessionValue != null ? (List<RoleVm>)sessionValue : new ArrayList<RoleVm>();
	}
	
	public static void setShikshaPlusMultiRoles(List<RoleVm> shikshaPlusMultiRoles) {
		getSession().setAttribute("shikshaPlusMultiRoles",shikshaPlusMultiRoles);
	}
	
	
	
	
	
	
	
	public static boolean isBothAppPermissions(){
		Object sessionValue = getSession().getAttribute("isBothAppPermissions");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsBothAppPermissions(boolean isBothAppPermissions) {
		getSession().setAttribute("isBothAppPermissions",isBothAppPermissions);
	}
	
	public static List<RoleVm> getRoles(){
		Object sessionValue = getSession().getAttribute("roles");
		return sessionValue != null ? (List<RoleVm>)sessionValue : new ArrayList<RoleVm>();
	}
	
	public static void setRoles(List<RoleVm> roles) {
		getSession().setAttribute("roles",roles);
	}

	
	

	public static boolean isNavigate(){
		Object sessionValue = getSession().getAttribute("isNavigate");
		return sessionValue != null ? (boolean)sessionValue : false;
	}
	
	public static void setIsNavigate(boolean isNavigate) {
		getSession().setAttribute("isNavigate",isNavigate);
	}
	
	public static String getAppTypeHomeURL(){
		Object sessionValue = getSession().getAttribute("appTypeHomeURL");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setAppTypeHomeURL(String isNavigate) {
		getSession().setAttribute("appTypeHomeURL",isNavigate);
	}
	public static String getAppLocale(){
		Object sessionValue = getSession().getAttribute("appLocale");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setAppLocale(String appLocale) {
		getSession().setAttribute("appLocale",appLocale);
	}
	
	
	
	
	public static String getCurrentAY(){
		Object sessionValue = getSession().getAttribute("currentAY");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setCurrentAY(String currentAY) {
		getSession().setAttribute("currentAY",currentAY);
	}
	
	
	public static String getCurrentAYCycle(){
		Object sessionValue = getSession().getAttribute("currentAYCycle");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setCurrentAYCycle(String currentAYCycle) {
		getSession().setAttribute("currentAYCycle",currentAYCycle);
	}
	
	public static String getUserAvatar(){
		Object sessionValue = getSession().getAttribute("userAvatar");
		return sessionValue != null ? (String)sessionValue : null;
	}
	
	public static void setUserAvatar(String userAvatar) {
		getSession().setAttribute("userAvatar",userAvatar);
	}
	
	
}



