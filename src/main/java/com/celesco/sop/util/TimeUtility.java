package com.celesco.sop.util;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUtility {


	/**
	 * Convert a String to java.sql.Time
	 * */
	public static Time toTime(String time) throws ParseException{
		boolean isValid=time!=null?!time.equals("")?true:false:false;
		if(isValid){
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
			LocalTime localTime= LocalTime.parse(time, formatter);
		
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm");
			String string24HFormat= getStringFromLocalTime(localTime, "HH:mm");
			localTime= LocalTime.parse(string24HFormat, fmt);
			
			return Time.valueOf(localTime);
		}
		return null;
	}
	public static Time toTime(String time,String timeformat) throws ParseException{
		boolean isValid=time!=null?!time.equals("")?true:false:false;
		if(isValid){
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeformat);
			LocalTime localTime= LocalTime.parse(time, formatter);
		
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm");
			String string24HFormat= getStringFromLocalTime(localTime, "HH:mm");
			localTime= LocalTime.parse(string24HFormat, fmt);
			
			return Time.valueOf(localTime);
		}
		return null;
	}

	public static Time toSQLTime(String time) throws ParseException{
		boolean isValid=time!=null?!time.equals("")?true:false:false;
		if(isValid){
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
			long ms = sdf.parse(time).getTime();
			
			return new Time(ms);
		}
		return null;
	}
	
	/**
	 * Convert Java.sql.Time to String format
	 * */
	public static String toTime(Time time) throws ParseException{
		if(time!=null){
			LocalTime localTime =time.toLocalTime();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
			String formatedTime= localTime.format(formatter);
			System.out.println("formatedTime : "+formatedTime);
			return formatedTime;
		}
		return null;

	}
	public static String getStringFromLocalTime(LocalTime time,String format) throws ParseException{
		boolean isValid=time!=null?!time.equals("")?true:false:false;
		if(time!=null && isValid){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String formatedTime= time.format(formatter);
		System.out.println("formatedTime Local date : "+formatedTime);
		return formatedTime;
		}
		return null;

	}
}
