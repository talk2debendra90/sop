package com.celesco.sop.dto;

import java.io.Serializable;
import java.util.List;

public class BaseResponseVm implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String response;
	private String responseMessage;
	private List<String> responseMessages;
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public List<String> getResponseMessages() {
		return responseMessages;
	}
	public void setResponseMessages(List<String> responseMessages) {
		this.responseMessages = responseMessages;
	}
	
	
}
