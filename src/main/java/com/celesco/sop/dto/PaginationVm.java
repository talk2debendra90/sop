package com.celesco.sop.dto;

import java.util.List;
import java.util.Map;

public class PaginationVm<T> extends BaseResponseVm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<T> rows;
	private Map<String,String> metadata;
	
	private int offset;
	private int limit;
	private List<String> fields;
	private String sort;
	private String search;
	private Long total;
	private String order;
	
	
	
	
	private String searchText;
	private String sortOrder;
	private Integer pageNumber;
	private Boolean isOnload;
	
	public PaginationVm(){
		super();
	}

	
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public List<String> getFields() {
		return fields;
	}
	public void setFields(List<String> fields) {
		this.fields = fields;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	


	public String getSearch() {
		return search;
	}



	public void setSearch(String search) {
		this.search = search;
	}



	public List<T> getRows() {
		return rows;
	}



	public void setRows(List<T> rows) {
		this.rows = rows;
	}



	public Map<String, String> getMetadata() {
		return metadata;
	}



	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}



	public Long getTotal() {
		return total;
	}



	public void setTotal(Long total) {
		this.total = total;
	}



	


	public Boolean getIsOnload() {
		return isOnload;
	}



	public void setIsOnload(Boolean isOnload) {
		this.isOnload = isOnload;
	}



	public String getOrder() {
		return order;
	}



	public void setOrder(String order) {
		this.order = order;
	}



	public String getSearchText() {
		return searchText;
	}



	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}



	public String getSortOrder() {
		return sortOrder;
	}



	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Integer getPageNumber() {
		return pageNumber;
	}



	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	
	
}
