package com.celesco.sop.dto;

public class RolePermissionVm extends BaseResponseVm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isCheck;
	private Integer roleId;
	private Integer permissionId;

	public RolePermissionVm() {
		super();
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public boolean getIsCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

}
