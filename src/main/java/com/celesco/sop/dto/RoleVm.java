package com.celesco.sop.dto;

public class RoleVm extends BaseResponseVm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer roleId;
	private String roleName;
	private String roleCode;
	private String isChecked;
	private Boolean isSelected;
	private Boolean isMappedToSchool;
	private Integer sequenceNumber;
	private String abbreviations;
	private Integer programCode;
	private String programType;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Boolean getIsMappedToSchool() {
		return isMappedToSchool;
	}

	public void setIsMappedToSchool(Boolean isMappedToSchool) {
		this.isMappedToSchool = isMappedToSchool;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getAbbreviations() {
		return abbreviations;
	}

	public void setAbbreviations(String abbreviations) {
		this.abbreviations = abbreviations;
	}

	public Integer getProgramCode() {
		return programCode;
	}

	public void setProgramCode(Integer programCode) {
		this.programCode = programCode;
	}

	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}
	
}
