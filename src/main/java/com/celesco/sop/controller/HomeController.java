package com.celesco.sop.controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.celesco.sop.util.SessionVariables;

@Controller
public class HomeController {
	
	
	private static final Logger logger = Logger.getLogger(HomeController.class.getName());
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcomePage() {		
		logger.info("Home controller initiated...");
		
		ModelAndView modelView=new ModelAndView();
		if(SessionVariables.getUserId()!=null){			
			modelView.setViewName("home");	
		}else{			
			modelView.setViewName("login");
		}	
		return modelView;
	}
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView userHome() {
		logger.info("User home controller initiated...");
		ModelAndView modelView=new ModelAndView();
		
		HttpSession session = SessionVariables.getSession();
		if(session==null){
			modelView.setViewName("login");
			return modelView;
		}
		try {		
			modelView.setViewName("home");
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error(exception.getMessage());
			modelView.setViewName("error/error");
		}
		
		return modelView;
	}
	
	@RequestMapping(value = "/home/fromForgotPassword", method = RequestMethod.GET)
	public ModelAndView userHomePage() {
		logger.info("Home  from forgotPassword view controller initiated...");
		ModelAndView modelView=new ModelAndView();
		try {
			modelView.addObject("isForgotPassword",true);
			modelView.setViewName("home");
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error(exception.getMessage());
			modelView.setViewName("error/error");
		}
		
		return modelView;
	}
	
	
	
	@RequestMapping(value = "/session/invalidate", method = RequestMethod.GET)
	public void invalidateSession(Model model, HttpServletRequest request) {
		logger.info("Session invalidate controller initiated.....");
		model.asMap().clear();
		HttpSession session = SessionVariables.getSession();
		if (session != null) {
			session.removeAttribute("email");			
			session.invalidate();
			session = null;
		}
	}
	@RequestMapping(value = "/session/reload", method = RequestMethod.GET)
	public ModelAndView loginPage(Model model, HttpServletRequest request) {		
		logger.info("Session reload controller initiated...");
		model.asMap().clear();
		HttpSession session = SessionVariables.getSession();
		if (session != null) {
			session.removeAttribute("email");			
			session.invalidate();
			session = null;
		}
		ModelAndView modelView=new ModelAndView();
		modelView.setViewName("login");
		return modelView;
	}
}
