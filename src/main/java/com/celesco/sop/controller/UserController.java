package com.celesco.sop.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.celesco.sop.dto.UserDto;
import com.celesco.sop.service.UserService;
import com.celesco.sop.util.SOPConstants;

@Controller
public class UserController {
	
	
	@Autowired
	private UserService userService;

	
	
	@PostMapping(value = "/signin")
	@ResponseBody
	public UserDto signin(@RequestBody UserDto userDto) {
		
		if(null!=userService.login(userDto)){
			userDto.setResponse(SOPConstants.SUCCESS_MESSAGE_CONSTANT);
			userDto.setResponseMessage(SOPConstants.SUCCESS_MESSAGE_CONSTANT);
			return userDto;
		}
		return null;
		
	}
}
