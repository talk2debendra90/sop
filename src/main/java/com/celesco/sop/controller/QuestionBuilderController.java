package com.celesco.sop.controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class QuestionBuilderController {
	
	
	
	@RequestMapping(value = "/question/build", method = RequestMethod.GET)
	public ModelAndView welcomePage() {		
		ModelAndView modelView=new ModelAndView();
		modelView.setViewName("questionBuilder/createQuestions");
		return modelView;
	}
	

}
