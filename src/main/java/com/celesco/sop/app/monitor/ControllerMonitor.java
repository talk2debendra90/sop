package com.celesco.sop.app.monitor;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerMonitor {

	protected static  final Logger LOGGER =  Logger.getLogger("Controller Audit Log");
	

	@Before("execution(public * com.celesco.sop.controller.*Controller.*(..))"
			 + "&& !@annotation(com.celesco.sop.annotations.NoLogging)" 
	            + "&& !@target(com.celesco.sop.annotations.NoLogging)")
	public void logBeforeAccess(JoinPoint joinPoint) {
		if(joinPoint!=null){
			String packageName = joinPoint.getSignature()!=null?joinPoint.getSignature().getDeclaringTypeName():"SOP-LOG-404";
			LOGGER.info(". . .A request initiated from controller [" + packageName + "."+ getMethodSignature(joinPoint) +  "]. . .");
		}

	}
	
	@After("execution(public * com.celesco.sop.controller.*Controller.*(..))"
			 + "&& !@annotation(com.celesco.sop.annotations.NoLogging)" 
	            + "&& !@target(com.celesco.sop.annotations.NoLogging)")
	public void logAfterAccess(JoinPoint joinPoint) {
		if(joinPoint!=null){
			String packageName = joinPoint.getSignature()!=null?joinPoint.getSignature().getDeclaringTypeName():"SOP-LOG-404";
			LOGGER.info(". . .Request from controller [" + packageName + "."+ getMethodSignature(joinPoint) +  "] completed. . .");
		}
	}
		
	@AfterThrowing(pointcut = "execution(public * com.celesco.sop.controller.*Controller.*(..))",throwing="exception")
	public void logAfterThrowing(Exception exception){
		LOGGER.error("Exception caught:"+ exception.getMessage());
	}
	
	private String getMethodSignature(JoinPoint joinPoint){
		if(joinPoint!=null){
			String methodName = joinPoint.getSignature().getName();
			Object[] arguments = joinPoint.getArgs();
			StringBuilder sb=new StringBuilder();
			if(arguments!=null){
				for (Object param: arguments) {
					sb.append(param).append(",");
				}
				sb =(sb.length()>1)?sb.deleteCharAt(sb.length()-1):sb;
			}
			methodName = methodName+"("+new String(sb)+")";
			return methodName;
		}else{
			return "SOP-LOG-405";
		}
	}
}
