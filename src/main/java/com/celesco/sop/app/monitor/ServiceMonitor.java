package com.celesco.sop.app.monitor;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceMonitor {
	protected static  final Logger LOGGER = Logger.getLogger("Service Audit Log");
	
	

	@Before("execution(public * com.celesco.sop.service.impl.*ServiceImpl.*(..))")
	public void logBeforeAccess(JoinPoint joinPoint) {
		if(joinPoint!=null){
			String packageName = joinPoint.getSignature()!=null?joinPoint.getSignature().getDeclaringTypeName():"SOP-LOG-404";
			LOGGER.info(". . .Service [" + packageName + "."+ getMethodSignature(joinPoint) +  " initiated . . .]");

		}
	}

	@After("execution(public * com.celesco.sop.service.impl.*ServiceImpl.*(..))")
	public void logAfterAccess(JoinPoint joinPoint) {
		if(joinPoint!=null){
			String packageName = joinPoint.getSignature()!=null?joinPoint.getSignature().getDeclaringTypeName():"SOP-LOG-404";
			LOGGER.info(". . .Service [" + packageName + "."+ getMethodSignature(joinPoint) +  " completed . . .]");
		}
	}
	
	private String getMethodSignature(JoinPoint joinPoint){
		
		if(joinPoint!=null){
			String methodName = joinPoint.getSignature().getName();
			 Object[] arguments = joinPoint.getArgs();
			 StringBuilder sb=new StringBuilder();
			 if(arguments!=null){
			   for (Object param: arguments) {
				   sb.append(param).append(",");
			   }
			   sb =(sb.length()>1)?sb.deleteCharAt(sb.length()-1):sb;
			 }
			methodName = methodName+"("+new String(sb)+")";
			return methodName;
		}else{
			return "SOP-OG-405";
		}
	}
	
	
}
