package com.celesco.sop.mappers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.celesco.sop.dto.PaginationVm;

public class PaginationVmMapper {@SuppressWarnings("rawtypes")
public static PaginationVm toPaginationVm(Page<?> page,PaginationVm<?> paginationVm){
	
		
			Map<String,String> metadata =new HashMap<String,String>();
		    metadata.put("numberOfPageElements", String.valueOf(page.getNumberOfElements()));
		    metadata.put("perPage",String.valueOf(page.getSize()));
		    metadata.put("totalPages",String.valueOf( page.getTotalPages()));
		    metadata.put("totalElements",String.valueOf(page.getTotalElements()));
		    
		    paginationVm.setMetadata(metadata);
		    return paginationVm;
	}
}
