package com.celesco.sop.mappers;

import com.celesco.sop.dto.RoleVm;
import com.celesco.sop.model.Role;

public class RoleMapper {
	private RoleMapper() {

	}
	public static RoleVm toRoleVm(Role role) {
		RoleVm roleVm = new RoleVm();
		if(role!=null){
			roleVm.setRoleId(role.getId());
			roleVm.setRoleName(role.getName());
			roleVm.setRoleCode(role.getCode());
			roleVm.setIsMappedToSchool(role.getIsMappedToSchool());
			roleVm.setSequenceNumber(role.getSequenceNumber());
			roleVm.setAbbreviations(role.getAbbreviations());	
			roleVm.setProgramCode(role.getProgramCode());
			
			roleVm.setProgramType(role.getProgramCode()==2?"Shiksha Elementary & Plus":role.getProgramCode()==1?"Shiksha Plus":"Shiksha Elementary");
			
			
		}
		
		return roleVm;
	}

	public static Role toRole(RoleVm roleVm) {
		Role role = new Role();
		role.setName(roleVm.getRoleName());
		role.setCode(roleVm.getRoleCode());
		role.setAbbreviations(roleVm.getRoleCode());
		role.setSequenceNumber(roleVm.getSequenceNumber());
		role.setIsMappedToSchool(roleVm.getIsMappedToSchool());
		role.setProgramCode(roleVm.getProgramCode());
		role.setIsActive(true);
		
		return role;
	}

	public static Role toRole(RoleVm roleVm, Role role) {
		role.setName(roleVm.getRoleName());
		//role.setCode(roleVm.getRoleCode());
		//role.setAbbreviations(roleVm.getRoleCode());
		role.setSequenceNumber(roleVm.getSequenceNumber());
		//role.setProgramCode(roleVm.getProgramCode());
		role.setIsMappedToSchool(roleVm.getIsMappedToSchool());
		return role;
	}

}
