package com.celesco.sop.mappers;


import com.celesco.sop.model.Role;
import com.celesco.sop.model.User;
import com.celesco.sop.model.UserRoleMapping;

public class UserRoleMappingMapper {
	private UserRoleMappingMapper() {
		
	}
	 public static UserRoleMapping toUserRoleMapping(Integer userId, Integer roleId){
		 User user = new User();
		 user.setId(userId);
		 
		 Role role = new Role();
		 role.setId(roleId);
		 
		 UserRoleMapping userRoleMapping = new UserRoleMapping();
		 userRoleMapping.setRole(role);
		 userRoleMapping.setUser(user);
		 userRoleMapping.setIsActive(true);
		 return userRoleMapping;
		 
	 }
	 public static UserRoleMapping toUserRoleMapping(User user, Role role){
		 if(user!=null && role!=null){
			 UserRoleMapping userRoleMapping = new UserRoleMapping();
			 userRoleMapping.setRole(role);
			 userRoleMapping.setUser(user);
			 userRoleMapping.setIsActive(true);
			 return userRoleMapping;
		 }
		return null;
		 
	 }
}
