package com.celesco.sop.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;



public class LoginInterceptor implements HandlerInterceptor {
	private static final Logger LOGGER = Logger.getLogger(LoginInterceptor.class);
	
	
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		/*
        Integer userId = SessionVariables.getUserId();
       try{
        	if (userId == null) {
        		response.sendRedirect(request.getContextPath() + "/");
        		return false;
        	}        	
        }catch(Exception e){
        	LOGGER.info("no session");
        }*/
		return true;
		
	}

	@Override
	public void postHandle(HttpServletRequest request,	HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate,max-stale=0,post-check=0,pre-check=0");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);		
		int sessionTimeoutValue = request.getSession().getMaxInactiveInterval();
		Cookie cookie = new Cookie("TimeoutCookie", "1");
		cookie.setMaxAge(sessionTimeoutValue);
		response.addCookie(cookie);
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}
