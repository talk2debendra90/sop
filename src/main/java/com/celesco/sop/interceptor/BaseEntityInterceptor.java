package com.celesco.sop.interceptor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.celesco.sop.util.SessionVariables;

@Component
public class BaseEntityInterceptor extends EmptyInterceptor{

	private String currentType="";
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	

	
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		Date date =new java.util.Date();
		if(SessionVariables.getUser()!=null){
			setValue(state, propertyNames, "createdDate", date);
			setValue(state, propertyNames, "modifiedDate", date);
			setValue(state, propertyNames, "createdBy", SessionVariables.getUser());
			setValue(state, propertyNames, "modifiedBy", SessionVariables.getUser());
			logger.info(" Sucessfully inserted");				
		}
		return true;
	}

	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] state, Object[] previousState,
			String[] propertyNames, Type[] types) {

		Date date =new java.util.Date();
		String updateType = entity.getClass().getName();
		if(!currentType.equals(updateType)){
			if(SessionVariables.getUser()!=null){
				setValue(state, propertyNames, "modifiedDate", date);
				setValue(state, propertyNames, "modifiedBy", SessionVariables.getUser());
				logger.info(" Sucessfully updated");
			}
			
		}
		currentType = "";
		return true;
	}

	public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		Date date = new Date();
		
		if(SessionVariables.getUser()!=null){
			setValue(state, propertyNames, "modifiedDate", date);
			setValue(state, propertyNames, "modifiedBy", SessionVariables.getUser());
			logger.info("Sucessfully deleted");					
		}
	}

	private void setValue(Object[] currentState, String[] propertyNames, String propertyToSet, Object value) {
		int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		}
	}
}
