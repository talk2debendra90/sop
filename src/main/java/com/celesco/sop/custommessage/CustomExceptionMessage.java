package com.celesco.sop.custommessage;

import com.celesco.sop.util.SOPConstants;

public class CustomExceptionMessage {

	public static String getCustomExceptionMessage(String exceptionMessage) {

		if (exceptionMessage.contains("Could not open Hibernate Session")) {
			return SOPConstants.DATABASE_CONNECTION_ERROR;
		} else if( exceptionMessage.contains("org.hibernate.exception.SQLGrammarException")){
			return SOPConstants.DATABASE_ERROR;
		}else if(exceptionMessage.contains("org.hibernate.exception.DataException")){
			return SOPConstants.DATABASE_COLUMNLIMIT;
		}else if(exceptionMessage.contains("could not execute statement; SQL [n/a]; nested exception")){
			return SOPConstants.DATABASE_ERROR;
		}else if(exceptionMessage.contains("The specified share is full.")) {
			return "The specified azure storage is full.Please contact IT support";
		}
		return "OOPs!!! Server error.";
	}

}
