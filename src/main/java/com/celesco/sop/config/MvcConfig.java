package com.celesco.sop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import com.celesco.sop.interceptor.LoginInterceptor;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/web-resources/**").addResourceLocations("/resources/");
	}
	@Override
	public void addInterceptors(InterceptorRegistry registry){

		registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**")
		.excludePathPatterns("/")
		.excludePathPatterns("/sessionExpired")
		.excludePathPatterns("/login")
		.excludePathPatterns("/forgot")
		.excludePathPatterns("/emailLinkVerify/*")
		.excludePathPatterns("/forgotPassword")		
		.excludePathPatterns("/web-resources/**");
		registry.addInterceptor(webContentInterceptor());
	}





	@SuppressWarnings("deprecation")
	@Bean
	public WebContentInterceptor webContentInterceptor() {
		WebContentInterceptor interceptor = new WebContentInterceptor();
		interceptor.setCacheSeconds(0);
		interceptor.setUseExpiresHeader(true);
		interceptor.setUseCacheControlHeader(true);
		interceptor.setUseCacheControlNoStore(true);

		return interceptor;
	}


}
