package com.celesco.sop.config;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.jta.JtaTransactionManager;

import com.celesco.sop.interceptor.BaseEntityInterceptor;

@Configuration
public class HibernateConfiguration extends HibernateJpaAutoConfiguration {

	public HibernateConfiguration(DataSource dataSource, JpaProperties jpaProperties,
			ObjectProvider<JtaTransactionManager> jtaTransactionManager,
			ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
		super(dataSource, jpaProperties, jtaTransactionManager, transactionManagerCustomizers);
	}

	@Autowired
	private BaseEntityInterceptor baseEntityInterceptor;

	@Override
	protected void customizeVendorProperties(Map<String, Object> vendorProperties) {
		vendorProperties.put("hibernate.ejb.interceptor", baseEntityInterceptor);

	}

}
