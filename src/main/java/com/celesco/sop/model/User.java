package com.celesco.sop.model;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import com.celesco.sop.security.CustomAuthority;
import com.celesco.sop.util.SessionVariables;


@SuppressWarnings("serial")
@Entity
@Table(name = "user")

public class User extends BaseModel implements UserDetails {

	private String name;
	private String login;
	private String password;
	private String email;
	private String phone;
	
	private String photoUrl;
	
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinTable(
	        name = "user_role",
	        joinColumns = @JoinColumn(name = "user_id"),
	        inverseJoinColumns =@JoinColumn(name = "roles_id")
	)
	
	private Set<Role> roles;	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Override
	@Transient
	@Transactional
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SessionVariables.setIsShikshaPlus(false);
		Set<CustomAuthority> authorities = new HashSet<CustomAuthority>();
		Role role =SessionVariables.getRoleList();
		if(role!=null){
			for (Permission permission : role.getPermissions()) {
				CustomAuthority customAuthority = new CustomAuthority(
						"PERMISSION_" + permission.getName());
				authorities.add(customAuthority);
			}
		}
		return authorities;
	}

	@Override
	public String getUsername() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// 
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// 
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// 
		return false;
	}

	@Override
	public boolean isEnabled() {
		// 
		return false;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
}
