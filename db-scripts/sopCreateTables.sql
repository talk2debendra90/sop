
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL auto_increment,
  `createdDate` datetime default NULL,
  `isActive` bit(1) default NULL,
  `modifiedDate` datetime default NULL,
  `code` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `hierarchy` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `programCode` int(11) default NULL,
  `url` varchar(255) default NULL,
  `createdBy` int(11) default NULL,
  `modifiedBy` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK1thlq4d8h2nmmqxqy6opj82xl` (`createdBy`),
  KEY `FKo001o2htdsuoay5hbfm2rmd81` (`modifiedBy`),
  CONSTRAINT `FKo001o2htdsuoay5hbfm2rmd81` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FK1thlq4d8h2nmmqxqy6opj82xl` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `createdDate` datetime default NULL,
  `isActive` bit(1) default NULL,
  `modifiedDate` datetime default NULL,
  `abbreviations` varchar(255) default NULL,
  `code` varchar(255) default NULL,
  `isMappedToSchool` bit(1) default NULL,
  `name` varchar(255) default NULL,
  `programCode` int(11) default NULL,
  `sequenceNumber` int(11) default NULL,
  `createdBy` int(11) default NULL,
  `modifiedBy` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UK_8sewwnpamngi6b1dwaa88askk` (`name`),
  KEY `FKnss8q8xknag2srmok5k8x7l76` (`createdBy`),
  KEY `FKiwl8o8ljj7u9sf5soc9k2n4hb` (`modifiedBy`),
  CONSTRAINT `FKiwl8o8ljj7u9sf5soc9k2n4hb` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FKnss8q8xknag2srmok5k8x7l76` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `createdDate` datetime default NULL,
  `isActive` bit(1) default NULL,
  `modifiedDate` datetime default NULL,
  `permissions_id` int(11) default NULL,
  `role_id` int(11) default NULL,
  `createdBy` int(11) default NULL,
  `modifiedBy` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FKqkbp6xt0yfvrpi805hjy9umsy` (`createdBy`),
  KEY `FKclain3nrsl53upfrdooocwiy0` (`modifiedBy`),
  KEY `FKsidab0lpqi82o4o15bwde2c5f` (`permissions_id`),
  KEY `FKa6jx8n8xkesmjmv6jqug6bg68` (`role_id`),
  CONSTRAINT `FKa6jx8n8xkesmjmv6jqug6bg68` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKclain3nrsl53upfrdooocwiy0` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FKqkbp6xt0yfvrpi805hjy9umsy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FKsidab0lpqi82o4o15bwde2c5f` FOREIGN KEY (`permissions_id`) REFERENCES `permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `createdDate` datetime default NULL,
  `isActive` bit(1) default NULL,
  `modifiedDate` datetime default NULL,
  `email` varchar(255) default NULL,
  `login` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `photoUrl` varchar(255) default NULL,
  `createdBy` int(11) default NULL,
  `modifiedBy` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FKt8b01a13ndrte11q8od3oa8pk` (`createdBy`),
  KEY `FKau35pwmd3omx41bww5oq8yxl5` (`modifiedBy`),
  CONSTRAINT `FKau35pwmd3omx41bww5oq8yxl5` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FKt8b01a13ndrte11q8od3oa8pk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `createdDate` datetime default NULL,
  `isActive` bit(1) default NULL,
  `modifiedDate` datetime default NULL,
  `createdBy` int(11) default NULL,
  `modifiedBy` int(11) default NULL,
  `roles_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`user_id`,`roles_id`),
  KEY `FK9hfp84o9sxyib057bv5nbqbm5` (`createdBy`),
  KEY `FK179wtys6mt6mbyxsoyownglb5` (`modifiedBy`),
  KEY `FKeog8p06nu33ihk13roqnrp1y6` (`roles_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK179wtys6mt6mbyxsoyownglb5` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FK9hfp84o9sxyib057bv5nbqbm5` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`),
  CONSTRAINT `FKeog8p06nu33ihk13roqnrp1y6` FOREIGN KEY (`roles_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

