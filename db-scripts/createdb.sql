drop database if exists sop;
create database sop DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
use lms;
source ./sopCreateTables.sql;
source ./seed.sql;
